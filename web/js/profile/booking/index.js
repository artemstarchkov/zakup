$(document).ready(function () {
   $("#bookingStatusFilter").on('change', function () {
       $("#bookingFilterForm").submit();
   });

    $("#bookingResetFilter").on('click', function () {
        $("#bookingStatusFilter").val('');
        $("#bookingFilterForm").submit();
    });
});