$(document).ready(function () {
    moment.locale("ru");

    $('.booking-history-daterange').daterangepicker({
        opens: 'left',
        locale: {
            applyLabel: 'Выбрать',
            cancelLabel: 'Отменить'
        }
    }, function(start, end, label) {
        $("input:hidden[name='startDate']").val(start.format('YYYY-MM-DD'));
        $("input:hidden[name='endDate']").val(end.format('YYYY-MM-DD'));
    });
});
