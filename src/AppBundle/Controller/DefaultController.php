<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('AppBundle:default:index.html.twig', []);
    }

    /**
     * User login
     *
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request) {

        if ($request->getMethod() == 'POST') {
            $user = null;
            $login =$request->get('login');
            $password =$request->get('password');

            $user = $this->getDoctrine()->getRepository('ApiBundle:User')->createQueryBuilder('u')
                ->where('u.email = :email')
                ->orWhere('u.username = :username')
                ->setParameter('email', $login)
                ->setParameter('username', $login)
                ->setMaxResults(1)
                ->getQuery()->getOneOrNullResult();

            if (!$user) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('errorUserLogin', 'Неправильно введён логин или пароль.');

                return $this->redirectToRoute('login');
            }

            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($user);

            $bool = ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) ? true : false;

            if ($bool) {
                $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());

                // For older versions of Symfony, use security.context here
                $this->get("security.token_storage")->setToken($token);

                // Fire the login event
                // Logging the user in above the way we do it doesn't do this automatically
                $event = new InteractiveLoginEvent($request, $token);
                $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

                if ($user->isSuperAdmin()) {
                    return $this->redirectToRoute('admin_index');
                } elseif($user->getSpecialRole() == 'ROLE_COMPANY') {
                    return $this->redirectToRoute('profile_company_index');
                } else {
                    $request->getSession()
                        ->getFlashBag()
                        ->add('errorUserLogin', 'У вас нет прав доступа.');

                    return $this->redirectToRoute('login');
                }
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('errorUserLogin', 'Неправильно введён логин или пароль.');

                return $this->redirectToRoute('login');
            }
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AppBundle:default:login.html.twig', array(
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * @Route("/send/contact_message", name="send_contact_message")
     * @Method({"GET", "POST"})
     */
    public function sendContactMessageAjax(Request $request) {
        $textMessage = $request->get('textMessage');
        $fromEmail = $request->get('fromEmail');
        $fromFullName = $request->get('fromFullName');
        $phoneNumber = $request->get('phoneNumber');

        $text = "Имя отправителя: {$fromFullName}; Почта отправителя: {$fromEmail}; Номер телефона отправителя: {$phoneNumber}; Текст сообщения: {$textMessage}";

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom($fromEmail)
            ->setTo($this->getParameter('mailer_user'))
            ->setBody($text,
                'text/html'
            );

        /*$this->get('mailer')->send($message);*/
        $transport = Swift_SmtpTransport::newInstance($this->getParameter('mailer_host'), $this->getParameter('mailer_port'), $this->getParameter('mailer_encryption'))
            ->setUsername($this->getParameter('mailer_user'))
            ->setPassword($this->getParameter('mailer_password'));

        $mailer = \Swift_Mailer::newInstance($transport);
        $mailer->send($message);

        return new JsonResponse(['isSend' => true]);
    }
}
