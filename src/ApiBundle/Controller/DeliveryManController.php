<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\BookingItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @package ApiBundle\Controller
 * @Route("/api/deliveryman", name="api_deliveryman_")
 */
class DeliveryManController extends FOSRestController
{
    /**
     * @Rest\Get("/accepted_booking/list")
     */
    public function getDeliveryAcceptedBookingListAction(Request $request)
    {
        $results = array();
        $bookings = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookings = $this->getDoctrine()->getRepository('ApiBundle:Booking')->getAcceptedBookingListByDeliveryStaff($user);

        foreach ($bookings as $booking) {
            $results[$booking->getId()]['booking'] = array(
                'bookingId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bookingPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerUserId' => $booking->getCustomerUser()->getId(),
                'customerShopName' => $booking->getCustomerUser()->getShop()->getName(),
                'customerShopThumbnailImage' => $booking->getCustomerUser()->getShop()->getPhotoThumbnailWebLink()
            );
        }

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/in_progress_booking/list")
     */
    public function getDeliveryInProgressBookingListAction(Request $request)
    {
        $results = array();

        /** @var Booking[] $bookings */
        $bookings = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookings = $this->getDoctrine()->getRepository('ApiBundle:Booking')->getInProgressBookingListByDeliveryStaff($user);

        foreach ($bookings as $booking) {
            $results[$booking->getId()]['booking'] = array(
                'bookingId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bookingPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerUserId' => $booking->getCustomerUser()->getId(),
                'customerShopName' => $booking->getCustomerUser()->getShop()->getName(),
                'customerShopThumbnailImage' => $booking->getCustomerUser()->getShop()->getPhotoThumbnailWebLink()
            );
        }

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/completed_booking/list")
     */
    public function getDeliveryCompletedBookingListAction(Request $request)
    {
        $results = array();
        $bookings = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookings = $this->getDoctrine()->getRepository('ApiBundle:Booking')->getCompletedBookingListByDeliveryStaff($user);

        foreach ($bookings as $booking) {
            $results[$booking->getId()]['booking'] = array(
                'bookingId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bookingPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerUserId' => $booking->getCustomerUser()->getId(),
                'customerShopName' => $booking->getCustomerUser()->getShop()->getName(),
                'customerShopThumbnailImage' => $booking->getCustomerUser()->getShop()->getPhotoThumbnailWebLink()
            );
        }

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/booking/info")
     */
    public function getBookingInfoAction(Request $request) {
        $bookingId = null;
        $customerUserId = null;
        $deliveryStaffCompanyId = null;
        $result = array();
        $user = null;
        $goodCategoryName = '';
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookingId = $request->headers->get('bookingId');
        $customerUserId = $request->headers->get('customerUserId');
        $deliveryStaffCompanyId = $user->getStaffCompany()->getId();

        $booking = $this->getDoctrine()->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'customerUser' => $customerUserId,
            'deliveryStaffCompany' => $deliveryStaffCompanyId
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isGetBookingInfo' => false
            ),Response::HTTP_BAD_REQUEST);
        }


        $result = array(
            'bId' => $booking->getId(),
            'number' => $booking->getNumber(),
            'cId' => $booking->getSellerCompany()->getId(),
            'createdDate' => $booking->getCreatedDate()->format('d-m-Y H:i:s'),
            'deliveryDate' => $booking->getDeliveryDate()->format('d-m-Y H:i:s'),
            'bPrice' => $bookingService->calculateTotalPrice($booking, false),
            'bFinalPrice' => $bookingService->calculateTotalPrice($booking, true),
            'customerUserId' => $booking->getCustomerUser()->getId(),
            'customerAddress' => $booking->getCustomerUser()->getShop()->getAddress(),
            'customerLatitude' => $booking->getCustomerUser()->getShop()->getLatitude(),
            'customerLongitude' => $booking->getCustomerUser()->getShop()->getLongitude(),
            'customerPhone' => $booking->getCustomerUser()->getShop()->getPhone(),
            'customerShopName' => $booking->getCustomerUser()->getShop()->getName(),
            'customerShopImage' => $booking->getCustomerUser()->getShop()->getPhotoWebLink(),
            'bStatus' => $booking->getStatus()
        );

        foreach ($booking->getBookingItem() as $bookingItem) {
            $goodCategoryName = '';
            $goodCategoryName = $bookingItem->getGoodCategory()? $bookingItem->getGoodCategory()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestOne()? '->'.$bookingItem->getGoodCategory()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestTwo()? '->'.$bookingItem->getGoodCategoryNestTwo()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestThree()? '->'.$bookingItem->getGoodCategoryNestThree()->getName() : '';

            $result['bookingItems'][] = array(
                'goodName' => $bookingItem->getGood()->getName(),
                'goodCategoryName' => $goodCategoryName,
                'pricePerOne' => $bookingItem->getPricePerOne(),
                'amount' => $bookingItem->getAmount(),
                'bookingItemPrice' => $bookingService->calculatePriceByAmount($bookingItem->getPricePerOne(), $bookingItem->getAmount()),
                'received' => $bookingItem->getReceived()
            );
        }


        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/booking/list/map")
     */
    public function getBookingListOnMap(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');
        $result = array();

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $statuses = [Booking::ACCEPTED, Booking::IN_PROGRESS];

        /** @var Booking[] $bookings */
        $bookings = $em->getRepository('ApiBundle:Booking')->getListForMapByDeliveryStaffAndStatuses($user, $statuses);

        foreach ($bookings as $booking) {
            $result[$booking->getId()] = array(
                'bId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerUserId' => $booking->getCustomerUser()->getId(),
                'customerAddress' => $booking->getCustomerUser()->getShop()->getAddress(),
                'customerLatitude' => $booking->getCustomerUser()->getShop()->getLatitude(),
                'customerLongitude' => $booking->getCustomerUser()->getShop()->getLongitude(),
                'customerPhone' => $booking->getCustomerUser()->getShop()->getPhone(),
                'customerShopName' => $booking->getCustomerUser()->getShop()->getName(),
                'customerShopImage' => $booking->getCustomerUser()->getShop()->getPhotoWebLink(),
                'bStatus' => $booking->getStatus()
            );

            /** @var BookingItem $bookingItem */
            foreach ($booking->getBookingItem() as $bookingItem) {
                $result[$booking->getId()]['bookingItems'][] = array(
                    'goodName' => $bookingItem->getGood()->getName(),
                    'amount' => $bookingItem->getAmount(),
                    'bookingItemPrice' => $bookingService->calculatePriceByAmount($bookingItem->getPricePerOne(), $bookingItem->getAmount())
                );
            }
        }

        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

}
