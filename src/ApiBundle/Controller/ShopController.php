<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Shop;
use ApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @package ApiBundle\Controller
 * @Route("/api/shop", name="api_shop_")
 */
class ShopController extends FOSRestController
{
    /**
     * @Rest\Post("/update/profile")
     */
    public function postUpdateProfileAction(Request $request)
    {
        $temporarySecretKey = null;
        $phone = null;
        $name = null;
        $address = null;
        $profileImage = null;
        $profileInfo = array();
        $fileService = $this->get('app.file_service');

        /** @var User $user */
        $user = null;

        /** @var Shop $shop */
        $shop = null;

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $name = $request->get('shop_name');
        $phone = $request->get('shop_phone');
        $address = $request->get('shop_address');
        $profileImage = $request->files->get('file');

        $shop = $user->getShop();

        if (!$shop) {
            return new JsonResponse(array(
                "message" => "User has not shop or use has different special role.",
                'isShop' => false,
                "isUpdated" => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $shop->setName($name);
        $shop->setPhone($phone);
        $shop->setAddress($address);
        $shop->setUpdateDate(new \DateTime('now'));

        if ($profileImage) {
            $shop->setFile($profileImage);
            $shop->saveFile();

            $result = false;
            $size = '256x';
            $thumbnailSize = '128x';

            $result = $fileService->compressImageFile($shop->getPhotoWebLink(), $shop->getPhotoThumbnailWebLink(), $thumbnailSize);

            if ($result) {
                $result = $fileService->compressImageFile($shop->getPhotoWebLink(), $shop->getPhotoWebLink(), $size);
            }
        }

        $em->persist($shop);
        $em->flush();

        $profileInfo = array(
            'name' => $shop->getName(),
            'image' => $shop->getPhotoWebLink(),
            'address' => $shop->getAddress(),
            'phone' => $shop->getPhone(),
            'username' => $user->getUsername()
        );

        return new JsonResponse(array(
            "isUpdated" => true,
            "isShop" => true,
            "message" => 'Shop successfully updated.',
            'profileInfo' => $profileInfo
        ),Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/booking/list")
     */
    public function getBookingListAction(Request $request)
    {
        $results = array();
        $bookings = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookings = $this->getDoctrine()->getRepository('ApiBundle:Booking')->getBookingListByCustomer($user);

        foreach ($bookings as $booking) {
            $results[$booking->getId()]['booking'] = array(
                'bookingId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'createdDate' => $booking->getCreatedDate()->format('d-m-Y H:i:s'),
                'deliveryDate' => $booking->getDeliveryDate()->format('d-m-Y H:i:s'),
                'companyName' => $booking->getSellerCompany()->getName(),
                'companyId' => $booking->getSellerCompany()->getId(),
                'isElected' => $booking->getIsElected(),
                'status' => $booking->getStatus()
            );
        }

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/booking/info")
     */
    public function getBookingInfoAction(Request $request) {
        $bookingId = null;
        $companyId = null;
        $result = array();
        $user = null;
        $goodCategoryName = '';
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $bookingId = $request->headers->get('bookingId');
        $companyId = $request->headers->get('companyId');

        $booking = $this->getDoctrine()->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $companyId,
            'customerUser' => $user->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isGetBookingInfo' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $result = array(
            'bookingId' => $booking->getId(),
            'number' => $booking->getNumber(),
            'price' => $bookingService->calculateTotalPrice($booking, false),
            'finalPrice' => $bookingService->calculateTotalPrice($booking, true),
            'createdDate' => $booking->getCreatedDate()->format('d-m-Y H:i:s'),
            'deliveryDate' => $booking->getDeliveryDate()->format('d-m-Y H:i:s'),
            'companyName' => $booking->getSellerCompany()->getName(),
            'status' => $booking->getStatus()
        );

        foreach ($booking->getBookingItem() as $bookingItem) {
            $goodCategoryName = '';
            $goodCategoryName = $bookingItem->getGoodCategory()? $bookingItem->getGoodCategory()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestOne()? '->'.$bookingItem->getGoodCategory()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestTwo()? '->'.$bookingItem->getGoodCategoryNestTwo()->getName() : '';
            $goodCategoryName .= $bookingItem->getGoodCategoryNestThree()? '->'.$bookingItem->getGoodCategoryNestThree()->getName() : '';

            $result['bookingItems'][] = array(
                'biId' => $bookingItem->getId(),
                'gId' => $bookingItem->getGood()->getId(),
                'gName' => $bookingItem->getGood()->getName(),
                'cId' => $bookingItem->getCompany()->getId(),
                'gcId' => $bookingItem->getGoodCategory()->getId(),
                'gcnOneId' => $bookingItem->getGoodCategoryNestOne()? $bookingItem->getGoodCategoryNestOne()->getId() : null,
                'gcnTwoId' => $bookingItem->getGoodCategoryNestTwo()? $bookingItem->getGoodCategoryNestTwo()->getId() : null,
                'gcnThreeId' => $bookingItem->getGoodCategoryNestThree()? $bookingItem->getGoodCategoryNestThree()->getId() : null,
                'gcName' => $goodCategoryName,
                'biPricePerOne' => $bookingItem->getPricePerOne(),
                'biAmount' => $bookingItem->getAmount(),
                'biReceived' => $bookingItem->getReceived()
            );
        }

        if ($booking->getDeliveryStaffCompany()) {
            $result['deliverymanInfo'] = array(
                'firstName' => $booking->getDeliveryStaffCompany()->getFirstName(),
                'secondName' => $booking->getDeliveryStaffCompany()->getSecondName(),
                'lastName' => $booking->getDeliveryStaffCompany()->getLastName(),
                'image' => $booking->getDeliveryStaffCompany()->getPhotoWebLink(),
                'phone' => $booking->getDeliveryStaffCompany()->getPhone(),
                'companyName' => $booking->getDeliveryStaffCompany()->getCompany()->getName()
            );
        }

        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Post("/add_remove/elected/booking")
     */
    public function postAddRemoveElectedBookingAction(Request $request)
    {
        $user = null;
        $booking = null;
        $message = '';
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $request->get('bookingId'),
            'sellerCompany' => $request->get('companyId'),
            'customerUser' => $user->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isElected' => false,
                'bookingId' => null
            ),Response::HTTP_BAD_REQUEST);
        }

        $booking->setIsElected(!$booking->getIsElected());

        $em->persist($booking);
        $em->flush();

        $message = $booking->getIsElected()? "Booking is added to elected list" : "Booking is removed from elected list";

        return new JsonResponse(array(
            'message' => $message,
            'isElected' => $booking->getIsElected(),
            'bookingId' => $booking->getId()
        ),Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/elected/booking/list")
     */
    public function getElectedBookingListAction(Request $request)
    {
        $user = null;
        $results = array();
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $bookingService = $this->get('app.booking_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $bookings = $this->getDoctrine()->getRepository('ApiBundle:Booking')->getElectedBookingListByCustomer($user);

        foreach ($bookings as $booking) {
            $results[$booking->getId()]['booking'] = array(
                'bookingId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'createdDate' => $booking->getCreatedDate()->format('d-m-Y H:i:s'),
                'deliveryDate' => $booking->getDeliveryDate()->format('d-m-Y H:i:s'),
                'companyName' => $booking->getSellerCompany()->getName(),
                'companyId' => $booking->getSellerCompany()->getId(),
                'isElected' => $booking->getIsElected(),
                'status' => $booking->getStatus()
            );
        }

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

}
