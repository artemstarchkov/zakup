<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\BookingItem;
use ApiBundle\Entity\HistoryBooking;
use ApiBundle\Entity\HistoryBookingItem;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package ApiBundle\Controller
 * @Route("/api/booking_item", name="api_booking_item_")
 */
class BookingItemController extends FOSRestController
{
    /**
     * @Rest\Post("/received")
     */
    public function postReceivedBookingItemAction(Request $request)
    {
        $data = null;
        $temporarySecretKey = null;
        $user = null;
        $isBookingCompleted = true;

        /** @var Booking $booking */
        $booking = null;

        /** @var BookingItem $bookingItem */
        $bookingItem = null;

        /** @var HistoryBooking $historyBooking */
        $historyBooking = null;

        /** @var HistoryBookingItem $historyBookingItem */
        $historyBookingItem = null;

        $bookingItems = array();

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $data = array(
            'bookingItemId' => $request->get('bookingItemId'),
            'goodId' => $request->get('goodId'),
            'companyId' => $request->get('companyId'),
            'bookingId' => $request->get('bookingId'),
            'gcId' => $request->get('gcId'),
            'gcnOneId' => $request->get('gcnOneId'),
            'gcnTwoId' => $request->get('gcnTwoId'),
            'gcnThreeId' => $request->get('gcnThreeId'),
            'received' => $request->get('received')
        );

        $bookingItem = $em->getRepository('ApiBundle:BookingItem')->getBookingItemByFieldsData($data);

        if (!$bookingItem) {
            return new JsonResponse(array(
                'isBookingCompleted' => false,
                "message" => "Booking item not found",
                'isBookingItemUpdate' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $booking = $bookingItem->getBooking();

        if ($booking->getStatus() == Booking::COMPLETED) {
            return new JsonResponse(array(
                'bookingStatus' => $booking->getStatus(),
                'isBookingCompleted' => $isBookingCompleted,
                "message" => 'Booking is completed yet'
            ),Response::HTTP_OK);
        }

        $historyBookingItem = $em->getRepository('ApiBundle:HistoryBookingItem')->findOneByBookingItem($bookingItem);

        $bookingItem->setReceived($data['received']);
        $historyBookingItem->setReceived($data['received']);

        $em->persist($bookingItem);
        $em->persist($historyBookingItem);
        $em->flush();

        $historyBooking = $historyBookingItem->getHistoryBooking();

        $bookingItems = $em->getRepository('ApiBundle:BookingItem')->findBy(array(
            'booking' => $bookingItem->getBooking()->getId(),
            'company' => $bookingItem->getCompany()->getId()
        ));

        foreach ($bookingItems as $item) {
            if (!$item->getReceived()) {
                $isBookingCompleted = false;
            }
        }

        if ($isBookingCompleted) {
            $booking->setStatus(Booking::COMPLETED);
        } else {
            $booking->setStatus(Booking::IN_PROGRESS);
        }

        $historyBooking->setStatus($booking->getStatus());

        $em->persist($booking);
        $em->persist($historyBooking);
        $em->flush();

        return new JsonResponse(array(
            'isBookingCompleted' => $isBookingCompleted,
            "isBookingItemUpdate" => true,
            'bookingStatus' => $booking->getStatus(),
            "message" => 'Booking item successfully updated.'
        ),Response::HTTP_OK);
    }

}
