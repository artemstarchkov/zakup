<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class StaffController
 * @package AppBundle\ActionResponse
 * @Route("/api", name="api")
 */
class ApiController extends Controller
{

}
