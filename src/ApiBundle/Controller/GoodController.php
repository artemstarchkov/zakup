<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @package ApiBundle\Controller
 * @Route("/api/good", name="api_good_")
 */
class GoodController extends FOSRestController
{
    /**
     * @Rest\Get("/list")
     */
    public function getGoodListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $preparedResult = array();
        $categories = array();
        $goods = array();
        $goodCategories = array();
        $goodCategoriesNestOne = array();
        $goodCategoriesNestTwo = array();
        $goodCategoriesNestThree = array();
        $company = null;
        $shopConfiguration = null;

        /** @var Shop|null $shop */
        $shop = null;
        $company = $em->getRepository('ApiBundle:Company')->find($request->headers->get('companyId'));
        $temporarySecretKey = $request->headers->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $shop = $user->getShop();

        $categories = array(
            'gcId' => $request->headers->get('gcId'),
            'gcnOneId' => $request->headers->get('gcnOneId'),
            'gcnTwoId' => $request->headers->get('gcnTwoId'),
            'gcnThreeId' => $request->headers->get('gcnThreeId'),
        );

        // Find "goodCategories" by "company"
        if (!$categories['gcId'] && !$categories['gcnOneId'] && !$categories['gcnTwoId'] && !$categories['gcnThreeId']) {
            $goodCategories = $em->getRepository('ApiBundle:GoodCategory')->findByCompany($company);

            foreach ($goodCategories as $goodCategory) {
                $preparedResult['goodCategory'][] = array(
                    'id' => $goodCategory['id'],
                    'name' => $goodCategory['name']
                );
            }
        }

        // Find "goodCategoriesNestOne" by "company" and "goodCategoryId"
        if ($categories['gcId'] && !$categories['gcnOneId'] && !$categories['gcnTwoId'] && !$categories['gcnThreeId']) {

            // Return "goodCategoriesNestOne" and goodCategory's goods
            $goodCategoriesNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->findByCompanyAndGoodCategory($company, $categories);
            $goods = $em->getRepository('ApiBundle:Good')->findByCompanyAndCategories($company, $categories);

            $preparedResult['goodCategoryNestOne'] = $goodCategoriesNestOne;
            $preparedResult['goods'] = $goods;
        }

        // Find "goodCategoriesNestTwo" by "company" and "goodCategoryId" and "goodCategoriesNestOne"
        if ($categories['gcId'] && $categories['gcnOneId'] && !$categories['gcnTwoId'] && !$categories['gcnThreeId']) {

            // Return "goodCategoriesNestTwo" and goodCategoryNestOne's goods
            $goodCategoriesNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findByCompanyAndGoodCategoryNestOne($company, $categories);
            $goods = $em->getRepository('ApiBundle:Good')->findByCompanyAndCategories($company, $categories);

            $preparedResult['goodCategoriesNestTwo'] = $goodCategoriesNestTwo;
            $preparedResult['goods'] = $goods;
        }

        // Find "goodCategoriesNestThree" by "company" and "goodCategoryId" and "goodCategoriesNestOne" and "goodCategoriesNestTwo"
        if ($categories['gcId'] && $categories['gcnOneId'] && $categories['gcnTwoId'] && !$categories['gcnThreeId']) {

            // Return "goodCategoriesNestThree" and goodCategoryNestTwo's goods
            $goodCategoriesNestThree = $em->getRepository('ApiBundle:GoodCategoryNestThree')->findByCompanyAndGoodCategoryNestTwo($company, $categories);
            $goods = $em->getRepository('ApiBundle:Good')->findByCompanyAndCategories($company, $categories);

            $preparedResult['goodCategoriesNestThree'] = $goodCategoriesNestThree;
            $preparedResult['goods'] = $goods;
        }

        // Find goodCategoryNestThree's goods
        if ($categories['gcId'] && $categories['gcnOneId'] && $categories['gcnTwoId'] && $categories['gcnThreeId']) {
            $goods = $em->getRepository('ApiBundle:Good')->findByCompanyAndCategories($company, $categories);
            $preparedResult['goods'] = $goods;
        }

        $shopConfiguration = $em->getRepository('ApiBundle:ShopConfiguration')->getForGoodListByShopAndCompany($shop, $company);
        $preparedResult['shopConfiguration'] = $shopConfiguration;

        $response = new JsonResponse($preparedResult,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

}
