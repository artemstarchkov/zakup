<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\BookingItem;
use ApiBundle\Entity\Consignment;
use ApiBundle\Entity\HistoryBooking;
use ApiBundle\Entity\HistoryBookingItem;
use DateTime;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package ApiBundle\Controller
 * @Route("/api/booking", name="api_booking_")
 */
class BookingController extends FOSRestController
{
    /**
     * @Rest\Post("/create")
     */
    public function postCreateBookingAction(Request $request)
    {
        $temporarySecretKey = null;
        $user = null;
        $createdDateStr = '';
        $createdDate = null;
        $deliveryDate = null;
        $companies = array();

        $bookingEntity = null;
        $bookingItemEntity = null;
        $historyBookingEntity = null;
        $historyBookingItemEntity = null;
        $historyBookingItemCategoryName = '';
        $bookingNumber = '';

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');
        $createdDateStr = $request->get('createdDate');
        $companies = $request->get('companies');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $companies = json_decode($companies, true);

        $createdDate = DateTime::createFromFormat('d-m-Y H:i:s', $createdDateStr);
        $deliveryDate = DateTime::createFromFormat('d-m-Y H:i:s', $createdDateStr)->modify('+1 day');

        foreach ($companies as $company) {
            $number = $em->getRepository('ApiBundle:Booking')->createQueryBuilder('b')
                ->select('MAX(b.countNumber) as countNumber')
                ->where('b.sellerCompany = :sellerCompany')
                ->andWhere('b.customerUser = :user')
                ->setParameter('sellerCompany', $company['cId'])
                ->setParameter('user', $user->getId())
                ->setMaxResults(1)
                ->getQuery()->getOneOrNullResult();

            $bookingNumber = $number && $number['countNumber'] ? ($number['countNumber'] + 1) : 1;

            $bookingEntity = new Booking();
            $bookingEntity->setCreatedDate($createdDate);
            $bookingEntity->setDeliveryDate($deliveryDate);
            $bookingEntity->setUpdatedDate($deliveryDate);
            $bookingEntity->setCustomerUser($user);
            $bookingEntity->setSellerCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
            $bookingEntity->setStatus(Booking::NEW);
            $bookingEntity->setNumber($company['cId'] . '_' . $bookingNumber);
            $bookingEntity->setCountNumber($bookingNumber);

            $historyBookingEntity = new HistoryBooking();
            $historyBookingEntity->setCreatedDate($createdDate);
            $historyBookingEntity->setDeliveryDate($deliveryDate);
            $historyBookingEntity->setUpdatedDate($deliveryDate);
            if ($user->getShop()) {
                $historyBookingEntity->setCustomerUserName($user->getShop()->getName());
            }
            if ($bookingEntity->getSellerCompany()) {
                $historyBookingEntity->setSellerCompanyName($bookingEntity->getSellerCompany()->getName());
            }
            $historyBookingEntity->setStatus(Booking::NEW);
            $historyBookingEntity->setBooking($bookingEntity);
            $historyBookingEntity->setSellerCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
            $historyBookingEntity->setBookingNumber($company['cId'] . '_' . $bookingNumber);

            $em->persist($bookingEntity);
            $em->persist($historyBookingEntity);


            foreach ($company['products'] as $product) {
                $bookingItemEntity = new BookingItem();
                $bookingItemEntity->setGood($em->getRepository('ApiBundle:Good')->find($product['gId']));
                $bookingItemEntity->setGoodCategory($em->getRepository('ApiBundle:GoodCategory')->find($product['gcId']));
                $bookingItemEntity->setGoodCategoryNestOne(isset($product['gcnOneId']) && $product['gcnOneId'] ? $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($product['gcnOneId']) : null);
                $bookingItemEntity->setGoodCategoryNestTwo(isset($product['gcnTwoId']) && $product['gcnTwoId'] ? $em->getRepository('ApiBundle:GoodCategoryNestTwo')->find($product['gcnTwoId']) : null);
                $bookingItemEntity->setGoodCategoryNestThree(isset($product['gcnThreeId']) && $product['gcnThreeId'] ? $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($product['gcnThreeId']) : null);
                $bookingItemEntity->setCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
                $bookingItemEntity->setPricePerOne($product['gPrice']);
                $bookingItemEntity->setAmount($product['gAmount']);
                $bookingItemEntity->setReturnCount($product['gReturnCount']);
                $bookingItemEntity->setReceived(false);
                $bookingItemEntity->setBooking($bookingEntity);

                /** @var Consignment|null $consignment */
                $consignment = null;

                // Check if booking item marked as consignment
                if ($product['isConsignment']) {

                    // Calculate total price of debt
                    $totalPrice = ($product['gPrice'] * $product['gAmount']);

                    $consignment = new Consignment();
                    $consignment->setShop($user->getShop());
                    $consignment->setBookingItemAmount($product['gAmount']);
                    $consignment->setPricePerOne($product['gPrice']);
                    $consignment->setTotalPrice($totalPrice);
                    $consignment->setBalanceDebt($totalPrice);
                    $consignment->setIsClosed(false);
                }

                $historyBookingItemEntity = new HistoryBookingItem();

                if ($bookingItemEntity->getGood()) {
                    $historyBookingItemEntity->setGoodName($bookingItemEntity->getGood()->getName());
                    $historyBookingItemEntity->setGoodDescription($bookingItemEntity->getGood()->getDescription());
                    $historyBookingItemEntity->setGoodShortDescription($bookingItemEntity->getGood()->getShortDescription());
                }

                $historyBookingItemCategoryName = '';
                if ($bookingItemEntity->getGoodCategory()) {
                    $historyBookingItemCategoryName = $bookingItemEntity->getGoodCategory()->getName();
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestOne() ? '->' . $bookingItemEntity->getGoodCategoryNestOne()->getName() : '';
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestTwo() ? '->' . $bookingItemEntity->getGoodCategoryNestTwo()->getName() : '';
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestThree() ? '->' . $bookingItemEntity->getGoodCategoryNestThree()->getName() : '';

                    $historyBookingItemEntity->setGoodCategoryName($historyBookingItemCategoryName);
                }

                if ($bookingItemEntity->getCompany()) {
                    $historyBookingItemEntity->setCompanyName($bookingItemEntity->getCompany()->getName());
                }

                $historyBookingItemEntity->setPricePerOne($product['gPrice']);
                $historyBookingItemEntity->setAmount($product['gAmount']);
                $historyBookingItemEntity->setReturnCount($product['gReturnCount']);
                $historyBookingItemEntity->setHistoryBooking($historyBookingEntity);
                $historyBookingItemEntity->setBookingItem($bookingItemEntity);
                $historyBookingItemEntity->setReceived(false);

                if ($consignment) {
                    $em->persist($consignment);

                    $bookingItemEntity->setConsignment($consignment);
                }

                $em->persist($bookingItemEntity);
                $em->persist($historyBookingItemEntity);
            }
        }

        //Check if there is at least one important information missing
        if (!$bookingEntity->getSellerCompany() || !$bookingItemEntity->getGood()
            || !$bookingItemEntity->getGoodCategory() || !$bookingItemEntity->getCompany()
            || !$bookingItemEntity->getPricePerOne() || !$bookingItemEntity->getAmount()) {

            return new JsonResponse(array(
                'isCreated' => false,
                "message" => "There was an error when placing your order. There may not be enough data to process the order."
            ), Response::HTTP_BAD_REQUEST);
        }

        $em->flush();

        return new JsonResponse(array(
            "isCreated" => true,
            "message" => 'Booking successfully created.'
        ), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/repeat")
     */
    public function postRepeatBookingAction(Request $request)
    {
        $temporarySecretKey = null;
        $user = null;
        $createdDateStr = '';
        $createdDate = null;
        $deliveryDate = null;
        $companies = array();

        $bookingEntity = null;
        $bookingItemEntity = null;
        $historyBookingEntity = null;
        $historyBookingItemEntity = null;
        $historyBookingItemCategoryName = '';

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');
        $createdDateStr = $request->get('createdDate');
        $companies = $request->get('companies');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $companies = json_decode($companies, true);

        $createdDate = DateTime::createFromFormat('d-m-Y H:i:s', $createdDateStr);
        $deliveryDate = DateTime::createFromFormat('d-m-Y H:i:s', $createdDateStr)->modify('+1 day');

        foreach ($companies as $company) {
            $number = $em->getRepository('ApiBundle:Booking')->createQueryBuilder('b')
                ->select('MAX(b.countNumber) as countNumber')
                ->where('b.sellerCompany = :sellerCompany')
                ->andWhere('b.customerUser = :user')
                ->setParameter('sellerCompany', $company['cId'])
                ->setParameter('user', $user->getId())
                ->setMaxResults(1)
                ->getQuery()->getOneOrNullResult();

            $bookingNumber = $number && $number['countNumber'] ? ($number['countNumber'] + 1) : 1;

            $bookingEntity = new Booking();
            $bookingEntity->setCreatedDate($createdDate);
            $bookingEntity->setDeliveryDate($deliveryDate);
            $bookingEntity->setUpdatedDate($deliveryDate);
            $bookingEntity->setCustomerUser($user);
            $bookingEntity->setSellerCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
            $bookingEntity->setStatus(Booking::NEW);
            $bookingEntity->setNumber($company['cId'] . '_' . $bookingNumber);
            $bookingEntity->setCountNumber($bookingNumber);

            $historyBookingEntity = new HistoryBooking();
            $historyBookingEntity->setCreatedDate($createdDate);
            $historyBookingEntity->setDeliveryDate($deliveryDate);
            $historyBookingEntity->setUpdatedDate($deliveryDate);
            if ($user->getShop()) {
                $historyBookingEntity->setCustomerUserName($user->getShop()->getName());
            }
            if ($bookingEntity->getSellerCompany()) {
                $historyBookingEntity->setSellerCompanyName($bookingEntity->getSellerCompany()->getName());
            }
            $historyBookingEntity->setStatus(Booking::NEW);
            $historyBookingEntity->setBooking($bookingEntity);
            $historyBookingEntity->setSellerCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
            $historyBookingEntity->setBookingNumber($company['cId'] . '_' . $bookingNumber);

            $em->persist($bookingEntity);
            $em->persist($historyBookingEntity);

            foreach ($company['products'] as $product) {
                $bookingItemEntity = new BookingItem();
                $bookingItemEntity->setGood($em->getRepository('ApiBundle:Good')->find($product['gId']));
                $bookingItemEntity->setGoodCategory($em->getRepository('ApiBundle:GoodCategory')->find($product['gcId']));
                $bookingItemEntity->setGoodCategoryNestOne(isset($product['gcnOneId']) && $product['gcnOneId'] ? $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($product['gcnOneId']) : null);
                $bookingItemEntity->setGoodCategoryNestTwo(isset($product['gcnTwoId']) && $product['gcnTwoId'] ? $em->getRepository('ApiBundle:GoodCategoryNestTwo')->find($product['gcnTwoId']) : null);
                $bookingItemEntity->setGoodCategoryNestThree(isset($product['gcnThreeId']) && $product['gcnThreeId'] ? $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($product['gcnThreeId']) : null);
                $bookingItemEntity->setCompany($em->getRepository('ApiBundle:Company')->find($company['cId']));
                $bookingItemEntity->setPricePerOne($product['gPrice']);
                $bookingItemEntity->setAmount($product['gAmount']);
                $bookingItemEntity->setReturnCount($product['gReturnCount']);
                $bookingItemEntity->setReceived(false);
                $bookingItemEntity->setBooking($bookingEntity);

                /** @var Consignment|null $consignment */
                $consignment = null;

                // Check if booking item marked as consignment
                if ($product['isConsignment']) {

                    // Calculate total price of debt
                    $totalPrice = ($product['gPrice'] * $product['gAmount']);

                    $consignment = new Consignment();
                    $consignment->setShop($user->getShop());
                    $consignment->setBookingItemAmount($product['gAmount']);
                    $consignment->setPricePerOne($product['gPrice']);
                    $consignment->setTotalPrice($totalPrice);
                    $consignment->setBalanceDebt($totalPrice);
                    $consignment->setIsClosed(false);
                }

                $historyBookingItemEntity = new HistoryBookingItem();

                if ($bookingItemEntity->getGood()) {
                    $historyBookingItemEntity->setGoodName($bookingItemEntity->getGood()->getName());
                    $historyBookingItemEntity->setGoodDescription($bookingItemEntity->getGood()->getDescription());
                    $historyBookingItemEntity->setGoodShortDescription($bookingItemEntity->getGood()->getShortDescription());
                }

                $historyBookingItemCategoryName = '';
                if ($bookingItemEntity->getGoodCategory()) {
                    $historyBookingItemCategoryName = $bookingItemEntity->getGoodCategory()->getName();
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestOne() ? '->' . $bookingItemEntity->getGoodCategoryNestOne()->getName() : '';
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestTwo() ? '->' . $bookingItemEntity->getGoodCategoryNestTwo()->getName() : '';
                    $historyBookingItemCategoryName .= $bookingItemEntity->getGoodCategoryNestThree() ? '->' . $bookingItemEntity->getGoodCategoryNestThree()->getName() : '';

                    $historyBookingItemEntity->setGoodCategoryName($historyBookingItemCategoryName);
                }

                if ($bookingItemEntity->getCompany()) {
                    $historyBookingItemEntity->setCompanyName($bookingItemEntity->getCompany()->getName());
                }

                $historyBookingItemEntity->setPricePerOne($product['gPrice']);
                $historyBookingItemEntity->setAmount($product['gAmount']);
                $historyBookingItemEntity->setReturnCount($product['gReturnCount']);
                $historyBookingItemEntity->setHistoryBooking($historyBookingEntity);
                $historyBookingItemEntity->setBookingItem($bookingItemEntity);
                $historyBookingItemEntity->setReceived(false);

                if ($consignment) {
                    $em->persist($consignment);

                    $bookingItemEntity->setConsignment($consignment);
                }

                $em->persist($bookingItemEntity);
                $em->persist($historyBookingItemEntity);
            }
        }

        //Check if there is at least one important information missing
        if (!$bookingEntity->getSellerCompany() || !$bookingItemEntity->getGood()
            || !$bookingItemEntity->getGoodCategory() || !$bookingItemEntity->getCompany()
            || !$bookingItemEntity->getPricePerOne() || !$bookingItemEntity->getAmount()) {

            return new JsonResponse(array(
                'isRepeated' => false,
                "message" => "There was an error when placing your order. There may not be enough data to process the order. Booking error repeated"
            ), Response::HTTP_BAD_REQUEST);
        }

        $em->flush();

        return new JsonResponse(array(
            "isRepeated" => true,
            "message" => 'Booking successfully repeated.'
        ), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/complete/by/deliveryman")
     */
    public function postCompleteBookingByDeliveryManAction(Request $request)
    {
        $temporarySecretKey = null;
        $user = null;
        $companyId = null;
        $bookingId = null;

        /** @var Booking $booking */
        $booking = null;

        /** @var HistoryBooking $historyBooking */
        $historyBooking = null;

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $companyId = $request->get('companyId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $companyId,
            'deliveryStaffCompany' => $user->getStaffCompany()->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isCompletedBooking' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $booking->setStatus(Booking::COMPLETED);
        $booking->setUpdatedDate(new \DateTime('now'));

        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneByBooking($booking);
        $historyBooking->setStatus(Booking::COMPLETED);
        $historyBooking->setUpdatedDate(new \DateTime('now'));

        $em->persist($booking);
        $em->persist($historyBooking);
        $em->flush();

        return new JsonResponse(array(
            "message" => "Booking completed successfully",
            'isCompletedBooking' => true
        ), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/in_progress/by/deliveryman")
     */
    public function postInProgressBookingByDeliveryManAction(Request $request)
    {
        $temporarySecretKey = null;
        $user = null;
        $companyId = null;
        $bookingId = null;

        /** @var Booking $booking */
        $booking = null;

        /** @var HistoryBooking $historyBooking */
        $historyBooking = null;

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $companyId = $request->get('companyId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $companyId,
            'deliveryStaffCompany' => $user->getStaffCompany()->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isInProgressBooking' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $booking->setStatus(Booking::IN_PROGRESS);
        $booking->setUpdatedDate(new \DateTime('now'));

        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneByBooking($booking);
        $historyBooking->setStatus(Booking::IN_PROGRESS);
        $historyBooking->setUpdatedDate(new \DateTime('now'));

        $em->persist($booking);
        $em->persist($historyBooking);
        $em->flush();

        return new JsonResponse(array(
            "message" => "Booking in progress",
            'isInProgressBooking' => true
        ), Response::HTTP_OK);
    }

    /**
     * Delete booking by customer user
     *
     * @Rest\Post("/delete")
     */
    public function deleteBookingAction(Request $request)
    {
        $temporarySecretKey = null;
        $user = null;
        $companyId = null;
        $bookingId = null;

        /** @var Booking $booking */
        $booking = null;

        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $companyId = $request->get('companyId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $companyId,
            'customerUser' => $user->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array(
                "message" => "Booking not found",
                'isInProgressBooking' => false
            ), Response::HTTP_BAD_REQUEST);
        }

        $booking->setIsDeleted(true);

        $em->persist($booking);
        $em->flush();

        return new JsonResponse(array(
            "message" => "Booking successfully deleted",
            'isDeletedBooking' => true
        ), Response::HTTP_OK);
    }

}
