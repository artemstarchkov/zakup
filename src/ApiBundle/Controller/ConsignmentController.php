<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Consignment;
use ApiBundle\Entity\ConsignmentPaid;
use ApiBundle\Entity\Shop;
use ApiBundle\Entity\StaffCompany;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @package ApiBundle\Controller
 * @Route("/api/consignment", name="api_consignment_")
 */
class ConsignmentController extends Controller
{
    /**
     * Return list of shops that have booking with bookingItem that under consignment
     *
     * @Rest\Get("/delivery-staff/shop/list")
     */
    public function getConsignmentShopList(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $result = array();
        $consignmentShops = array();

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user->getStaffCompany()) {
            $consignmentShops = $em->getRepository('ApiBundle:Booking')->getShopListByStaffAndBookingItemIsConsignment($user->getStaffCompany(), true, false);
        } else {
            return new JsonResponse(array(
                "message" => "User not staff company",
                'secretKey' => true
            ),Response::HTTP_BAD_REQUEST);
        }

        foreach ($consignmentShops as $consignmentShop) {
            $result[$consignmentShop['shopId']]['shopId'] = $consignmentShop['shopId'];
            $result[$consignmentShop['shopId']]['bookingId'] = $consignmentShop['bookingId'];
            $result[$consignmentShop['shopId']]['bookingNumber'] = $consignmentShop['bookingNumber'];
            $result[$consignmentShop['shopId']]['shopName'] = $consignmentShop['shopName'];
            $result[$consignmentShop['shopId']]['photoThumbnailWebLink'] = $consignmentShop['photoThumbnailWebLink'];
            $result[$consignmentShop['shopId']]['shopPhone'] = $consignmentShop['shopPhone'];

            // Check if not set value of debt for shop, then set it. Need to sum all debt by shop
            if (!isset($result[$consignmentShop['shopId']]['balanceDebt'])) {
                $result[$consignmentShop['shopId']]['balanceDebt'] = 0;
            }

            // Sum all debt by shop
            $result[$consignmentShop['shopId']]['balanceDebt'] += $consignmentShop['balanceDebt'];
        }

        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * Display detail info about consignments (debt) for shop
     *
     * @Rest\Get("/delivery-staff/shop/debt-info")
     */
    public function getConsignmentShopInfo(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');
        $consignments = array();
        $shopInfo = null;
        $result = array();
        $currentDebt = 0;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        /** @var StaffCompany|null $staffCompany */
        $staffCompany = $user->getStaffCompany();

        /** @var Shop|null $shop */
        $shop = $em->getRepository('ApiBundle:Shop')->find($request->headers->get('shopId'));

        if (!$staffCompany || !$shop) {
            return new JsonResponse(array(
                "message" => "User is not staff of the company or shop not found",
                'secretKey' => true
            ),Response::HTTP_BAD_REQUEST);
        }

        $consignments = $em->getRepository('ApiBundle:BookingItem')->getIsConsignmentBookingItemsForShopByDeliveryStaff($shop, $staffCompany, true, false);

        // Calculate total current shop's balance debt
        foreach ($consignments as $consignment) {
            $currentDebt += $consignment['balanceDebt'];
        }

        $shopInfo = [
            'name' => $shop->getName(),
            'phone' => $shop->getPhone(),
            'photoWebLink' => $shop->getPhotoWebLink(),
            'currentDebt' => $currentDebt
        ];

        $result = [
            'shopInfo' => $shopInfo,
            'consignments' => $consignments
        ];

        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Post("/paid/debt")
     */
    public function postPaidDebt(Request $request)
    {
        $user = null;
        $paymentAmount = 0;
        $consignmentId = null;
        $shopId = null;

        /** @var Consignment|null $consignment */
        $consignment = null;

        /** @var StaffCompany|null $staffCompany */
        $staffCompany = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                "isPaidDebt" => false,
                "isClosed" => false,
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $staffCompany = $user->getStaffCompany();
        $paymentAmount = $request->get('paymentAmount');
        $consignmentId = $request->get('consignmentId');

        /** @var Shop|null $shop */
        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('shopId'));

        if (!$consignmentId || !$staffCompany || !$paymentAmount || !$shop) {
            return new JsonResponse(array(
                "message" => "Bad parameters for paid debt",
                "isPaidDebt" => false,
                "isClosed" => false,
                'secretKey' => true
            ),Response::HTTP_BAD_REQUEST);
        }

        $consignment = $em->getRepository('ApiBundle:Consignment')->getOneToPaidDebt($consignmentId, $shop, $staffCompany, false);

        if (!$consignment) {
            return new JsonResponse(array(
                "message" => "Consignment not found",
                "isPaidDebt" => false,
                "isClosed" => false,
                'secretKey' => true
            ),Response::HTTP_BAD_REQUEST);
        }

        if (($consignment->getBalanceDebt() - $paymentAmount) < 0) {
            return new JsonResponse(array(
                "message" => "Payment amount more then balance debt",
                "isPaidDebt" => false,
                "isClosed" => false,
                'secretKey' => true
            ),Response::HTTP_BAD_REQUEST);
        }

        // Calculate current balance debt
        $balanceDebt = ($consignment->getBalanceDebt() - $paymentAmount);

        $consignment->setBalanceDebt($balanceDebt);
        if ($balanceDebt === 0) {
            $consignment->setIsClosed(true);
        }

        $consignment->setUpdateDate(new \DateTime('now'));

        // Create "consignment paid" for "consignment"
        $consignmentPaid = new ConsignmentPaid();
        $consignmentPaid->setPaidDebt($paymentAmount);
        $consignmentPaid->setConsignment($consignment);

        $em->persist($consignment);
        $em->persist($consignmentPaid);
        $em->flush();

        return new JsonResponse(array(
            "message" => "Successful payment debt",
            "isPaidDebt" => true,
            "isClosed" => $consignment->getIsClosed(),
            "balanceDebt" => $consignment->getBalanceDebt(),
            "secretKey" => true
        ),Response::HTTP_OK);
    }

}
