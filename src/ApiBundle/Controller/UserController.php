<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\Shop;
use ApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * @package ApiBundle\Controller
 * @Route("/api/user", name="api_user_")
 */
class UserController extends FOSRestController
{
    /**
     * @Rest\Post("/login")
     */
    public function postLoginAction(Request $request)
    {
        $login = null;
        $password = null;
        $user = null;
        $userProfileInfo = array();
        $bool = false;
        $responseResult = array();
        $em = $this->getDoctrine()->getManager();

        $login = $request->get('login');
        $password = $request->get('password');

        /** @var User $user */
        $user = $em->getRepository('ApiBundle:User')->findByUsernameOrEmail($login);

        if (!$user) {
            return new JsonResponse(array(
                "message" => "Authorization data is incorrect",
                'login' => false,
                'password' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($user);

        $bool = ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) ? true : false;

        if ($bool) {
            $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());

            // For older versions of Symfony, use security.context here
            $this->get("security.token_storage")->setToken($token);

            // Fire the login event
            // Logging the user in above the way we do it doesn't do this automatically
            $event = new InteractiveLoginEvent($request, $token);
            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            $bytes = random_bytes(20);
            $hexKey = bin2hex($bytes);
            $user->setTemporarySecretKey($hexKey);

            $em->persist($user);
            $em->flush();

            // Check if login user is not under "super user role"
            if (!$user->isSuperAdmin()) {
                if ($user->getSpecialRole() == User::ROLE_SHOP) {
                    $userProfileInfo = array(
                        'name' => $user->getShop()->getName(),
                        'image' => $user->getShop()->getPhotoWebLink(),
                        'address' => $user->getShop()->getAddress(),
                        'phone' => $user->getShop()->getPhone(),
                        'username' => $user->getUsername()
                    );
                } elseif ($user->getSpecialRole() == User::ROLE_STAFF_COMPANY) {
                    $userProfileInfo = array(
                        'firstName' => $user->getStaffCompany()->getFirstName(),
                        'secondName' => $user->getStaffCompany()->getSecondName(),
                        'lastName' => $user->getStaffCompany()->getLastName(),
                        'image' => $user->getStaffCompany()->getPhotoWebLink(),
                        'address' => $user->getStaffCompany()->getAddress(),
                        'phone' => $user->getStaffCompany()->getPhone(),
                        'companyName' => $user->getStaffCompany()->getCompany()->getName()
                    );
                }

                $responseResult = array(
                    "message" => "Authorization data is correct",
                    'login' => true,
                    'password' => true,
                    'role' => $user->getSpecialRole(),
                    'secretKey' => $hexKey,
                    'userProfile' => $userProfileInfo
                );
            } else {
                $responseResult = array(
                    "message" => "Authorization data is correct",
                    'login' => true,
                    'password' => true,
                    'role' => $user->getRoles(),
                    'secretKey' => $hexKey,
                    'userProfile' => $userProfileInfo
                );
            }

            $response = new JsonResponse($responseResult,Response::HTTP_OK);

            // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;
        } else {
            $responseResult = array(
                "message" => "Authorization data is incorrect",
                'login' => true,
                'password' => false
            );

            return new JsonResponse($responseResult,Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/logout")
     */
    public function postLogoutAction(Request $request) {
        $temporarySecretKey = null;
        $user = null;
        $em = $this->getDoctrine()->getManager();

        $temporarySecretKey = $request->get('secretKey');
        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
           'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();
        $user->setTemporarySecretKey(null);

        $em->persist($user);
        $em->flush();

        return new JsonResponse(array(
            "message" => "User successfully logout",
            'secretKey' => true
        ),Response::HTTP_OK);
    }
}
