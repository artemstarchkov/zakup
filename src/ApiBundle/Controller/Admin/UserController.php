<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10/24/18
 * Time: 9:13 PM
 */

namespace ApiBundle\Controller\Admin;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\Shop;
use ApiBundle\Entity\StaffCompany;
use ApiBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @package ApiBundle\Controller
 * @Route("/api/admin/user", name="api_admin_user_")
 */
class UserController extends FOSRestController
{
    /**
     * @Rest\Get("/list")
     */
    public function getListAction(Request $request)
    {
        $results = array();
        $shops = array();
        $companies = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        $shops = $em->getRepository('ApiBundle:Shop')->getAllInfo();
        $companies = $em->getRepository('ApiBundle:Company')->getAllInfo();

        $results = array_merge($shops, $companies);

        usort($results, function ($a, $b) {
            if ($a['createDate'] == $b['createDate']) {
                return 0;
            }

            return ($a['createDate'] > $b['createDate']) ? -1 : 1;
        });

        $response = new JsonResponse($results,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * @Rest\Get("/info")
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserInfo(Request $request) {
        $temporarySecretKey = $request->headers->get('secretKey');
        $em = $this->getDoctrine()->getManager();
        $result = null;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        $id = $request->headers->get('id');

        /** @var User|null $specUser */
        $specUser = null;

        $specUser = $em->getRepository('ApiBundle:User')->find($id);

        if ($specUser->getShop()) {
            $result = $em->getRepository('ApiBundle:Shop')->getInfoByOne($specUser->getShop());
        } elseif ($specUser->getCompany()) {
            $result = $em->getRepository('ApiBundle:Company')->getInfoByOne($specUser->getCompany());
        } else {
            return new JsonResponse(array(
                "message" => "User has not any special role",
                'hasRole' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $response = new JsonResponse($result,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * Create new user with special role (ROLE_SHOP/ROLE_COMPANY)
     *
     * @Rest\Post("/register")
     */
    public function postRegisterAction(Request $request) {
        /** @var User $user */
        $user = null;

        /** @var Shop|Company|null $specUser */
        $specUser = null;

        $temporarySecretKey = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->get('secretKey');
        $fileService = $this->get('app.file_service');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        $specialRole = $request->get('specialRole');
        $username = $request->get('username');
        $password = $request->get('password');
        $name = $request->get('name');
        $email = $request->get('email');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $fileImage = $request->files->get('fileImage');
        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        $status = $request->get('status');

        if ($specialRole && $username && $password && $name && $address && $phone) {
            $manager = $this->get('fos_user.user_manager');

            $existingUser = $em->getRepository('ApiBundle:User')->findByUsernameOrEmail($username);

            if ($existingUser) {
                return new JsonResponse(array(
                    "message" => "User with same username exist in system",
                    'isExistUser' => true
                ),Response::HTTP_BAD_REQUEST);
            }

            $newUser = $manager->createUser();
            $newUser->setUsername($username);
            $newUser->setPlainPassword($password);
            $newUser->setSpecialRole($specialRole);
            $newUser->setEnabled(true);
            $manager->updateUser($newUser, false);

            if ($specialRole === User::ROLE_SHOP) {
                $specUser = new Shop();
            } elseif ($specialRole === User::ROLE_COMPANY) {
                $specUser = new Company();
            }

            $specUser->setStatus($status);
            $specUser->setName($name);
            $specUser->setEmail($email);
            $specUser->setAddress($address);
            $specUser->setPhone($phone);
            $specUser->setLatitude($latitude);
            $specUser->setLongitude($longitude);
            $specUser->setUser($newUser);


            if ($fileImage) {
                $specUser->setFile($fileImage);
                $specUser->saveFile();

                $result = false;
                $size = '256x';
                $thumbnailSize = '128x';

                $result = $fileService->compressImageFile($specUser->getPhotoWebLink(), $specUser->getPhotoThumbnailWebLink(), $thumbnailSize);

                if ($result) {
                    $result = $fileService->compressImageFile($specUser->getPhotoWebLink(), $specUser->getPhotoWebLink(), $size);
                }
            }

            $em->persist($specUser);
            $em->flush();

            $profileInfo = array(
                'name' => $specUser->getName(),
                'image' => $specUser->getPhotoWebLink(),
                'address' => $specUser->getAddress(),
                'phone' => $specUser->getPhone(),
                'username' => $newUser->getUsername()
            );

            return new JsonResponse(array(
                "isCreated" => true,
                "isExistUser" => false,
                "isValidData" => true,
                "message" => 'User successfully created.',
                'profileInfo' => $profileInfo
            ), Response::HTTP_OK);
        } else {
            return new JsonResponse(array(
                "message" => "Required field should be fill",
                'isValidData' => false
            ),Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update user with special role (ROLE_SHOP/ROLE_COMPANY)
     *
     * @Rest\Post("/update")
     */
    public function update(Request $request) {
        /** @var User $user */
        $user = null;

        /** @var User $roleUser */
        $roleUser = null;

        /** @var Shop|Company|null $specUser */
        $specUser = null;
        $userInfo = null;

        $temporarySecretKey = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->get('secretKey');
        $fileService = $this->get('app.file_service');
        $manager = $this->get('fos_user.user_manager');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        $specUserId = $request->get('id');
        $specialRole = $request->get('specialRole');
        $username = $request->get('username');
        $name = $request->get('name');
        $email = $request->get('email');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $fileImage = $request->files->get('fileImage');
        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        $status = $request->get('status');

        if ($specialRole === User::ROLE_SHOP) {
            $specUser = $em->getRepository('ApiBundle:Shop')->find($specUserId);
        } elseif ($specialRole === User::ROLE_COMPANY) {
            $specUser = $em->getRepository('ApiBundle:Company')->find($specUserId);
        }

        if (!$specUser) {
            return new JsonResponse(array(
                "message" => "Special role entity is not found",
                'hasRole' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $roleUser = $specUser->getUser();

        $roleUser->setUsername($username);
        $manager->updateUser($roleUser, false);

        $specUser->setStatus($status);
        $specUser->setName($name);
        $specUser->setEmail($email);
        $specUser->setAddress($address);
        $specUser->setPhone($phone);
        $specUser->setLatitude($latitude);
        $specUser->setLongitude($longitude);
        $specUser->setUser($roleUser);
        $specUser->setUpdateDate(new \DateTime('now'));

        if ($fileImage) {
            $specUser->setFile($fileImage);
            $specUser->saveFile();

            $result = false;
            $size = '256x';
            $thumbnailSize = '128x';

            $result = $fileService->compressImageFile($specUser->getPhotoWebLink(), $specUser->getPhotoThumbnailWebLink(), $thumbnailSize);

            if ($result) {
                $result = $fileService->compressImageFile($specUser->getPhotoWebLink(), $specUser->getPhotoWebLink(), $size);
            }
        }

        $em->persist($specUser);
        $em->flush();

        if ($roleUser->getShop()) {
            $userInfo = $em->getRepository('ApiBundle:Shop')->getInfoByOne($roleUser->getShop());
        } elseif ($roleUser->getCompany()) {
            $userInfo = $em->getRepository('ApiBundle:Company')->getInfoByOne($roleUser->getCompany());
        } else {
            return new JsonResponse(array(
                "message" => "User has not any special role",
                'hasRole' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $response = new JsonResponse($userInfo,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }

    /**
     * Complete removal user
     *
     * @Rest\Post("/delete")
     */
    public function delete(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = null;
        $uId = $request->get('uId');
        $temporarySecretKey = $request->get('secretKey');

        /** @var Shop|Company|null $specialUser */
        $specialUser = null;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        /** @var User|null $user */
        $deleteUser = $em->getRepository('ApiBundle:User')->find($uId);

        if (!$deleteUser) {
            return new JsonResponse(array(
                "message" => "User to delete not found",
                'secretKey' => true,
                'isDeleted' => false,
                'hasRole' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($deleteUser->getSpecialRole() === User::ROLE_SHOP) {
            $specialUser = $deleteUser->getShop();
        } elseif ($deleteUser->getSpecialRole() === User::ROLE_COMPANY) {
            $specialUser = $deleteUser->getCompany();

            /** @var StaffCompany $staffCompany */
            foreach ($specialUser->getStaffCompany() as $staffCompany) {
                $staffCompany->removeUpload();
            }
        } else {
            return new JsonResponse(array(
                "message" => "Special role entity is not found",
                'secretKey' => true,
                'hasRole' => false,
                'isDeleted' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        $specialUser->removeUpload();
        $em->remove($specialUser);
        $em->remove($deleteUser);
        $em->flush();

        return new JsonResponse(array(
            "message" => "User was successfully deleted",
            'secretKey' => true,
            'hasRole' => true,
            'isDeleted' => true
        ),Response::HTTP_OK);
    }
}