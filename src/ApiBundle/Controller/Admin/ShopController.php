<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 10/24/18
 * Time: 9:08 PM
 */

namespace ApiBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @package ApiBundle\Controller
 * @Route("/api/admin/shop", name="api_admin_shop_")
 */
class ShopController extends FOSRestController
{
    /**
     * @Rest\Get("/list")
     */
    public function getListAction(Request $request)
    {
        $shops = array();
        $user = null;
        $em = $this->getDoctrine()->getManager();
        $temporarySecretKey = $request->headers->get('secretKey');

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'temporarySecretKey' => $temporarySecretKey
        ));

        if (!$user) {
            return new JsonResponse(array(
                "message" => "User not found",
                'secretKey' => false
            ),Response::HTTP_BAD_REQUEST);
        }

        if ($user && !$user->isSuperAdmin()) {
            return new JsonResponse(array(
                "message" => "User has not permission",
                'secretKey' => true
            ),Response::HTTP_FORBIDDEN);
        }

        $shops = $em->getRepository('ApiBundle:Shop')->getAllInfo();


        usort($shops, function ($a, $b) {
            if ($a['createDate'] == $b['createDate']) {
                return 0;
            }

            return ($a['createDate'] > $b['createDate']) ? -1 : 1;
        });

        $response = new JsonResponse($shops,Response::HTTP_OK);

        // Set option JSON_UNESCAPED_UNICODE to return in normal view cyrillic string (example: Привет)
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }
}