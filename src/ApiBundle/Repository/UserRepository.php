<?php

namespace ApiBundle\Repository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByUsernameOrEmail($login) {
        $qb = $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->orWhere('u.username = :username')
            ->setParameter('email', $login)
            ->setParameter('username', $login)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
