<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConsignmentPaid
 *
 * @ORM\Table(name="consignment_paid")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ConsignmentPaidRepository")
 */
class ConsignmentPaid
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Paid on debt (оплачено по долгу)
     *
     * @var float|null
     *
     * @ORM\Column(name="paidDebt", type="float", nullable=true)
     */
    private $paidDebt;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Consignment")
     * @ORM\JoinColumn(name="consigment", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $consignment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paidDebt.
     *
     * @param float|null $paidDebt
     *
     * @return ConsignmentPaid
     */
    public function setPaidDebt($paidDebt = null)
    {
        $this->paidDebt = $paidDebt;

        return $this;
    }

    /**
     * Get paidDebt.
     *
     * @return float|null
     */
    public function getPaidDebt()
    {
        return $this->paidDebt;
    }

    /**
     * Get consignment
     *
     * @return Consignment
     */
    public function getConsignment()
    {
        return $this->consignment;
    }

    /**
     * @param Consignment|null $consignment
     * @return ConsignmentPaid
     */
    public function setConsignment(Consignment $consignment = null)
    {
        $this->consignment = $consignment;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
