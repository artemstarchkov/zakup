<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GoodCategoryNestThree
 *
 * @ORM\Table(name="good_category_nest_three")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GoodCategoryNestThreeRepository")
 */
class GoodCategoryNestThree
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Good", mappedBy="goodCategoryNestOne")
     */
    private $good;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategory")
     * @ORM\JoinColumn(name="goodCategory", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategory;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestOne", inversedBy="goodCategoryNestThree")
     * @ORM\JoinColumn(name="goodCategoryNestOne", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategoryNestOne;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestTwo", inversedBy="goodCategoryNestThree")
     * @ORM\JoinColumn(name="goodCategoryNestTwo", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategoryNestTwo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return GoodCategoryNestThree
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * Get Goods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Set goodCategory
     *
     * @param \ApiBundle\Entity\GoodCategory $goodCategory
     *
     * @return GoodCategoryNestThree
     */
    public function setGoodCategory(\ApiBundle\Entity\GoodCategory $goodCategory = null)
    {
        $this->goodCategory = $goodCategory;

        return $this;
    }

    /**
     * Get goodCategory
     *
     * @return \ApiBundle\Entity\GoodCategory
     */
    public function getGoodCategory()
    {
        return $this->goodCategory;
    }

    /**
     * Set goodCategoryNestOne
     *
     * @param \ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne
     *
     * @return GoodCategoryNestThree
     */
    public function setGoodCategoryNestOne(\ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne = null)
    {
        $this->goodCategoryNestOne = $goodCategoryNestOne;

        return $this;
    }

    /**
     * Get goodCategoryNestOne
     *
     * @return \ApiBundle\Entity\GoodCategoryNestOne
     */
    public function getGoodCategoryNestOne()
    {
        return $this->goodCategoryNestOne;
    }

    /**
     * Set goodCategoryNestTwo
     *
     * @param \ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo
     *
     * @return GoodCategoryNestThree
     */
    public function setGoodCategoryNestTwo(\ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo = null)
    {
        $this->goodCategoryNestTwo = $goodCategoryNestTwo;

        return $this;
    }

    /**
     * Get goodCategoryNestTwo
     *
     * @return \ApiBundle\Entity\GoodCategoryNestTwo
     */
    public function getGoodCategoryNestTwo()
    {
        return $this->goodCategoryNestTwo;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
