<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * HistoryBooking
 *
 * @ORM\Table(name="history_booking")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\HistoryBookingRepository")
 */
class HistoryBooking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customerUserName", type="string", length=255, nullable=true)
     */
    private $customerUserName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sellerCompanyName", type="string", length=255, nullable=true)
     */
    private $sellerCompanyName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=true)
     */
    private $status;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\HistoryBookingItem", mappedBy="historyBooking")
     */
    private $historyBookingItem;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Booking")
     * @ORM\JoinColumn(name="booking", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $booking;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bookingNumber", type="string", length=255, nullable=true)
     */
    private $bookingNumber;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company")
     * @ORM\JoinColumn(name="sellerCompany", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $sellerCompany;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deliveryManInfo", type="text", nullable=true)
     */
    private $deliveryManInfo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deliveryDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerUserName.
     *
     * @param string|null $customerUserName
     *
     * @return HistoryBooking
     */
    public function setCustomerUserName($customerUserName = null)
    {
        $this->customerUserName = $customerUserName;

        return $this;
    }

    /**
     * Get customerUserName.
     *
     * @return string|null
     */
    public function getCustomerUserName()
    {
        return $this->customerUserName;
    }

    /**
     * Set sellerCompanyName.
     *
     * @param string|null $sellerCompanyName
     *
     * @return HistoryBooking
     */
    public function setSellerCompanyName($sellerCompanyName = null)
    {
        $this->sellerCompanyName = $sellerCompanyName;

        return $this;
    }

    /**
     * Get sellerCompanyName.
     *
     * @return string|null
     */
    public function getSellerCompanyName()
    {
        return $this->sellerCompanyName;
    }

    /**
     * Set deliveryManInfo.
     *
     * @param string|null $deliveryManInfo
     *
     * @return HistoryBooking
     */
    public function setDeliveryManInfo($deliveryManInfo = null)
    {
        $this->deliveryManInfo = $deliveryManInfo;

        return $this;
    }

    /**
     * Get deliveryManInfo.
     *
     * @return string|null
     */
    public function getDeliveryManInfo()
    {
        return $this->deliveryManInfo;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return HistoryBooking
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add historyBookingItem
     *
     * @param \ApiBundle\Entity\HistoryBookingItem $historyBookingItem
     *
     * @return HistoryBooking
     */
    public function addHistoryBookingItem(\ApiBundle\Entity\HistoryBookingItem $historyBookingItem)
    {
        $this->historyBookingItem[] = $historyBookingItem;

        return $this;
    }

    /**
     * Remove historyBookingItem
     *
     * @param \ApiBundle\Entity\HistoryBookingItem $historyBookingItem
     */
    public function removeHistoryBookingItem(\ApiBundle\Entity\HistoryBookingItem $historyBookingItem)
    {
        $this->historyBookingItem->removeElement($historyBookingItem);
    }

    /**
     * Get historyBookingItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistoryBookingItem()
    {
        return $this->historyBookingItem;
    }

    /**
     * Set booking
     *
     * @param \ApiBundle\Entity\Booking $booking
     *
     * @return HistoryBooking
     */
    public function setBooking(\ApiBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \ApiBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set bookingNumber.
     *
     * @param string|null $bookingNumber
     *
     * @return HistoryBooking
     */
    public function setBookingNumber($bookingNumber = null)
    {
        $this->bookingNumber = $bookingNumber;

        return $this;
    }

    /**
     * Get bookingNumber.
     *
     * @return string|null
     */
    public function getBookingNumber()
    {
        return $this->bookingNumber;
    }

    /**
     * Set sellerCompany
     *
     * @param \ApiBundle\Entity\Company $sellerCompany
     *
     * @return HistoryBooking
     */
    public function setSellerCompany(\ApiBundle\Entity\Company $sellerCompany = null)
    {
        $this->sellerCompany = $sellerCompany;

        return $this;
    }

    /**
     * Get sellerCompany
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getSellerCompany()
    {
        return $this->sellerCompany;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param mixed $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param mixed $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }

    public function __construct()
    {
        $this->createdDate = new \DateTime('now');
        $this->historyBookingItem = new ArrayCollection();
    }
}
