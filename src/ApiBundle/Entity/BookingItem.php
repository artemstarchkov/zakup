<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BookingItem
 *
 * @ORM\Table(name="booking_item")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\BookingItemRepository")
 */
class BookingItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Good")
     * @ORM\JoinColumn(name="good", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $good;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategory")
     * @ORM\JoinColumn(name="goodCategory", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategory;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestOne")
     * @ORM\JoinColumn(name="goodCategoryNestOne", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategoryNestOne;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestTwo")
     * @ORM\JoinColumn(name="goodCategoryNestTwo", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategoryNestTwo;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestThree")
     * @ORM\JoinColumn(name="goodCategoryNestThree", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategoryNestThree;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Booking", inversedBy="bookingItem")
     * @ORM\JoinColumn(name="booking", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $booking;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Consignment")
     * @ORM\JoinColumn(name="consignment", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $consignment;

    /**
     * @var float|null
     *
     * @ORM\Column(name="pricePerOne", type="float", nullable=true)
     */
    private $pricePerOne;

    /**
     * @var int|null
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="returnCount", type="integer", nullable=true)
     */
    private $returnCount;

    /**
     * @var bool
     *
     * @ORM\Column(name="received", type="boolean", nullable=true)
     */
    protected $received;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set good
     *
     * @param \ApiBundle\Entity\Good $good
     *
     * @return BookingItem
     */
    public function setGood(\ApiBundle\Entity\Good $good = null)
    {
        $this->good = $good;

        return $this;
    }

    /**
     * Get good
     *
     * @return \ApiBundle\Entity\Good
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Set goodCategory
     *
     * @param \ApiBundle\Entity\GoodCategory $goodCategory
     *
     * @return BookingItem
     */
    public function setGoodCategory(\ApiBundle\Entity\GoodCategory $goodCategory = null)
    {
        $this->goodCategory = $goodCategory;

        return $this;
    }

    /**
     * Get goodCategory
     *
     * @return \ApiBundle\Entity\GoodCategory
     */
    public function getGoodCategory()
    {
        return $this->goodCategory;
    }

    /**
     * Set goodCategoryNestOne
     *
     * @param \ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne
     *
     * @return BookingItem
     */
    public function setGoodCategoryNestOne(\ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne = null)
    {
        $this->goodCategoryNestOne = $goodCategoryNestOne;

        return $this;
    }

    /**
     * Get goodCategoryNestOne
     *
     * @return \ApiBundle\Entity\GoodCategoryNestOne
     */
    public function getGoodCategoryNestOne()
    {
        return $this->goodCategoryNestOne;
    }

    /**
     * Set goodCategoryNestTwo
     *
     * @param \ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo
     *
     * @return BookingItem
     */
    public function setGoodCategoryNestTwo(\ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo = null)
    {
        $this->goodCategoryNestTwo = $goodCategoryNestTwo;

        return $this;
    }

    /**
     * Get goodCategoryNestTwo
     *
     * @return \ApiBundle\Entity\GoodCategoryNestTwo
     */
    public function getGoodCategoryNestTwo()
    {
        return $this->goodCategoryNestTwo;
    }

    /**
     * Set goodCategoryNestThree
     *
     * @param \ApiBundle\Entity\GoodCategoryNestThree $goodCategoryNestThree
     *
     * @return BookingItem
     */
    public function setGoodCategoryNestThree(\ApiBundle\Entity\GoodCategoryNestThree $goodCategoryNestThree = null)
    {
        $this->goodCategoryNestThree = $goodCategoryNestThree;

        return $this;
    }

    /**
     * Get goodCategoryNestThree
     *
     * @return \ApiBundle\Entity\GoodCategoryNestThree
     */
    public function getGoodCategoryNestThree()
    {
        return $this->goodCategoryNestThree;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return BookingItem
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set booking
     *
     * @param \ApiBundle\Entity\Booking $booking
     *
     * @return BookingItem
     */
    public function setBooking(\ApiBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \ApiBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Get consignment
     *
     * @return \ApiBundle\Entity\Consignment
     */
    public function getConsignment()
    {
        return $this->consignment;
    }


    /**
     * Set consignment
     *
     * @param \ApiBundle\Entity\Consignment $consignment
     *
     * @return BookingItem
     */
    public function setConsignment(\ApiBundle\Entity\Consignment $consignment = null)
    {
        $this->consignment = $consignment;

        return $this;
    }

    /**
     * Set pricePerOne.
     *
     * @param float|null $pricePerOne
     *
     * @return BookingItem
     */
    public function setPricePerOne($pricePerOne = null)
    {
        $this->pricePerOne = $pricePerOne;

        return $this;
    }

    /**
     * Get pricePerOne.
     *
     * @return float|null
     */
    public function getPricePerOne()
    {
        return $this->pricePerOne;
    }

    /**
     * Set amount.
     *
     * @param int|null $amount
     *
     * @return BookingItem
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set returnCount.
     *
     * @param int|null $returnCount
     *
     * @return BookingItem
     */
    public function setReturnCount($returnCount = null)
    {
        $this->returnCount = $returnCount;

        return $this;
    }

    /**
     * Get returnCount.
     *
     * @return int|null
     */
    public function getReturnCount()
    {
        return $this->returnCount;
    }

    /**
     * @param $received
     * @return $this
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }

    /**
     * Get received
     *
     * @return bool
     */
    public function getReceived()
    {
        return $this->received;
    }
}
