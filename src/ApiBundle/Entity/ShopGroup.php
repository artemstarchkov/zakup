<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ShopGroup
 *
 * @ORM\Table(name="shop_group")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ShopGroupRepository")
 */
class ShopGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company", inversedBy="shopGroup")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     * @var ShopGroup[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="ApiBundle\Entity\Shop")
     */
    private $shops;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\DateAccessCompany", mappedBy="shopGroup")
     */
    private $dateAccessCompany;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return ShopGroup
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return ShopGroup
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add shop
     *
     * @param \ApiBundle\Entity\Shop $shop
     *
     * @return ShopGroup
     */
    public function addShop(\ApiBundle\Entity\Shop $shop)
    {
        $this->shops[] = $shop;

        return $this;
    }

    /**
     * Remove shop
     *
     * @param \ApiBundle\Entity\Shop $shop
     */
    public function removeShop(\ApiBundle\Entity\Shop $shop)
    {
        $this->shops->removeElement($shop);
    }

    /**
     * Get shop
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShop()
    {
        return $this->shops;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return ShopGroup
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add dateAccessCompany
     *
     * @param \ApiBundle\Entity\DateAccessCompany $dateAccessCompany
     *
     * @return ShopGroup
     */
    public function addDateAccessCompany(\ApiBundle\Entity\DateAccessCompany $dateAccessCompany)
    {
        $this->dateAccessCompany[] = $dateAccessCompany;

        return $this;
    }

    /**
     * Remove dateAccessCompany
     *
     * @param \ApiBundle\Entity\DateAccessCompany $dateAccessCompany
     */
    public function removeDateAccessCompany(\ApiBundle\Entity\DateAccessCompany $dateAccessCompany)
    {
        $this->dateAccessCompany->removeElement($dateAccessCompany);
    }

    /**
     * Get dateAccessCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDateAccessCompany()
    {
        return $this->dateAccessCompany;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
