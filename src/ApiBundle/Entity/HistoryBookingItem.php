<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoryBookingItem
 *
 * @ORM\Table(name="history_booking_item")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\HistoryBookingItemRepository")
 */
class HistoryBookingItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="goodName", type="string", length=255, nullable=true)
     */
    private $goodName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="goodDescription", type="text", nullable=true)
     */
    private $goodDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="goodShortDescription", type="text", nullable=true)
     */
    private $goodShortDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="companyName", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var float|null
     *
     * @ORM\Column(name="pricePerOne", type="float", nullable=true)
     */
    private $pricePerOne;

    /**
     * @var int|null
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="returnCount", type="integer", nullable=true)
     */
    private $returnCount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="goodCategoryName", type="string", length=255, nullable=true)
     */
    private $goodCategoryName;

    /**
     * @var bool
     *
     * @ORM\Column(name="received", type="boolean")
     */
    protected $received;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\HistoryBooking", inversedBy="historyBookingItem")
     * @ORM\JoinColumn(name="historyBooking", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $historyBooking;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\BookingItem")
     * @ORM\JoinColumn(name="bookingItem", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $bookingItem;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set goodName.
     *
     * @param string|null $goodName
     *
     * @return HistoryBookingItem
     */
    public function setGoodName($goodName = null)
    {
        $this->goodName = $goodName;

        return $this;
    }

    /**
     * Get goodName.
     *
     * @return string|null
     */
    public function getGoodName()
    {
        return $this->goodName;
    }

    /**
     * Set goodDescription.
     *
     * @param string|null $goodDescription
     *
     * @return HistoryBookingItem
     */
    public function setGoodDescription($goodDescription = null)
    {
        $this->goodDescription = $goodDescription;

        return $this;
    }

    /**
     * Get goodDescription.
     *
     * @return string|null
     */
    public function getGoodDescription()
    {
        return $this->goodDescription;
    }

    /**
     * Set goodShortDescription.
     *
     * @param string|null $goodShortDescription
     *
     * @return HistoryBookingItem
     */
    public function setGoodShortDescription($goodShortDescription = null)
    {
        $this->goodShortDescription = $goodShortDescription;

        return $this;
    }

    /**
     * Get goodShortDescription.
     *
     * @return string|null
     */
    public function getGoodShortDescription()
    {
        return $this->goodShortDescription;
    }

    /**
     * Set companyName.
     *
     * @param string|null $companyName
     *
     * @return HistoryBookingItem
     */
    public function setCompanyName($companyName = null)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set pricePerOne.
     *
     * @param float|null $pricePerOne
     *
     * @return HistoryBookingItem
     */
    public function setPricePerOne($pricePerOne = null)
    {
        $this->pricePerOne = $pricePerOne;

        return $this;
    }

    /**
     * Get pricePerOne.
     *
     * @return float|null
     */
    public function getPricePerOne()
    {
        return $this->pricePerOne;
    }

    /**
     * Set amount.
     *
     * @param int|null $amount
     *
     * @return HistoryBookingItem
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set returnCount.
     *
     * @param int|null $returnCount
     *
     * @return HistoryBookingItem
     */
    public function setReturnCount($returnCount = null)
    {
        $this->returnCount = $returnCount;

        return $this;
    }

    /**
     * Get returnCount.
     *
     * @return int|null
     */
    public function getReturnCount()
    {
        return $this->returnCount;
    }

    /**
     * Set goodCategoryName.
     *
     * @param string|null $goodCategoryName
     *
     * @return HistoryBookingItem
     */
    public function setGoodCategoryName($goodCategoryName = null)
    {
        $this->goodCategoryName = $goodCategoryName;

        return $this;
    }

    /**
     * Get goodCategoryName.
     *
     * @return string|null
     */
    public function getGoodCategoryName()
    {
        return $this->goodCategoryName;
    }

    /**
     * @param $received
     * @return $this
     */
    public function setReceived($received)
    {
        $this->received = $received;

        return $this;
    }

    /**
     * Get received
     *
     * @return bool
     */
    public function getReceived()
    {
        return $this->received;
    }

    public function getReceivedString() {
        return $this->getReceived()? 'Да' : 'Нет';
    }

    /**
     * Set historyBooking
     *
     * @param \ApiBundle\Entity\HistoryBooking $historyBooking
     *
     * @return HistoryBookingItem
     */
    public function setHistoryBooking(\ApiBundle\Entity\HistoryBooking $historyBooking = null)
    {
        $this->historyBooking = $historyBooking;

        return $this;
    }

    /**
     * Get historyBooking
     *
     * @return \ApiBundle\Entity\HistoryBooking
     */
    public function getHistoryBooking()
    {
        return $this->historyBooking;
    }

    /**
     * Set bookingItem
     *
     * @param \ApiBundle\Entity\BookingItem $bookingItem
     *
     * @return HistoryBookingItem
     */
    public function setBookingItem(\ApiBundle\Entity\BookingItem $bookingItem = null)
    {
        $this->bookingItem = $bookingItem;

        return $this;
    }

    /**
     * Get bookingItem
     *
     * @return \ApiBundle\Entity\BookingItem
     */
    public function getBookingItem()
    {
        return $this->bookingItem;
    }
}
