<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GoodCategoryNestOne
 *
 * @ORM\Table(name="good_category_nest_one")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GoodCategoryNestOneRepository")
 */
class GoodCategoryNestOne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Good", mappedBy="goodCategoryNestOne")
     */
    private $good;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategory", inversedBy="goodCategoryNestOne")
     * @ORM\JoinColumn(name="goodCategory", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategory;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\GoodCategoryNestTwo", mappedBy="goodCategoryNestOne")
     */
    private $goodCategoryNestTwo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return GoodCategoryNestOne
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * Get Goods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Set goodCategory
     *
     * @param \ApiBundle\Entity\GoodCategory $goodCategory
     *
     * @return GoodCategoryNestOne
     */
    public function setGoodCategory(\ApiBundle\Entity\GoodCategory $goodCategory = null)
    {
        $this->goodCategory = $goodCategory;

        return $this;
    }

    /**
     * Get goodCategory
     *
     * @return \ApiBundle\Entity\GoodCategory
     */
    public function getGoodCategory()
    {
        return $this->goodCategory;
    }

    /**
     * Get goodCategoryNestTwo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoodCategoryNestTwo()
    {
        return $this->goodCategoryNestTwo;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
