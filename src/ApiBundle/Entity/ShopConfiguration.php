<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShopConfiguration
 *
 * @ORM\Table(name="shop_configuration")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ShopConfigurationRepository")
 */
class ShopConfiguration
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $shop;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="isAllowConsignment", type="boolean", nullable=true)
     */
    private $isAllowConsignment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return ShopConfiguration
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set shop
     *
     * @param \ApiBundle\Entity\Shop $shop
     *
     * @return ShopConfiguration
     */
    public function setShop(\ApiBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \ApiBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set isAllowConsignment.
     *
     * @param bool|null $isAllowConsignment
     *
     * @return ShopConfiguration
     */
    public function setIsAllowConsignment($isAllowConsignment = null)
    {
        $this->isAllowConsignment = $isAllowConsignment;

        return $this;
    }

    /**
     * Get isAllowConsignment.
     *
     * @return bool|null
     */
    public function getIsAllowConsignment()
    {
        return $this->isAllowConsignment;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
    }
}
