<?php

namespace ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Booking
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\BookingRepository")
 */
class Booking
{
    const NEW = 'new';
    const ACCEPTED = 'accepted';
    const IN_PROGRESS = 'in_progress';
    const COMPLETED = 'completed';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\BookingItem", mappedBy="booking")
     */
    private $bookingItem;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Consignment", mappedBy="booking")
     */
    private $consignment;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\User")
     * @ORM\JoinColumn(name="customerUser", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $customerUser;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company")
     * @ORM\JoinColumn(name="sellerCompany", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $sellerCompany;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=30, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="countNumber", type="integer")
     */
    private $countNumber;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deliveryDate;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\StaffCompany")
     * @ORM\JoinColumn(name="deliveryStaffCompany", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $deliveryStaffCompany;

    /**
     * @var bool
     *
     * @ORM\Column(name="isElected", type="boolean", nullable=true)
     */
    protected $isElected;

    /**
     * @var bool
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    protected $isDeleted;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add bookingItem
     *
     * @param \ApiBundle\Entity\BookingItem $bookingItem
     *
     * @return Booking
     */
    public function addBookingItem(\ApiBundle\Entity\BookingItem $bookingItem)
    {
        $this->bookingItem[] = $bookingItem;

        return $this;
    }

    /**
     * Remove bookingItem
     *
     * @param \ApiBundle\Entity\BookingItem $bookingItem
     */
    public function removeBookingItem(\ApiBundle\Entity\BookingItem $bookingItem)
    {
        $this->bookingItem->removeElement($bookingItem);
    }

    /**
     * Get bookingItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBookingItem()
    {
        return $this->bookingItem;
    }

    /**
     * Add consignment
     *
     * @param \ApiBundle\Entity\Consignment $consignment
     *
     * @return Booking
     */
    public function addConsignment(\ApiBundle\Entity\Consignment $consignment)
    {
        $this->consignment[] = $consignment;

        return $this;
    }

    /**
     * Remove consignment
     *
     * @param \ApiBundle\Entity\Consignment $consignment
     */
    public function removeConsignment(\ApiBundle\Entity\Consignment $consignment)
    {
        $this->consignmentbookingItem->removeElement($consignment);
    }

    /**
     * Get consignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConsignment()
    {
        return $this->consignment;
    }

    /**
     * Set customerUser
     *
     * @param \ApiBundle\Entity\User $customerUser
     *
     * @return Booking
     */
    public function setCustomerUser(\ApiBundle\Entity\User $customerUser = null)
    {
        $this->customerUser = $customerUser;

        return $this;
    }

    /**
     * Get customerUser
     *
     * @return \ApiBundle\Entity\User
     */
    public function getCustomerUser()
    {
        return $this->customerUser;
    }

    /**
     * Set sellerCompany
     *
     * @param \ApiBundle\Entity\Company $sellerCompany
     *
     * @return Booking
     */
    public function setSellerCompany(\ApiBundle\Entity\Company $sellerCompany = null)
    {
        $this->sellerCompany = $sellerCompany;

        return $this;
    }

    /**
     * Get sellerCompany
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getSellerCompany()
    {
        return $this->sellerCompany;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return Booking
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set number.
     *
     * @param string|null $number
     *
     * @return Booking
     */
    public function setNumber($number = null)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set countNumber.
     *
     * @param integer|null $countNumber
     *
     * @return Booking
     */
    public function setCountNumber($countNumber = null)
    {
        $this->countNumber = $countNumber;

        return $this;
    }

    /**
     * Get countNumber.
     *
     * @return integer|null
     */
    public function getCountNumber()
    {
        return $this->countNumber;
    }

    /**
     * Is booking new
     *
     * @return bool
     */
    public function isNew() {
        return $this->status === 'new';
    }

    /**
     * Is booking accepted
     *
     * @return bool
     */
    public function isAccepted() {
        return $this->status === 'accepted';
    }

    /**
     * Is booking in progress
     *
     * @return bool
     */
    public function isInProgress() {
        return $this->status === 'in_progress';
    }

    /**
     * Is booking completed
     *
     * @return bool
     */
    public function isCompleted() {
        return $this->status === 'completed';
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param mixed $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param mixed $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * Set deliveryStaffCompany
     *
     * @param \ApiBundle\Entity\StaffCompany $deliveryStaffCompany
     *
     * @return Booking
     */
    public function setDeliveryStaffCompany(\ApiBundle\Entity\StaffCompany $deliveryStaffCompany = null)
    {
        $this->deliveryStaffCompany = $deliveryStaffCompany;

        return $this;
    }

    /**
     * Get deliveryStaffCompany
     *
     * @return \ApiBundle\Entity\StaffCompany
     */
    public function getDeliveryStaffCompany()
    {
        return $this->deliveryStaffCompany;
    }

    /**
     * @param $isElected
     * @return $this
     */
    public function setIsElected($isElected)
    {
        $this->isElected = $isElected;

        return $this;
    }

    /**
     * Get isElected
     *
     * @return bool
     */
    public function getIsElected()
    {
        return $this->isElected;
    }

    /**
     * @param $isDeleted
     * @return $this
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    public function __construct()
    {
        $this->createdDate = new \DateTime('now');
        $this->bookingItem = new ArrayCollection();
    }
}
