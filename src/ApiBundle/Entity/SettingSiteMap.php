<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SettingSiteMap
 *
 * @ORM\Table(name="setting_site_map")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\SettingSiteMapRepository")
 */
class SettingSiteMap
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="zoom", type="integer", nullable=true)
     */
    private $zoom;

    /**
     * @var float
     *
     * @ORM\Column(name="centerLatitude", type="float")
     */
    private $centerLatitude;

    /**
     * @var float
     *
     * @ORM\Column(name="centerLongitude", type="float")
     */
    private $centerLongitude;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\Company", inversedBy="settingSiteMap", cascade={"persist"})
     */
    private $company;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zoom.
     *
     * @param int|null $zoom
     *
     * @return SettingSiteMap
     */
    public function setZoom($zoom = null)
    {
        $this->zoom = $zoom;

        return $this;
    }

    /**
     * Get zoom.
     *
     * @return int|null
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * Set centerLatitude.
     *
     * @param float $centerLatitude
     *
     * @return SettingSiteMap
     */
    public function setCenterLatitude($centerLatitude)
    {
        $this->centerLatitude = $centerLatitude;

        return $this;
    }

    /**
     * Get centerLatitude.
     *
     * @return float
     */
    public function getCenterLatitude()
    {
        return $this->centerLatitude;
    }

    /**
     * Set centerLongitude.
     *
     * @param float $centerLongitude
     *
     * @return SettingSiteMap
     */
    public function setCenterLongitude($centerLongitude)
    {
        $this->centerLongitude = $centerLongitude;

        return $this;
    }

    /**
     * Get centerLongitude.
     *
     * @return float
     */
    public function getCenterLongitude()
    {
        return $this->centerLongitude;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return SettingSiteMap
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
