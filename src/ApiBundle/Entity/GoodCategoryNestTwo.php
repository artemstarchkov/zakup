<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GoodCategoryNestTwo
 *
 * @ORM\Table(name="good_category_nest_two")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GoodCategoryNestTwoRepository")
 */
class GoodCategoryNestTwo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Good", mappedBy="goodCategoryNestOne")
     */
    private $good;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategory")
     * @ORM\JoinColumn(name="goodCategory", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategory;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestOne", inversedBy="goodCategoryNestTwo")
     * @ORM\JoinColumn(name="goodCategoryNestOne", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategoryNestOne;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\GoodCategoryNestThree", mappedBy="goodCategoryNestTwo")
     */
    private $goodCategoryNestThree;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return GoodCategoryNestTwo
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * Get Goods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Set goodCategory
     *
     * @param \ApiBundle\Entity\GoodCategory $goodCategory
     *
     * @return GoodCategoryNestTwo
     */
    public function setGoodCategory(\ApiBundle\Entity\GoodCategory $goodCategory = null)
    {
        $this->goodCategory = $goodCategory;

        return $this;
    }

    /**
     * Get goodCategory
     *
     * @return \ApiBundle\Entity\GoodCategory
     */
    public function getGoodCategory()
    {
        return $this->goodCategory;
    }

    /**
     * Set goodCategoryNestOne
     *
     * @param \ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne
     *
     * @return GoodCategoryNestTwo
     */
    public function setGoodCategoryNestOne(\ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne = null)
    {
        $this->goodCategoryNestOne = $goodCategoryNestOne;

        return $this;
    }

    /**
     * Get goodCategoryNestOne
     *
     * @return \ApiBundle\Entity\GoodCategoryNestOne
     */
    public function getGoodCategoryNestOne()
    {
        return $this->goodCategoryNestOne;
    }

    /**
     * Get goodCategoryNestThree
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoodCategoryNestThree()
    {
        return $this->goodCategoryNestThree;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
