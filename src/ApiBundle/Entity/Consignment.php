<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consignment
 *
 * @ORM\Table(name="consignment")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\ConsignmentRepository")
 */
class Consignment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\StaffCompany")
     * @ORM\JoinColumn(name="deliveryStaffCompany", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $deliveryStaffCompany;

    /**
     * Number of items in debt (количество товара в долг)
     *
     * @var int|null
     *
     * @ORM\Column(name="bookingItemAmount", type="integer", nullable=true)
     */
    private $bookingItemAmount;

    /**
     * Price per one booking item at the time of registration (цена за один товар на момент оформления долга)
     * @var float|null
     *
     * @ORM\Column(name="pricePerOne", type="float", nullable=true)
     */
    private $pricePerOne;

    /**
     * Total price (всего долг)
     *
     * @var float|null
     *
     * @ORM\Column(name="totalPrice", type="float", nullable=true)
     */
    private $totalPrice;

    /**
     * Balance of Debt (остаток долга)
     *
     * @var float|null
     *
     * @ORM\Column(name="balanceDebt", type="float", nullable=true)
     */
    private $balanceDebt;

    /**
     * @var bool
     *
     * @ORM\Column(name="isClosed", type="boolean", nullable=true)
     */
    private $isClosed;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookingItemAmount.
     *
     * @param int|null $bookingItemAmount
     *
     * @return Consignment
     */
    public function setBookingItemAmount($bookingItemAmount = null)
    {
        $this->bookingItemAmount = $bookingItemAmount;

        return $this;
    }

    /**
     * Get bookingItemAmount.
     *
     * @return int|null
     */
    public function getBookingItemAmount()
    {
        return $this->bookingItemAmount;
    }

    /**
     * Set deliveryStaffCompany
     *
     * @param \ApiBundle\Entity\StaffCompany $deliveryStaffCompany
     *
     * @return Consignment
     */
    public function setDeliveryStaffCompany(\ApiBundle\Entity\StaffCompany $deliveryStaffCompany = null)
    {
        $this->deliveryStaffCompany = $deliveryStaffCompany;

        return $this;
    }

    /**
     * Get deliveryStaffCompany
     *
     * @return \ApiBundle\Entity\StaffCompany
     */
    public function getDeliveryStaffCompany()
    {
        return $this->deliveryStaffCompany;
    }

    /**
     * Set shop
     *
     * @param \ApiBundle\Entity\Shop $shop
     *
     * @return Consignment
     */
    public function setShop(\ApiBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \ApiBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set pricePerOne.
     *
     * @param float|null $pricePerOne
     *
     * @return Consignment
     */
    public function setPricePerOne($pricePerOne = null)
    {
        $this->pricePerOne = $pricePerOne;

        return $this;
    }

    /**
     * Get pricePerOne.
     *
     * @return float|null
     */
    public function getPricePerOne()
    {
        return $this->pricePerOne;
    }

    /**
     * Set totalPrice.
     *
     * @param float|null $totalPrice
     *
     * @return Consignment
     */
    public function setTotalPrice($totalPrice = null)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice.
     *
     * @return float|null
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set balanceDebt.
     *
     * @param float|null $balanceDebt
     *
     * @return Consignment
     */
    public function setBalanceDebt($balanceDebt = null)
    {
        $this->balanceDebt = $balanceDebt;

        return $this;
    }

    /**
     * Get balanceDebt.
     *
     * @return float|null
     */
    public function getBalanceDebt()
    {
        return $this->balanceDebt;
    }

    /**
     * @param $isClosed
     * @return $this
     */
    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;

        return $this;
    }

    /**
     * Get isClosed
     *
     * @return bool
     */
    public function getIsClosed()
    {
        return $this->isClosed;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
