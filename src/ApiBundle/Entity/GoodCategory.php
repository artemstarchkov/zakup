<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoodCategory
 *
 * @ORM\Table(name="good_category")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GoodCategoryRepository")
 */
class GoodCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Good", mappedBy="goodCategory")
     */
    private $good;

    /**
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\GoodCategoryNestOne", mappedBy="goodCategory")
     * @ORM\JoinColumn(name="goodCategoryNestOne", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $goodCategoryNestOne;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return GoodCategory
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return GoodCategory
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add Good
     *
     * @param \ApiBundle\Entity\Good $good
     *
     * @return GoodCategory
     */
    public function addGood(\ApiBundle\Entity\Good $good)
    {
        $this->good[] = $good;

        return $this;
    }

    /**
     * Remove Good
     *
     * @param \ApiBundle\Entity\Good $good
     */
    public function removeGood(\ApiBundle\Entity\Good $good)
    {
        $this->good->removeElement($good);
    }

    /**
     * Get Goods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Get goodCategoryNestOne
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoodCategoryNestOne()
    {
        return $this->goodCategoryNestOne;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
