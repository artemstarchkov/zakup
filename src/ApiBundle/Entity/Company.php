<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CompanyRepository")
 */
class Company
{
    private $_directory = 'uploads/company/';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="photoWebLink", type="string", length=255, nullable=true)
     */
    private $photoWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photoThumbnailWebLink", type="string", length=255, nullable=true)
     */
    private $photoThumbnailWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnailPhoto", type="string", length=255, nullable=true)
     */
    private $thumbnailPhoto;

    /**
     * @Assert\File(maxSize="12000000",mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *      })
     */
    public $file;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\User", mappedBy="company")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\SettingSiteMap", inversedBy="company")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $settingSiteMap;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\StaffCompany", mappedBy="company")
     */
    private $staffCompany;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\ShopGroup", mappedBy="company")
     */
    private $shopGroup;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\Good", mappedBy="company")
     */
    private $good;

    /**
     *
     * @ORM\OneToMany(targetEntity="ApiBundle\Entity\DateAccessCompany", mappedBy="company")
     */
    private $dateAccessCompany;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     */
    private $longitude;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Company
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return Company
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return Company
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return Company
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set photoWebLink
     *
     * @param string $photoWebLink
     *
     * @return Company
     */
    public function setPhotoWebLink($photoWebLink)
    {
        $this->photoWebLink = $photoWebLink;

        return $this;
    }

    /**
     * Get photoWebLink
     *
     * @return string
     */
    public function getPhotoWebLink()
    {
        return $this->photoWebLink;
    }

    /**
     * Set photoThumbnailWebLink
     *
     * @param string $photoThumbnailWebLink
     *
     * @return Company
     */
    public function setPhotoThumbnailWebLink($photoThumbnailWebLink)
    {
        $this->photoThumbnailWebLink = $photoThumbnailWebLink;

        return $this;
    }

    /**
     * Get photoThumbnailWebLink
     *
     * @return string
     */
    public function getPhotoThumbnailWebLink()
    {
        return $this->photoThumbnailWebLink;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Company
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set thumbnailPhoto
     *
     * @param string $thumbnailPhoto
     *
     * @return Company
     */
    public function setThumbnailPhoto($thumbnailPhoto)
    {
        $this->thumbnailPhoto = $thumbnailPhoto;

        return $this;
    }

    /**
     * Get thumbnailPhoto
     *
     * @return string
     */
    public function getThumbnailPhoto()
    {
        return $this->thumbnailPhoto;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return Company
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set settingSiteMap
     *
     * @param \ApiBundle\Entity\SettingSiteMap $settingSiteMap
     *
     * @return Company
     */
    public function setSettingSiteMap(\ApiBundle\Entity\SettingSiteMap $settingSiteMap = null)
    {
        $this->settingSiteMap = $settingSiteMap;

        return $this;
    }

    /**
     * Get settingSiteMap
     *
     * @return \ApiBundle\Entity\SettingSiteMap
     */
    public function getSettingSiteMap()
    {
        return $this->settingSiteMap;
    }

    /**
     * Add staffCompany
     *
     * @param \ApiBundle\Entity\StaffCompany $staffCompany
     *
     * @return Company
     */
    public function addStaffCompany(\ApiBundle\Entity\StaffCompany $staffCompany)
    {
        $this->staffCompany[] = $staffCompany;

        return $this;
    }

    /**
     * Remove staffCompany
     *
     * @param \ApiBundle\Entity\StaffCompany $staffCompany
     */
    public function removeStaffCompany(\ApiBundle\Entity\StaffCompany $staffCompany)
    {
        $this->staffCompany->removeElement($staffCompany);
    }

    /**
     * Get staffCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStaffCompany()
    {
        return $this->staffCompany;
    }

    /**
     * Add shopGroup
     *
     * @param \ApiBundle\Entity\ShopGroup $shopGroup
     *
     * @return Company
     */
    public function addShopGroup(\ApiBundle\Entity\ShopGroup $shopGroup)
    {
        $this->shopGroup[] = $shopGroup;

        return $this;
    }

    /**
     * Remove shopGroup
     *
     * @param \ApiBundle\Entity\ShopGroup $shopGroup
     */
    public function removeShopGroup(\ApiBundle\Entity\ShopGroup $shopGroup)
    {
        $this->shopGroup->removeElement($shopGroup);
    }

    /**
     * Get shopGroup
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShopGroup()
    {
        return $this->shopGroup;
    }

    /**
     * Add Good
     *
     * @param \ApiBundle\Entity\Good $good
     *
     * @return Company
     */
    public function addGood(\ApiBundle\Entity\Good $good)
    {
        $this->good[] = $good;

        return $this;
    }

    /**
     * Remove Good
     *
     * @param \ApiBundle\Entity\Good $good
     */
    public function removeGood(\ApiBundle\Entity\Good $good)
    {
        $this->good->removeElement($good);
    }

    /**
     * Get Goods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGood()
    {
        return $this->good;
    }

    /**
     * Add dateAccessCompany
     *
     * @param \ApiBundle\Entity\DateAccessCompany $dateAccessCompany
     *
     * @return Company
     */
    public function addDateAccessCompany(\ApiBundle\Entity\DateAccessCompany $dateAccessCompany)
    {
        $this->dateAccessCompany[] = $dateAccessCompany;

        return $this;
    }

    /**
     * Remove dateAccessCompany
     *
     * @param \ApiBundle\Entity\DateAccessCompany $dateAccessCompany
     */
    public function removeDateAccessCompany(\ApiBundle\Entity\DateAccessCompany $dateAccessCompany)
    {
        $this->dateAccessCompany->removeElement($dateAccessCompany);
    }

    /**
     * Get dateAccessCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDateAccessCompany()
    {
        return $this->dateAccessCompany;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Company
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Company
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    /**
     * @return bool
     */
    private function getFileType() {
        /** @var UploadedFile $file */
        $file = $this->file;

        if ($file) {
            $arr = explode('/',$file->getMimeType());

            return $arr[1];
        } else return false;
    }

    /**
     * @return bool
     */
    public function saveFile() {
        if ($this->getFileType()) {
            $this->removeUpload();

            $bytes = random_bytes(15);
            $hexName = bin2hex($bytes);

            $fileName = 'company_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();
            $thumbnailFileName = 'company_thumbnail_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();

            $this->upload($fileName,$this->file->getPathname());

            $this->setPhoto($fileName);
            $this->setThumbnailPhoto($thumbnailFileName);

            $this->setPhotoWebLink($this->_directory.$fileName);
            $this->setPhotoThumbnailWebLink($this->_directory.$thumbnailFileName);

            return $fileName;
        }

        return false;
    }

    /**
     * @param $fileName
     */
    private function upload($fileName,$tmpName) {
        move_uploaded_file($tmpName,$this->_directory.$fileName);
    }

    /**
     * Remove uploaded file
     */
    public function removeUpload() {
        if (is_file($this->_directory.$this->photo)) {
            unlink($this->_directory.$this->photo);
        }
        if (is_file($this->_directory.$this->thumbnailPhoto)) {
            unlink($this->_directory.$this->thumbnailPhoto);
        }
    }

    /**
     * @return string
     */
    public function getFileSrc() {
        if (is_file($this->_directory.$this->photo)) {
            return "/{$this->_directory}".$this->photo;
        }

        return "";
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
