<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping\AttributeOverride;


/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\UserRepository")
 * @ORM\AttributeOverrides({
 *       @AttributeOverride(name="email",
 *          column=@Column(
 *              nullable = true,
 *              unique   = true,
 *              length   = 255
 *          )
 *      ),
 *      @AttributeOverride(name="emailCanonical",
 *          column=@Column(
 *              nullable = true,
 *              unique   = true,
 *              length   = 255
 *          )
 *      )
 *     })
 */
class User extends BaseUser
{
    const ROLE_STAFF_COMPANY = 'ROLE_STAFF_COMPANY';
    const ROLE_SHOP = 'ROLE_SHOP';
    const ROLE_COMPANY = 'ROLE_COMPANY';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="specialRole", type="string", length=255, nullable=true)
     */
    private $specialRole;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $temporarySecretKey;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\Company", mappedBy="user", cascade={"persist"})
     */
    private $company;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\Shop", mappedBy="user", cascade={"persist"})
     */
    private $shop;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\StaffCompany", mappedBy="user", cascade={"persist"})
     */
    private $staffCompany;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set specialRole.
     *
     * @param string|null $specialRole
     *
     * @return User
     */
    public function setSpecialRole($specialRole = null)
    {
        $this->specialRole = $specialRole;

        return $this;
    }

    /**
     * Get specialRole.
     *
     * @return string|null
     */
    public function getSpecialRole()
    {
        return $this->specialRole;
    }

    /**
     * Set temporarySecretKey
     *
     * @param string $temporarySecretKey
     *
     * @return User
     */
    public function setTemporarySecretKey($temporarySecretKey)
    {
        $this->temporarySecretKey = $temporarySecretKey;

        return $this;
    }

    /**
     * Get temporarySecretKey
     *
     * @return string
     */
    public function getTemporarySecretKey()
    {
        return $this->temporarySecretKey;
    }

    /**
     * Set shop
     *
     * @param \ApiBundle\Entity\Shop $shop
     *
     * @return User
     */
    public function setShop(\ApiBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \ApiBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return User
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set staffCompany
     *
     * @param \ApiBundle\Entity\StaffCompany $staffCompany
     *
     * @return User
     */
    public function setStaffCompany(\ApiBundle\Entity\StaffCompany $staffCompany = null)
    {
        $this->staffCompany = $staffCompany;

        return $this;
    }

    /**
     * Get staffCompany
     *
     * @return \ApiBundle\Entity\StaffCompany
     */
    public function getStaffCompany()
    {
        return $this->staffCompany;
    }
}
