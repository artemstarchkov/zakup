<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * StaffCompany
 *
 * @ORM\Table(name="staff_company")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\StaffCompanyRepository")
 */
class StaffCompany
{
    private $_directory = 'uploads/staffCompany/';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="secondName", type="string", length=255, nullable=true)
     */
    private $secondName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="photoWebLink", type="string", length=255, nullable=true)
     */
    private $photoWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photoThumbnailWebLink", type="string", length=255, nullable=true)
     */
    private $photoThumbnailWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnailPhoto", type="string", length=255, nullable=true)
     */
    private $thumbnailPhoto;

    /**
     * @Assert\File(maxSize="12000000",mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *      })
     */
    public $file;

    /**
     * @ORM\OneToOne(targetEntity="ApiBundle\Entity\User", inversedBy="staffCompany")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company", inversedBy="staffCompany")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return StaffCompany
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set secondName.
     *
     * @param string|null $secondName
     *
     * @return StaffCompany
     */
    public function setSecondName($secondName = null)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName.
     *
     * @return string|null
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return StaffCompany
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFullName() {
        return $this->firstName.' '.$this->secondName.' '.$this->lastName;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return StaffCompany
     */
    public function setPhone($phone = null)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address.
     *
     * @param string|null $address
     *
     * @return StaffCompany
     */
    public function setAddress($address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set photoWebLink
     *
     * @param string $photoWebLink
     *
     * @return StaffCompany
     */
    public function setPhotoWebLink($photoWebLink)
    {
        $this->photoWebLink = $photoWebLink;

        return $this;
    }

    /**
     * Get photoWebLink
     *
     * @return string
     */
    public function getPhotoWebLink()
    {
        return $this->photoWebLink;
    }

    /**
     * Set photoThumbnailWebLink
     *
     * @param string $photoThumbnailWebLink
     *
     * @return StaffCompany
     */
    public function setPhotoThumbnailWebLink($photoThumbnailWebLink)
    {
        $this->photoThumbnailWebLink = $photoThumbnailWebLink;

        return $this;
    }

    /**
     * Get photoThumbnailWebLink
     *
     * @return string
     */
    public function getPhotoThumbnailWebLink()
    {
        return $this->photoThumbnailWebLink;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return StaffCompany
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set thumbnailPhoto
     *
     * @param string $thumbnailPhoto
     *
     * @return StaffCompany
     */
    public function setThumbnailPhoto($thumbnailPhoto)
    {
        $this->thumbnailPhoto = $thumbnailPhoto;

        return $this;
    }

    /**
     * Get thumbnailPhoto
     *
     * @return string
     */
    public function getThumbnailPhoto()
    {
        return $this->thumbnailPhoto;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set user
     *
     * @param \ApiBundle\Entity\User $user
     *
     * @return StaffCompany
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \ApiBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return StaffCompany
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    /**
     * @return bool
     */
    private function getFileType() {
        /** @var UploadedFile $file */
        $file = $this->file;

        if ($file) {
            $arr = explode('/',$file->getMimeType());

            return $arr[1];
        } else return false;
    }

    /**
     * @return bool
     */
    public function saveFile() {
        if ($this->getFileType()) {
            $this->removeUpload();

            $bytes = random_bytes(15);
            $hexName = bin2hex($bytes);

            $fileName = 'staff_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();
            $thumbnailFileName = 'staff_thumbnail_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();

            $this->upload($fileName,$this->file->getPathname());

            $this->setPhoto($fileName);
            $this->setThumbnailPhoto($thumbnailFileName);

            $this->setPhotoWebLink($this->_directory.$fileName);
            $this->setPhotoThumbnailWebLink($this->_directory.$thumbnailFileName);

            return $fileName;
        }

        return false;
    }

    /**
     * @param $fileName
     */
    private function upload($fileName,$tmpName) {
        move_uploaded_file($tmpName,$this->_directory.$fileName);
    }

    /**
     *
     */
    public function removeUpload() {
        if (is_file($this->_directory.$this->photo)) {
            unlink($this->_directory.$this->photo);
        }
        if (is_file($this->_directory.$this->thumbnailPhoto)) {
            unlink($this->_directory.$this->thumbnailPhoto);
        }
    }

    /**
     * @return string
     */
    public function getFileSrc() {
        if (is_file($this->_directory.$this->photo)) {
            return "/{$this->_directory}".$this->photo;
        }

        return "";
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
