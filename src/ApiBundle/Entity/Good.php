<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Good
 *
 * @ORM\Table(name="good")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\GoodRepository")
 */
class Good
{
    private $_directory = 'uploads/good/';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="text", nullable=true)
     */
    private $shortDescription;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategory", inversedBy="good")
     * @ORM\JoinColumn(name="goodCategory", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategory;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestOne", inversedBy="good")
     * @ORM\JoinColumn(name="goodCategoryNestOne", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategoryNestOne;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestTwo", inversedBy="good")
     * @ORM\JoinColumn(name="goodCategoryNestTwo", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategoryNestTwo;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\GoodCategoryNestThree", inversedBy="good")
     * @ORM\JoinColumn(name="goodCategoryNestThree", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $goodCategoryNestThree;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable = true)
     */
    protected $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company", inversedBy="good")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="photoWebLink", type="string", length=255, nullable=true)
     */
    private $photoWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="originPhotoWebLink", type="string", length=255, nullable=true)
     */
    private $originPhotoWebLink;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Assert\File(maxSize="12000000",mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif"
     *      })
     */
    public $file;

    /**
     * @var bool
     *
     * @ORM\Column(name="isReturnable", type="boolean")
     */
    protected $isReturnable;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updateDate;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Good
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Good
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set shortDescription.
     *
     * @param string $shortDescription
     *
     * @return Good
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription.
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set goodCategory
     *
     * @param \ApiBundle\Entity\GoodCategory $goodCategory
     *
     * @return Good
     */
    public function setGoodCategory(\ApiBundle\Entity\GoodCategory $goodCategory = null)
    {
        $this->goodCategory = $goodCategory;

        return $this;
    }

    /**
     * Get goodCategory
     *
     * @return \ApiBundle\Entity\GoodCategory
     */
    public function getGoodCategory()
    {
        return $this->goodCategory;
    }

    /**
     * Set goodCategoryNestOne
     *
     * @param \ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne
     *
     * @return Good
     */
    public function setGoodCategoryNestOne(\ApiBundle\Entity\GoodCategoryNestOne $goodCategoryNestOne = null)
    {
        $this->goodCategoryNestOne = $goodCategoryNestOne;

        return $this;
    }

    /**
     * Get goodCategoryNestOne
     *
     * @return \ApiBundle\Entity\GoodCategoryNestOne
     */
    public function getGoodCategoryNestOne()
    {
        return $this->goodCategoryNestOne;
    }

    /**
     * Set goodCategoryNestTwo
     *
     * @param \ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo
     *
     * @return Good
     */
    public function setGoodCategoryNestTwo(\ApiBundle\Entity\GoodCategoryNestTwo $goodCategoryNestTwo = null)
    {
        $this->goodCategoryNestTwo = $goodCategoryNestTwo;

        return $this;
    }

    /**
     * Get goodCategoryNestTwo
     *
     * @return \ApiBundle\Entity\GoodCategoryNestTwo
     */
    public function getGoodCategoryNestTwo()
    {
        return $this->goodCategoryNestTwo;
    }

    /**
     * Set goodCategoryNestThree
     *
     * @param \ApiBundle\Entity\GoodCategoryNestThree $goodCategoryNestThree
     *
     * @return Good
     */
    public function setGoodCategoryNestThree(\ApiBundle\Entity\GoodCategoryNestThree $goodCategoryNestThree = null)
    {
        $this->goodCategoryNestThree = $goodCategoryNestThree;

        return $this;
    }

    /**
     * Get goodCategoryNestThree
     *
     * @return \ApiBundle\Entity\GoodCategoryNestThree
     */
    public function getGoodCategoryNestThree()
    {
        return $this->goodCategoryNestThree;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString()
    {
        return $this->getStatus()?"Активный":'Неактивный';
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return Good
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set photoWebLink
     *
     * @param string $photoWebLink
     *
     * @return Good
     */
    public function setPhotoWebLink($photoWebLink)
    {
        $this->photoWebLink = $photoWebLink;

        return $this;
    }

    /**
     * Get photoWebLink
     *
     * @return string
     */
    public function getPhotoWebLink()
    {
        return $this->photoWebLink;
    }

    /**
     * Set originPhotoWebLink
     *
     * @param string $originPhotoWebLink
     *
     * @return Good
     */
    public function setOriginPhotoWebLink($originPhotoWebLink)
    {
        $this->originPhotoWebLink = $originPhotoWebLink;

        return $this;
    }

    /**
     * Get originPhotoWebLink
     *
     * @return string
     */
    public function getOriginPhotoWebLink()
    {
        return $this->originPhotoWebLink;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Good
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @return bool
     */
    private function getFileType() {
        /** @var UploadedFile $file */
        $file = $this->file;

        if ($file) {
            $arr = explode('/',$file->getMimeType());

            return $arr[1];
        } else return false;
    }

    /**
     * @return bool
     */
    public function saveFile() {
        if ($this->getFileType()) {
            $this->removeUpload();

            $bytes = random_bytes(15);
            $hexName = bin2hex($bytes);

            $webLinkFileName = 'good_cmprss_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();
            $fileName = 'good_'.$hexName.'_'.$this->getId().'.'.$this->getFileType();

            $this->upload($fileName,$this->file->getPathname());

            $this->setPhoto($fileName);
            $this->setOriginPhotoWebLink($this->_directory.$fileName);
            $this->setPhotoWebLink($this->_directory.$webLinkFileName);

            return true;
        }

        return false;
    }

    /**
     * @param $fileName
     */
    private function upload($fileName,$tmpName){
        move_uploaded_file($tmpName,$this->_directory.$fileName);
    }

    /**
     *
     */
    public function removeUpload()
    {
        if (is_file($this->_directory.$this->photo)) {
            unlink($this->_directory.$this->photo);
        }
    }

    /**
     * @return string
     */
    public function getFileSrc(){
        if(is_file($this->_directory.$this->photo)){
            return "/{$this->_directory}".$this->photo;
        }
        return "";
    }

    /**
     * @param $isReturnable
     * @return $this
     */
    public function setIsReturnable($isReturnable)
    {
        $this->isReturnable = $isReturnable;

        return $this;
    }

    /**
     * Get isReturnable
     *
     * @return bool
     */
    public function getIsReturnable()
    {
        return $this->isReturnable;
    }

    public function getIsReturnableString()
    {
        return $this->getIsReturnable()?"Возвратный":'Невозвратный';
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param mixed $createDate
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->createDate = $updateDate;
    }

    public function __construct()
    {
        $this->createDate = new \DateTime('now');
        $this->updateDate = new \DateTime('now');
    }
}
