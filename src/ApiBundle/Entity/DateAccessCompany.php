<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DateAccessCompany
 *
 * @ORM\Table(name="date_access_company")
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\DateAccessCompanyRepository")
 */
class DateAccessCompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\Company", inversedBy="dateAccessCompany")
     * @ORM\JoinColumn(name="company", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="ApiBundle\Entity\ShopGroup", inversedBy="dateAccessCompany")
     * @ORM\JoinColumn(name="shopGroup", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $shopGroup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="shop", type="text", nullable=true)
     */
    private $shop;

    /**
     * @var string|null
     *
     * @ORM\Column(name="weekDay", type="text", nullable=true)
     */
    private $weekDay;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param \ApiBundle\Entity\Company $company
     *
     * @return DateAccessCompany
     */
    public function setCompany(\ApiBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \ApiBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set shopGroup
     *
     * @param \ApiBundle\Entity\ShopGroup $shopGroup
     *
     * @return DateAccessCompany
     */
    public function setShopGroup(\ApiBundle\Entity\ShopGroup $shopGroup = null)
    {
        $this->shopGroup = $shopGroup;

        return $this;
    }

    /**
     * Get shopGroup
     *
     * @return \ApiBundle\Entity\ShopGroup
     */
    public function getShopGroup()
    {
        return $this->shopGroup;
    }

    /**
     * Set shop.
     *
     * @param string|null $shop
     *
     * @return DateAccessCompany
     */
    public function setShop($shop = null)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop.
     *
     * @return string|null
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set weekDay.
     *
     * @param string|null $weekDay
     *
     * @return DateAccessCompany
     */
    public function setWeekDay($weekDay = null)
    {
        $this->weekDay = $weekDay;

        return $this;
    }

    /**
     * Get weekDay.
     *
     * @return string|null
     */
    public function getWeekDay()
    {
        return $this->weekDay;
    }

    /**
     * Get weekDay as array
     */
    public function getWeekDayAssArray() {
        return unserialize($this->weekDay);
    }

    /**
     * Get shop as array
     */
    public function getShopAssArray() {
        return unserialize($this->shop);
    }
}
