<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ShopConfigurationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isAllowConsignment',ChoiceType::class,[
                'label' => 'Разрешить консигнацию',
                'choices' => [
                    'Да' => 1,
                    'Нет' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => $options['data'] && $options['data']->getId()? $options['data']->getIsAllowConsignment() : 0
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\ShopConfiguration'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_shopconfiguration';
    }


}
