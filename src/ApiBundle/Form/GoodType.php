<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class GoodType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->company = $options['company'];

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Описание',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('shortDescription', TextareaType::class, array(
                'label' => 'Краткое описание',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('price', NumberType::class, array(
                'label' => 'Цена',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('status',ChoiceType::class,[
                'label' => 'Статус',
                'choices' => [
                    'Активный' => 1,
                    'Неактивный' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => 1
            ])
            ->add('isReturnable',ChoiceType::class,[
                'label' => '(Не)Возвратный',
                'choices' => [
                    'Возвратный' => 1,
                    'Невозвратный' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => 0
            ])
            ->add('file', FileType::class,[
                'label' => 'Фото',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('goodCategory',EntityType::class,[
                'label' => 'Категория',
                'class' => 'ApiBundle\Entity\GoodCategory',
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('q')
                        ->where('q.company = :companyId')
                        ->setParameter('companyId',  $this->company->getId())
                        ->orderBy('q.name', 'ASC');
                },
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSetData(FormEvent $event) {
        $entity = $event->getData();
        $form = $event->getForm();

        if ($entity->getGoodCategoryNestOne()) {
            $form->add('goodCategoryNestOne', EntityType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> false,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestOne',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.goodCategory = :goodCategory')
                        ->setParameter('goodCategory', $entity->getGoodCategoryNestOne()->getGoodCategory()->getId())
                        ->orderBy('q.name', 'ASC');
                },
                'data' =>  $entity->getGoodCategoryNestOne(),
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'
            ]);
        } else {
            $form->add('goodCategoryNestOne', ChoiceType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> false,
                'choices'=> [],
                "attr" => [
                    "class" => "form-control"
                ]
            ]);
        }

        if ($entity->getGoodCategoryNestTwo()) {
            $form->add('goodCategoryNestTwo', EntityType::class, [
                'label'=> 'Категория (2-ая вложенность)',
                'required'=> false,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestTwo',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.goodCategoryNestOne = :goodCategoryNestOne')
                        ->setParameter('goodCategoryNestOne', $entity->getGoodCategoryNestOne()->getId())
                        ->orderBy('q.name', 'ASC');
                },
                'data' =>  $entity->getGoodCategoryNestTwo(),
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'
            ]);
        } else {
            $form->add('goodCategoryNestTwo', ChoiceType::class, [
                'label'=> 'Категория (2-ая вложенность)',
                'required'=> false,
                'choices'=> [],
                "attr" => [
                    "class" => "form-control"
                ]
            ]);
        }
    }

    public function onPreSubmit(FormEvent $event) {
        $entity = $event->getData();
        $form = $event->getForm();

        if (isset($entity['goodCategoryNestOne']) && $entity['goodCategoryNestOne']) {
            $form->add('goodCategoryNestOne', EntityType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> false,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestOne',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.id = :id')
                        ->setParameter('id', $entity['goodCategoryNestOne'])
                        ->orderBy('q.name', 'ASC');
                },
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'

            ]);
        } else {
            $form->add('goodCategoryNestOne', ChoiceType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> false,
                'choices'=> []
            ]);
        }

        if (isset($entity['goodCategoryNestTwo']) && $entity['goodCategoryNestTwo']) {
            $form->add('goodCategoryNestTwo', EntityType::class, [
                'label'=> 'Категория (2-ая вложенность)',
                'required'=> false,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestTwo',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.id = :id')
                        ->setParameter('id', $entity['goodCategoryNestTwo'])
                        ->orderBy('q.name', 'ASC');
                },
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'

            ]);
        } else {
            $form->add('goodCategoryNestTwo', ChoiceType::class, [
                'label'=> 'Категория (2-ая вложенность)',
                'required'=> false,
                'choices'=> []
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Good',
            'company' => 'ApiBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_good';
    }

    /** @var Company */
    private $company = null;
}
