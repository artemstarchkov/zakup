<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class StaffCompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => 'Имя',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('secondName', TextType::class, array(
                'label' => 'Фамилия',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Отчество',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Телефон',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('address', TextType::class, array(
                'label' => 'Адрес',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('file', FileType::class,[
                'label' => 'Фото',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\StaffCompany'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_staffcompany';
    }


}
