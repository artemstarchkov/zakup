<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class GoodCategoryNestOneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->companyId = $options['companyId'];

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('goodCategory',EntityType::class,[
                'label' => 'Категория',
                'class' => 'ApiBundle\Entity\GoodCategory',
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('q')
                        ->where('q.company = :company')
                        ->setParameter('company', $this->companyId)
                        ->orderBy('q.name', 'ASC');
                },
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('status',ChoiceType::class,[
                'label' => 'Статус',
                'choices' => [
                    'Активный' => 1,
                    'Неактивный' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => 1
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\GoodCategoryNestOne',
            'companyId' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_goodcategorynestone';
    }

    /** @var null | int */
    private $companyId = null;

}
