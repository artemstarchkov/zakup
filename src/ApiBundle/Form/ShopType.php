<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ShopType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email (почта)',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required' => false
            ))
            ->add('address', TextType::class, array(
                'label' => 'Адрес',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'Телефон',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('latitude', TextType::class, array(
                'label' => 'Широта',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('longitude', TextType::class, array(
                'label' => 'Долгота',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('file', FileType::class,[
                'label' => 'Фото',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('status',ChoiceType::class,[
                'label' => 'Статус',
                'choices' => [
                    'Активный' => 1,
                    'Неактивный' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => 1
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Shop'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_shop';
    }


}
