<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingSiteMapType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('zoom', IntegerType::class, array(
                'label' => 'Масштаб',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('centerLatitude', TextType::class, array(
                'label' => 'Широта',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('centerLongitude', TextType::class, array(
                'label' => 'Долгота',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\SettingSiteMap'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_settingsitemap';
    }


}
