<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->userData = $options['data'];
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Логин',
                'translation_domain' => 'FOSUserBundle',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'required' => true
            ))
            ->add('plainPassword', PasswordType::class, array(
                'required' => true,
                'label' => 'Пароль',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('specialRole', ChoiceType::class, array(
               'label' => 'Роль',
               'choices' => array(
                   'Компания' => 'ROLE_COMPANY',
                   'Магазин' => 'ROLE_SHOP'
               ),
                'attr' => array(
                    'class' => 'form-control'
                ),
                'placeholder' => 'Выберите роль...',
                'required' => true
            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_user';
    }

    private $userData;

}
