<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class GoodCategoryNestTwoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->companyId = $options['companyId'];

        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('goodCategory',EntityType::class,[
                'label' => 'Категория',
                'class' => 'ApiBundle\Entity\GoodCategory',
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('q')
                        ->where('q.company = :company')
                        ->setParameter('company', $this->companyId)
                        ->orderBy('q.name', 'ASC');
                },
                'required' => true,
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            /*->add('goodCategoryNestOne',ChoiceType::class,[
                'label' => 'Категория (1-ая вложенность)',
                //'class' => 'ApiBundle\Entity\GoodCategoryNestOne',
                'choices' => [],
                'attr' => array(
                    'class' => 'form-control'
                ),
                'expanded' => false,
                'multiple' => false,
                'required' => true
            ])*/
            ->add('status',ChoiceType::class,[
                'label' => 'Статус',
                'choices' => [
                    'Активный' => 1,
                    'Неактивный' => 0,
                ],
                'expanded' => true,
                'required' => true,
                'data' => 1
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }

    public function onPreSetData(FormEvent $event) {
        $entity = $event->getData();
        $form = $event->getForm();

        if ($entity->getGoodCategoryNestOne()) {
            $form->add('goodCategoryNestOne', EntityType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> true,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestOne',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.goodCategory = :goodCategory')
                        ->setParameter('goodCategory', $entity->getGoodCategoryNestOne()->getGoodCategory()->getId())
                        ->orderBy('q.name', 'ASC');
                },
                'data' =>  $entity->getGoodCategoryNestOne(),
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'
            ]);
        } else {
            $form->add('goodCategoryNestOne', ChoiceType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> true,
                'choices'=> [],
                "attr" => [
                    "class" => "form-control"
                ]
            ]);
        }
    }

    public function onPreSubmit(FormEvent $event) {
        $entity = $event->getData();
        $form = $event->getForm();

        if (isset($entity['goodCategoryNestOne']) && $entity['goodCategoryNestOne']) {
            $form->add('goodCategoryNestOne', EntityType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> true,
                'class'=> 'ApiBundle\Entity\GoodCategoryNestOne',
                'query_builder' => function (EntityRepository $er) use ($entity) {
                    return $er->createQueryBuilder('q')
                        ->where('q.id = :id')
                        ->setParameter('id', $entity['goodCategoryNestOne'])
                        ->orderBy('q.name', 'ASC');
                },
                "attr" => [
                    "class" => "form-control"
                ],
                'choice_label' => 'name'

            ]);
        } else {
            $form->add('goodCategoryNestOne', ChoiceType::class, [
                'label'=> 'Категория (1-ая вложенность)',
                'required'=> true,
                'choices'=> []
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\GoodCategoryNestTwo',
            'companyId' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'apibundle_goodcategorynesttwo';
    }

    /** @var null | int */
    private $companyId = null;

}
