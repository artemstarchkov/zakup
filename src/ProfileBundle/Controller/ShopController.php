<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\Shop;
use ApiBundle\Entity\ShopConfiguration;
use ApiBundle\Form\ShopConfigurationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShopController
 *
 * @Route("/profile/shop", name="profile_shop_")
 */
class ShopController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:Shop')->createQueryBuilder('s')
            ->orderBy('s.name', 'ASC')
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:Shop:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/show/{id}", name="show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $shop = null;

        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('id'));

        return $this->render('ProfileBundle:Shop:show.html.twig', array(
            'shop' => $shop
        ));
    }

    /**
     * Display shops on map
     *
     * @Route("/map", name="show_on_map")
     * @Method({"GET", "POST"})
     */
    public function showOnMap(Request $request) {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $em = $this->getDoctrine()->getManager();

        $shops = $em->getRepository('ApiBundle:Shop')->getInfoForMap();
        $settingSiteMap = $em->getRepository('ApiBundle:SettingSiteMap')->findOneBy(array(
            'company' => $company->getId()
        ));

        $jsonShops = json_encode($shops);

        return $this->render('@Profile/Shop/map.html.twig', [
            'shops' => $jsonShops,
            'settingSiteMap' => $settingSiteMap
        ]);
    }

    /**
     * Display shops on map
     *
     * @Route("/{shopGroupId}/{shopId}/configuration", name="configuration")
     * @Method({"GET", "POST"})
     */
    public function configurationAction(Request $request) {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $em = $this->getDoctrine()->getManager();
        $shopConfiguration = null;
        $shop = null;

        /** @var Shop $shop */
        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('shopId'));

        if (!$shop) {
            throw  $this->createNotFoundException('Shop not found');
        }

        if ($company) {
            $shopConfiguration = $em->getRepository('ApiBundle:ShopConfiguration')->findOneBy(array(
                'shop' => $shop->getId(),
                'company' => $company->getId()
            ));
        } else {
            throw  $this->createNotFoundException('Company not found');
        }

        if ($company && !$shopConfiguration) {
            $shopConfiguration = new ShopConfiguration();
            $shopConfiguration->setCompany($company);
            $shopConfiguration->setShop($shop);
        }

        $form = $this->createForm(ShopConfigurationType::class, $shopConfiguration, array());
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $shopConfiguration->setUpdateDate(new \DateTime('now'));

            $em->persist($shopConfiguration);
            $em->flush();

            return $this->redirectToRoute('profile_shop_group_shop_list', ['shopGroupId' => $request->get('shopGroupId')]);
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('@Profile/Shop/configuration.html.twig', [
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'shopGroupId' => $request->get('shopGroupId')
        ]);
    }
}
