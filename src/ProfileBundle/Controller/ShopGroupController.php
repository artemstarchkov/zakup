<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\DateAccessCompany;
use ApiBundle\Entity\ShopGroup;
use ApiBundle\Entity\User;
use ApiBundle\Form\ShopGroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShopGroupController
 *
 * @Route("/profile/shop_group", name="profile_shop_group_")
 */
class ShopGroupController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:ShopGroup')->createQueryBuilder('sg')
            ->where('sg.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:ShopGroup:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Create new ShopGroup
     *
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $shopGroup = new ShopGroup();

        $form = $this->createForm(ShopGroupType::class, $shopGroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $shopGroup->setCompany($user->getCompany());

            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('profile_shop_group_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:ShopGroup:new.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit ShopGroup
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $user = $this->getUser();
        $shopGroup = null;
        $em =$this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        $form = $this->createForm(ShopGroupType::class, $shopGroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('profile_shop_group_index', array());
        }

        return $this->render('ProfileBundle:ShopGroup:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/shop/list/{shopGroupId}", name="shop_list")
     * @Method({"GET", "POST"})
     */
    public function shopListAction(Request $request)
    {
        $user = $this->getUser();
        $shopGroupId = $request->get('shopGroupId');
        $em = $this->getDoctrine()->getManager();
        $shopList = array();
        $shopIds = array();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $shopGroupId,
            'company' => $user->getCompany()->getId()
        ));

        $shopGroupList = $em->getRepository('ApiBundle:ShopGroup')->findBy(array(
            'company' => $user->getCompany()->getId()
        ));

        foreach ($shopGroupList as $shopGroupItem) {
            foreach ($shopGroupItem->getShop() as $shopItem) {
                $shopIds[] = $shopItem->getId();
            }
        }

        $shopIds = array_unique($shopIds);

        foreach ($shopGroup->getShop() as $shopItem) {
            $shopIds[] = $shopItem->getId();
        }

        $query = $em->getRepository('ApiBundle:Shop')->createQueryBuilder('s');

        if (count($shopIds) > 0) {
            $query->where($query->expr()->notIn('s.id', $shopIds));
        }

        $shopList = $query->getQuery()->getResult();

        return $this->render('ProfileBundle:ShopGroup:shopList.html.twig', array(
            'shopGroup' => $shopGroup,
            'shopList' => $shopList
        ));
    }

    /**
     * Delete ShopGroup
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $shopGroup = null;
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        if (!$shopGroup) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($shopGroup);
        $em->flush();

        return $this->redirectToRoute('profile_shop_group_index', array());
    }

    /**
     * Add Shop to ShopGroup
     *
     * @Route("/add/shop/{shopGroupId}", name="add_shop")
     * @Method({"GET", "POST"})
     */
    public function addShopAction(Request $request)
    {
        $user = $this->getUser();
        $shopGroup = null;
        $dateAccessCompany = null;
        $shopIds = array();
        $shopId = $request->get('shop');
        $em =$this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $request->get('shopGroupId'),
            'company' => $user->getCompany()->getId()
        ));

        if ($request->getMethod() == 'POST') {
            $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
                'company' => $user->getCompany()->getId(),
                'shopGroup' => $shopGroup->getId()
            ));

            $shop = $em->getRepository('ApiBundle:Shop')->find($shopId);
            $shopGroup->addShop($shop);

            if ($dateAccessCompany) {
                foreach ($shopGroup->getShop() as $shop) {
                    $shopIds[] = $shop->getId();
                }

                $dateAccessCompany->setShop(serialize($shopIds));
                $em->persist($dateAccessCompany);
            }

            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('profile_shop_group_shop_list', array(
                'shopGroupId' => $shopGroup->getId()
            ));
        }

        return $this->redirectToRoute('profile_shop_group_shop_list', array(
            'shopGroupId' => $shopGroup->getId()
        ));
    }

    /**
     * Delete Shop from ShopGroup
     *
     * @Route("/delete/shop/from/group/{shopGroupId}/{shopId}", name="delete_shop")
     * @Method({"GET", "POST"})
     */
    public function deleteShopAction(Request $request)
    {
        $user = $this->getUser();
        $shopGroup = null;
        $shop = null;
        $dateAccessCompany = null;
        $shopIds = array();
        $em = $this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $request->get('shopGroupId'),
            'company' => $user->getCompany()->getId()
        ));

        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('shopId'));

        if (!$shopGroup || !$shop) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
            'company' => $user->getCompany()->getId(),
            'shopGroup' => $shopGroup->getId()
        ));

        $shopGroup->removeShop($shop);

        if ($dateAccessCompany) {
            foreach ($shopGroup->getShop() as $shop) {
                $shopIds[] = $shop->getId();
            }

            $dateAccessCompany->setShop(serialize($shopIds));
            $em->persist($dateAccessCompany);
        }

        $em->persist($shopGroup);
        $em->flush();

        return $this->redirectToRoute('profile_shop_group_shop_list', array(
            'shopGroupId' => $shopGroup->getId()
        ));
    }

    /**
     * Set date company access to ShopGroup and it's shops
     *
     * @Route("/set_date/company_access/{shopGroupId}", name="set_date_company_access")
     * @Method({"GET", "POST"})
     */
    public function setDateCompanyAccessAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $shopGroup = null;
        $weekDay = array();
        $shops = array();
        $dateAccessCompany = null;
        $weekDayList = array(
            'Понедельник' => 1,
            'Вторник' => 2,
            'Среда' => 3,
            'Четверг' => 4,
            'Пятница' => 5,
            'Суббота' => 6,
            'Воскресенье' => 7
        );
        $em = $this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->findOneBy(array(
            'id' => $request->get('shopGroupId'),
            'company' => $user->getCompany()->getId()
        ));

        if (!$shopGroup || !$user->getCompany()) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
            'company' => $user->getCompany()->getId(),
            'shopGroup' => $shopGroup->getId()
        ));

        if ($request->getMethod() == 'POST') {
            $weekDay = $request->get('weekDay');

            if (!$dateAccessCompany) {
                $dateAccessCompany = new DateAccessCompany();
            }

            foreach ($shopGroup->getShop() as $shop) {
                $shops[] = $shop->getId();
            }

            $dateAccessCompany->setCompany($user->getCompany());
            $dateAccessCompany->setShopGroup($shopGroup);
            $dateAccessCompany->setWeekDay(serialize($weekDay));
            $dateAccessCompany->setShop(serialize($shops));

            $em->persist($dateAccessCompany);
            $em->flush();

            return $this->redirectToRoute('profile_shop_group_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:ShopGroup:dateAccessCompany.html.twig', array(
            'shopGroup' => $shopGroup,
            'dateAccessCompany' => $dateAccessCompany,
            'weekDayList' => $weekDayList,
            'csrf_token' => $csrfToken
        ));
    }

}
