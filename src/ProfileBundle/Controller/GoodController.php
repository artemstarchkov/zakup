<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Good;
use ApiBundle\Entity\User;
use ApiBundle\Form\GoodType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoodController
 *
 * @Route("/profile/good", name="profile_good_")
 */
class GoodController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:Good')->createQueryBuilder('g')
            ->where('g.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:Good:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * New Good
     *
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        /** @var Good $good */
        $good = new Good();

        $form = $this->createForm(GoodType::class, $good, array(
            'company' => $user->getCompany()
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            // Сохраняем файл
            if($good->file){
                $em->persist($good);
                $em->flush();

                $good->saveFile();
                $result = false;
                $size = '128x';

                $result = $fileService->compressImageFile($good->getOriginPhotoWebLink(), $good->getPhotoWebLink(), $size);
            }

            $good->setCompany($user->getCompany());

            $em->persist($good);
            $em->flush();

            return $this->redirectToRoute('profile_good_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:Good:new.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit Good
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $good = null;
        $em =$this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        $good = $em->getRepository('ApiBundle:Good')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        $form = $this->createForm(GoodType::class, $good, array(
            'company' => $user->getCompany()
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($good);
            $em->flush();

            // Сохраняем файл
            if($good->file){
                $good->saveFile();

                $result = false;
                $size = '128x';

                $result = $fileService->compressImageFile($good->getOriginPhotoWebLink(), $good->getPhotoWebLink(), $size);
            }

            $em->persist($good);
            $em->flush();

            return $this->redirectToRoute('profile_good_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:Good:edit.html.twig', array(
            'form' => $form->createView(),
            'good' => $good,
            'csrf_token' => $csrfToken,
        ));
    }

    /**
     * Delete Good
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $good = null;
        $em = $this->getDoctrine()->getManager();

        $good = $em->getRepository('ApiBundle:Good')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        if (!$good) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $good->removeUpload();

        $em->remove($good);
        $em->flush();

        return $this->redirectToRoute('profile_good_index', array());
    }

}
