<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\DateAccessCompany;
use ApiBundle\Entity\Good;
use ApiBundle\Entity\ShopGroup;
use ApiBundle\Form\GoodType;
use ApiBundle\Form\ShopGroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CompanyController
 *
 * @Route("/profile/company", name="profile_company_")
 */
class CompanyController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        return $this->render('ProfileBundle:Company:index.html.twig');
    }
}
