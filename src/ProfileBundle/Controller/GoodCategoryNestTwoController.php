<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\GoodCategoryNestOne;
use ApiBundle\Entity\GoodCategoryNestTwo;
use ApiBundle\Entity\User;
use ApiBundle\Form\GoodCategoryNestTwoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class GoodCategoryNestTwoController
 *
 * @Route("/profile/good_category_nest_two", name="profile_good_category_nest_two_")
 */
class GoodCategoryNestTwoController extends Controller
{
    /**
     * Add new GoodCategoryNestTwo
     *
     * @Route("/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var GoodCategoryNestTwo $goodCategoryNestTwo */
        $goodCategoryNestTwo = new GoodCategoryNestTwo();

        $form = $this->createForm(GoodCategoryNestTwoType::class, $goodCategoryNestTwo, array(
            'companyId' => $user->getCompany()->getId()
        ));

        if ($form->isSubmitted() && $request->getMethod() == Request::METHOD_POST) {
            /** @var GoodCategoryNestOne $goodCategoryNestOne */
            $goodCategoryNestOne = null;
            $goodCategoryNestOneId = null;

            if ($request->get('apibundle_goodcategorynesttwo')['goodCategoryNestOne']) {
                $goodCategoryNestOneId = $request->get('apibundle_goodcategorynesttwo')['goodCategoryNestOne'];
                $goodCategoryNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($goodCategoryNestOneId);
                $goodCategoryNestTwo->setGoodCategoryNestOne($goodCategoryNestOne);
            }
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em->persist($goodCategoryNestTwo);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:GoodCategoryNestTwo:add.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit GoodCategory
     *
     * @Route("/edit/{goodCategoryId}/{goodCategoryNestOneId}/{id}", name="edit")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategoryNestTwo = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryNestOneId' => $request->get('goodCategoryNestOneId'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $user->getCompany()->getId()
        );

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findOneByCAndGCAndGCNOAndId($options);

        $form = $this->createForm(GoodCategoryNestTwoType::class, $goodCategoryNestTwo, array(
            'companyId' => $user->getCompany()->getId()
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($goodCategoryNestTwo);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        return $this->render('ProfileBundle:GoodCategoryNestTwo:edit.html.twig', array(
            'form' => $form->createView(),
            'goodCategoryNestTwo' => $goodCategoryNestTwo
        ));
    }

    /**
     * Delete GoodCategory
     *
     * @Route("/delete/{goodCategoryId}/{goodCategoryNestOneId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function deleteAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategoryNestTwo = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryNestOneId' => $request->get('goodCategoryNestOneId'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $user->getCompany()->getId()
        );

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findOneByCAndGCAndGCNOAndId($options);

        if (!$goodCategoryNestTwo) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($goodCategoryNestTwo);
        $em->flush();

        return $this->redirectToRoute('profile_good_category_index', array());
    }

    /**
     * @Route("/get/list/by/good_category_nest_one", name="get_list_by_good_category_nest_one")
     * @Method({"GET", "POST"})
     */
    public function getListGoodCategoryNestTwoAjaxAction(Request $request) {
        $goodCategoryNestOneId = null;
        $goodCategoryNestTwo = array();
        $goodCategoryNestOneId = $request->get('goodCategoryNestOneId');
        $em = $this->getDoctrine()->getManager();

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findByGoodCategoryNestOne($goodCategoryNestOneId);

        return new JsonResponse($goodCategoryNestTwo);
    }

}
