<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\BookingItem;
use ApiBundle\Entity\Company;
use ApiBundle\Entity\HistoryBooking;
use ApiBundle\Entity\HistoryBookingItem;
use ApiBundle\Entity\User;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BookingController
 *
 * @Route("/profile/booking", name="profile_booking_")
 */
class BookingController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $bookingStatuses = [Booking::NEW, Booking::ACCEPTED, Booking::IN_PROGRESS, Booking::COMPLETED];
        $selectedBookingStatus = null;

        if (Request::METHOD_POST === $request->getMethod()) {
            $request->getSession()->set('selectedBookingStatusFilter', $request->get('bookingStatus'));
        }

        $selectedBookingStatus = $request->getSession()->get('selectedBookingStatusFilter')? $request->getSession()->get('selectedBookingStatusFilter') : null;

        $query = $em->getRepository('ApiBundle:Booking')->createQueryBuilder('b')
            ->where('b.sellerCompany = :companyId')
            ->setParameter('companyId', $user->getCompany()->getId());

        if ($selectedBookingStatus) {
            $query->andWhere('b.status = :status')
                ->setParameter('status', $selectedBookingStatus);
        }

        $query->orderBy('b.createdDate', 'DESC')
              ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:Booking:index.html.twig', array(
            'pagination' => $pagination,
            'bookingService' => $bookingService,
            'bookingStatuses' => $bookingStatuses,
            'selectedBookingStatus' => $selectedBookingStatus
        ));
    }

    /**
     * @Route("/show/{bookingId}", name="show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request)
    {
        $user = $this->getUser();
        $bookingId = null;
        $booking = null;
        $staffsCompany = array();
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');

        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $user->getCompany()->getId()
        ));

        $staffsCompany = $em->getRepository('ApiBundle:StaffCompany')->findBy(array(
            'company' => $user->getCompany()->getId()
        ));

        return $this->render('ProfileBundle:Booking:show.html.twig', array(
            'booking' => $booking,
            'bookingService' => $bookingService,
            'staffsCompany' => $staffsCompany
        ));
    }

    /**
     * @Route("/set/deliveryman", name="set_deliveryman")
     * @Method({"GET", "POST"})
     */
    public function setDeliveryManAjax(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $staffId = null;
        $bookingId = null;
        $staff = null;
        $staffInfo = '';

        /** @var Booking $booking */
        $booking = null;

        /** @var HistoryBooking $historyBooking */
        $historyBooking = null;

        $em = $this->getDoctrine()->getManager();

        $staffId = $request->get('staffId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $user->getCompany()->getId()
        ));

        if (!$booking) {
            return new JsonResponse(array('isSetDeliveryMan' => false, 'message' => 'Заказ не найден'));
        }

        $staff = $em->getRepository('ApiBundle:StaffCompany')->find($staffId);

        if ($staff) {
            $booking->setStatus(Booking::ACCEPTED);

            $staffInfo = "ФИО: {$staff->getSecondName()} {$staff->getFirstName()} {$staff->getLastName()}<br />
                          Телефон: {$staff->getPhone()}<br />
                          Адрес: {$staff->getAddress()}<br />
                          Компания: {$staff->getCompany()->getName()}<br />";
        } else {
            $booking->setStatus(Booking::NEW);
        }

        // Get bookingItems(s) with consignment
        $bookingItems = $em->getRepository('ApiBundle:BookingItem')->findByBookingAndConsignment($booking, true, false);

        // Set to consignment delivery company's staff
        if($bookingItems) {
            /** @var BookingItem $bookingItem */
            foreach ($bookingItems as $bookingItem) {
                $bookingItem->getConsignment()->setDeliveryStaffCompany($staff);

                $em->persist($bookingItem);
            }
        }

        $booking->setDeliveryStaffCompany($staff);
        $booking->setUpdatedDate(new \DateTime('now'));

        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneByBooking($booking);
        $historyBooking->setStatus($booking->getStatus());
        $historyBooking->setUpdatedDate(new \DateTime('now'));
        $historyBooking->setDeliveryManInfo($staffInfo);

        $em->persist($booking);
        $em->persist($historyBooking);
        $em->flush();

        return new JsonResponse(array('isSetDeliveryMan' => true));
    }

    /**
     * @Route("/history", name="history")
     * @Method({"GET", "POST"})
     */
    public function historyAction(Request $request)
    {
        $user = $this->getUser();
        $paginator = $this->get('knp_paginator');
        $limit = 50;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $currentDate = new \DateTime('now');

        $query = $em->getRepository('ApiBundle:HistoryBooking')->createQueryBuilder('hb')
            ->where('hb.sellerCompany = :companyId')
            ->setParameter('companyId', $user->getCompany()->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:Booking:history.html.twig', array(
            'pagination' => $pagination,
            'bookingService' => $bookingService,
            'startDate' => $currentDate->format('Y-m-d'),
            'endDate' => $currentDate->format('Y-m-d')
        ));
    }

    /**
     * @Route("/show/history/{historyBookingId}", name="show_history")
     * @Method({"GET", "POST"})
     */
    public function showHistoryAction(Request $request)
    {
        $user = $this->getUser();
        $historyBooking = null;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');

        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneBy(array(
            'id' => $request->get('historyBookingId'),
            'sellerCompany' => $user->getCompany()->getId()
        ));

        return $this->render('ProfileBundle:Booking:showHistory.html.twig', array(
            'historyBooking' => $historyBooking,
            'bookingService' => $bookingService
        ));
    }

    /**
     * @Route("/generate/history/pdf/order", name="generate_history_pdf_order")
     * @Method({"GET", "POST"})
     */
    public function generateHistoryBookingPdfOrderAction(Request $request) {
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $filename = 'заказы_отчёт_' . $startDate . '_' . $endDate . '.pdf';

        /** @var Company|null $company */
        $company = $this->getUser()->getCompany();

        /** @var HistoryBooking[] $historyBookings */
        $historyBookings = [];
        $historyBookingPdfTemplateOrder = '';

        $historyBookings = $em->getRepository('ApiBundle:HistoryBooking')->findBySellerCompanyAndRangeDates($company, $startDate, $endDate);

        $historyBookingPdfTemplateOrder = $this->renderView('@Profile/Booking/OrderTemplates/historyBookingPdf.html.twig', [
           'historyBookings' => $historyBookings,
           'bookingService' => $bookingService
        ]);

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($historyBookingPdfTemplateOrder),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="'. $filename .'"',
                'encoding' => 'UTF-8'
            )
        );
    }

    /**
     * Display shops on map
     *
     * @Route("/map", name="show_on_map")
     * @Method({"GET", "POST"})
     */
    public function showOnMap(Request $request) {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $result = array();
        $search =  '!"#$%&/()=?*+\'-.,;:_' ;
        $search = str_split($search);

        $settingSiteMap = $em->getRepository('ApiBundle:SettingSiteMap')->findOneBy(array(
            'company' => $company->getId()
        ));

        $statuses = [Booking::NEW, Booking::ACCEPTED, Booking::IN_PROGRESS, Booking::COMPLETED];

        /** @var Booking[] $bookings */
        $bookings = $em->getRepository('ApiBundle:Booking')->getListForMapByCompanyAndStatuses($company, $statuses);

        foreach ($bookings as $booking) {
            $tmpBooking = array();
            $tmpBookingItems = array();

            //$result[$booking->getId()] = array(
            $tmpBooking = array(
                'bId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerAddress' => str_replace($search, "", $booking->getCustomerUser()->getShop()->getAddress()),
                'customerLatitude' => $booking->getCustomerUser()->getShop()->getLatitude(),
                'customerLongitude' => $booking->getCustomerUser()->getShop()->getLongitude(),
                'customerPhone' => $booking->getCustomerUser()->getShop()->getPhone(),
                'customerShopName' => str_replace($search, "", $booking->getCustomerUser()->getShop()->getName()),
                'customerShopImage' => $booking->getCustomerUser()->getShop()->getPhotoWebLink(),
                'bStatus' => $booking->getStatus()
            );

            /** @var BookingItem $bookingItem */
            foreach ($booking->getBookingItem() as $bookingItem) {
                $tmpBookingItems[] = array(
                    'goodName' => str_replace($search, "", $bookingItem->getGood()->getName()),
                    'amount' => $bookingItem->getAmount()
                );
            }

            $tmpBooking['bookingItems'] = $tmpBookingItems;

            $result[] = $tmpBooking;
        }

        return $this->render('@Profile/Booking/map.html.twig', [
            'bookings' => json_encode($result),
            'statuses' => $statuses,
            'settingSiteMap' => $settingSiteMap,
            'bookingService' => $bookingService
        ]);
    }

    /**
     * Filter bookings for map by parameters
     *
     * @Route("/filter/map", name="filter_on_map")
     * @Method({"GET", "POST"})
     */
    public function filterBookingOnMapAjax(Request $request) {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $search =  '!"#$%&/()=?*+\'-.,;:_' ;
        $search = str_split($search);
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $statuses = [];

        if (!$request->get('status')) {
            $statuses = [Booking::NEW, Booking::ACCEPTED, Booking::IN_PROGRESS, Booking::COMPLETED];
        } else {
            $statuses[] = $request->get('status');
        }
        $result = array();

        /** @var Booking[] $bookings */
        $bookings = $em->getRepository('ApiBundle:Booking')->getListForMapByCompanyAndStatuses($company, $statuses);

        foreach ($bookings as $booking) {
            $tmpBooking = array();
            $tmpBookingItems = array();

            $tmpBooking = array(
                'bId' => $booking->getId(),
                'number' => $booking->getNumber(),
                'bPrice' => $bookingService->calculateTotalPrice($booking, false),
                'customerAddress' => str_replace($search, "", $booking->getCustomerUser()->getShop()->getAddress()),
                'customerLatitude' => $booking->getCustomerUser()->getShop()->getLatitude(),
                'customerLongitude' => $booking->getCustomerUser()->getShop()->getLongitude(),
                'customerPhone' => $booking->getCustomerUser()->getShop()->getPhone(),
                'customerShopName' => str_replace($search, "", $booking->getCustomerUser()->getShop()->getName()),
                'customerShopImage' => $booking->getCustomerUser()->getShop()->getPhotoWebLink(),
                'bStatus' => $booking->getStatus()
            );

            /** @var BookingItem $bookingItem */
            foreach ($booking->getBookingItem() as $bookingItem) {
                $tmpBookingItems[] = array(
                    'goodName' => str_replace($search, "", $bookingItem->getGood()->getName()),
                    'amount' => $bookingItem->getAmount()
                );
            }

            $tmpBooking['bookingItems'] = $tmpBookingItems;

            $result[] = $tmpBooking;
        }

        return new JsonResponse($result);
    }
}
