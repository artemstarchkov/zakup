<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\SettingSiteMap;
use ApiBundle\Form\SettingSiteMapType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StaffCompanyController
 *
 * @Route("/profile/setting_site/map", name="profile_setting_site_map_")
 */
class SettingSiteMapController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $em = $this->getDoctrine()->getManager();

        $settingSiteMap = $em->getRepository('ApiBundle:SettingSiteMap')->findOneBy(array(
            'company' => $company->getId()
        ));

        if (!$settingSiteMap) {
            $settingSiteMap = new SettingSiteMap();
        }

        $form = $this->createForm(SettingSiteMapType::class, $settingSiteMap);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $settingSiteMap->setCompany($company);
            $settingSiteMap->setUpdateDate(new \DateTime('now'));

            $em->persist($settingSiteMap);
            $em->flush();

            return $this->render('ProfileBundle:SettingSiteMap:index.html.twig', array(
                'form' => $form->createView()
            ));
        }

        return $this->render('ProfileBundle:SettingSiteMap:index.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
