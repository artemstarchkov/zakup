<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\GoodCategory;
use ApiBundle\Entity\User;
use ApiBundle\Form\GoodCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class GoodCategoryController
 *
 * @Route("/profile/good_category", name="profile_good_category_")
 */
class GoodCategoryController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $query = $em->getRepository('ApiBundle:GoodCategory')->createQueryBuilder('gc')
            ->where('gc.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:GoodCategory:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Add new GoodCategory
     *
     * @Route("/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var GoodCategory $goodCategory */
        $goodCategory = new GoodCategory();

        $form = $this->createForm(GoodCategoryType::class, $goodCategory);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $goodCategory->setCompany($user->getCompany());

            $em->persist($goodCategory);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:GoodCategory:add.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit GoodCategory
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategory = null;
        $em =$this->getDoctrine()->getManager();
        $goodCategory = $em->getRepository('ApiBundle:GoodCategory')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        $form = $this->createForm(GoodCategoryType::class, $goodCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($goodCategory);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        return $this->render('ProfileBundle:GoodCategory:edit.html.twig', array(
            'form' => $form->createView(),
            'goodCategory' => $goodCategory
        ));
    }

    /**
     * Delete GoodCategory
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategory = null;
        $em = $this->getDoctrine()->getManager();

        $goodCategory = $em->getRepository('ApiBundle:GoodCategory')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        if (!$goodCategory) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($goodCategory);
        $em->flush();

        return $this->redirectToRoute('profile_good_category_index', array());
    }

}
