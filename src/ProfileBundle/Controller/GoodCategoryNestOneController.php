<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\GoodCategoryNestOne;
use ApiBundle\Entity\User;
use ApiBundle\Form\GoodCategoryNestOneType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class GoodCategoryNestOneController
 *
 * @Route("/profile/good_category_nest_one", name="profile_good_category_nest_one_")
 */
class GoodCategoryNestOneController extends Controller
{
    /**
     * Add new GoodCategoryNestOne
     *
     * @Route("/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var GoodCategoryNestOne $goodCategoryNestOne */
        $goodCategoryNestOne = new GoodCategoryNestOne();

        $form = $this->createForm(GoodCategoryNestOneType::class, $goodCategoryNestOne, array(
            'companyId' => $user->getCompany()->getId()
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $em->persist($goodCategoryNestOne);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:GoodCategoryNestOne:add.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit GoodCategory
     *
     * @Route("/edit/{goodCategoryId}/{id}", name="edit")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function editAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategoryNestOne = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $user->getCompany()->getId()
        );

        $goodCategoryNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->findOneByCAndGCAndId($options);

        $form = $this->createForm(GoodCategoryNestOneType::class, $goodCategoryNestOne, array(
            'companyId' => $user->getCompany()->getId()
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($goodCategoryNestOne);
            $em->flush();

            return $this->redirectToRoute('profile_good_category_index', array());
        }

        return $this->render('ProfileBundle:GoodCategoryNestOne:edit.html.twig', array(
            'form' => $form->createView(),
            'goodCategoryNestOne' => $goodCategoryNestOne
        ));
    }

    /**
     * Delete GoodCategory
     *
     * @Route("/delete/{goodCategoryId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function deleteAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $goodCategoryNestOne = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $user->getCompany()->getId()
        );

        $goodCategoryNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->findOneByCAndGCAndId($options);

        if (!$goodCategoryNestOne) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($goodCategoryNestOne);
        $em->flush();

        return $this->redirectToRoute('profile_good_category_index', array());
    }

    /**
     * @Route("/get/list/by/good_category", name="get_list_by_good_category")
     * @Method({"GET", "POST"})
     */
    public function getListGoodCategoryNestOneAjaxAction(Request $request) {
        $goodCategoryId = null;
        $goodCategoryNestOne = array();
        $goodCategoryId = $request->get('goodCategoryId');
        $em = $this->getDoctrine()->getManager();

        $goodCategoryNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->findByGoodCategory($goodCategoryId);

        return new JsonResponse($goodCategoryNestOne);
    }

}
