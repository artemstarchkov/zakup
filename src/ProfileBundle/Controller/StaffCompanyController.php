<?php

namespace ProfileBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\StaffCompany;
use ApiBundle\Entity\User;
use ApiBundle\Form\StaffCompanyType;
use ApiBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StaffCompanyController
 *
 * @Route("/profile/staff_company", name="profile_staff_company_")
 */
class StaffCompanyController extends Controller
{
    /**
     * @Route("/index", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:StaffCompany')->createQueryBuilder('sc')
            ->where('sc.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('ProfileBundle:StaffCompany:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * New StaffCompany
     *
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Company $company */
        $company = $this->getUser()->getCompany();
        $manager = $this->get('fos_user.user_manager');
        $fileService = $this->get('app.file_service');

        /** @var User $user */
        $user = $manager->createUser();

        /** @var StaffCompany $staffCompany */
        $staffCompany = new StaffCompany();

        $userForm = $this->createForm(UserType::class,$user);
        $form = $this->createForm(StaffCompanyType::class,$staffCompany);

        $userForm->handleRequest($request);
        $form->handleRequest($request);

        if ($userForm->isSubmitted() and $userForm->isValid()) {
            $user->setSpecialRole('ROLE_STAFF_COMPANY');
            $user->setEnabled(true);
            $manager->updateUser($user);

            // Сохраняем файл
            if ($staffCompany->file) {
                $em->persist($staffCompany);
                $em->flush();

                $staffCompany->saveFile();

                $result = false;
                $size = '256x';
                $thumbnailSize = '128x';

                $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoThumbnailWebLink(), $thumbnailSize);

                if ($result) {
                    $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoWebLink(), $size);
                }
            }

            $staffCompany->setCompany($company);
            $staffCompany->setUser($user);

            $em->persist($staffCompany);
            $em->flush();

            return $this->redirectToRoute('profile_staff_company_index', array());
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('ProfileBundle:StaffCompany:new.html.twig', array(
            'form' => $form->createView(),
            'userForm' => $userForm->createView(),
            'staffCompany' => $staffCompany,
            'csrf_token' => $csrfToken
        ));
    }

    /**
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $staffCompany = null;

        /** @var User $user */
        $user = $this->getUser();
        $em =$this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');
        $staffCompany = $em->getRepository('ApiBundle:StaffCompany')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $user->getCompany()->getId()
        ));

        $staffCompanyForm = $this->createForm(StaffCompanyType::class, $staffCompany);
        $userForm = $this->createForm(UserType::class, $staffCompany->getUser());

        $staffCompanyForm->handleRequest($request);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {

            // Set again "ROLE_STAFF_COMPANY" role, because we haven't it role in "specialRole" select,
            // and "specialRole" field for user set in "NULL" when edit "staffCompany"
            $staffCompany->getUser()->setSpecialRole('ROLE_STAFF_COMPANY');

            // Сохраняем файл
            if($staffCompany->file){
                $staffCompany->saveFile();

                $result = false;
                $size = '256x';
                $thumbnailSize = '128x';

                $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoThumbnailWebLink(), $thumbnailSize);

                if ($result) {
                    $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoWebLink(), $size);
                }
            }

            $em->persist($staffCompany);
            $em->flush();

            $manager = $this->get('fos_user.user_manager');
            $manager->updateUser($staffCompany->getUser());

            return $this->redirectToRoute('profile_staff_company_index', array());
        }

        return $this->render('ProfileBundle:StaffCompany:edit.html.twig', array(
            'staffCompanyForm' => $staffCompanyForm->createView(),
            'staffCompany' => $staffCompany,
            'userForm' => $userForm->createView()
        ));
    }

    /**
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = null;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'id' => $request->get('id'),
            'specialRole' => 'ROLE_STAFF_COMPANY'
        ));

        if (!$user) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $user->getStaffCompany()->removeUpload();

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('profile_staff_company_index', array());
    }

    /**
     * Check on exist staffCompany (user) by username
     *
     * @Route("/check/exist_username", name="check_exist_username")
     * @Method({"GET", "POST"})
     */
    public function checkExistLoginAction(Request $request) {
        $user = null;
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'username' => $request->get('username')
        ));

        if ($user) {
            return new JsonResponse(['isExist' => true, 'message' => 'Пользователь с данным логином уже создан в системе. Используйте другой логин.']);
        } else {
            return new JsonResponse(['isExist' => false, 'message' => '']);
        }
    }
}
