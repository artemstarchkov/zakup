<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;

class UserService
{
    /**
     * @var Request
     */
    protected $request;

    /** @var Container */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
    }

    public function convertUserRole($role) {
        $roleList = $this->container->getParameter('role_list');

        return $roleList[$role]? $roleList[$role] : '';
    }

}