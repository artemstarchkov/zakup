<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;

class FileService
{
    /**
     * @var Request
     */
    protected $request;

    /** @var Container */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
    }


    /**
     * Example:
     *      $localFilePath = 'here/path/to/local/file/image.jpg' - your local file path
     *      $targetFilePath = 'here/path/to/target/file/newImage.jpg' - path where file will be saved after compress
     *      $size = '128x' - size with image will be compress (128x, 256x, 512x, ...) whatever "number" value
     *
     * @param $localFilePath
     * @param $targetFilePath
     * @param $size
     * @return bool
     */
    public function compressImageFile($localFilePath, $targetFilePath, $size) {

        // Prepare command to compress image file
        $command = 'convert '.$localFilePath.' -resize '.$size.' '.$targetFilePath;

        exec($command, $output, $return);

        // Check if command was successfully
        if (!$return) {
            return true;
        } else {
            return false;
        }
    }

}