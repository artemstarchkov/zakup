<?php

namespace AdminBundle\Service;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\HistoryBooking;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;

class BookingService
{
    /**
     * @var Request
     */
    protected $request;

    /** @var Container */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
    }

    public function calculatePriceByAmount($pricePerOne, $amount) {
        return ($amount * $pricePerOne);
    }

    /**
     * Calculate total booking price by received/not received booking item using flag "isReceivedOnly"
     *
     * @param Booking $booking
     * @param bool $isReceivedOnly
     * @return float|int|null
     */
    public function calculateTotalPrice(Booking $booking, $isReceivedOnly = false) {
        $totalPrice = null;

        if ($isReceivedOnly) {
            foreach ($booking->getBookingItem() as $item) {
                if ($item->getReceived()) {
                    $totalPrice += ($item->getAmount() * $item->getPricePerOne());
                }
            }
        } else {
            foreach ($booking->getBookingItem() as $item) {
                $totalPrice += ($item->getAmount() * $item->getPricePerOne());
            }
        }

        return $totalPrice;
    }

    /**
     * Calculate total history booking price by received/not received booking item using flag "isReceivedOnly"
     *
     * @param HistoryBooking $historyBooking
     * @param bool $isReceivedOnly
     * @return float|int|null
     */
    public function calculateTotalHistoryBookingPrice(HistoryBooking $historyBooking, $isReceivedOnly = false) {
        $totalPrice = null;

        if ($isReceivedOnly) {
            foreach ($historyBooking->getHistoryBookingItem() as $item) {
                if ($item->getReceived()) {
                    $totalPrice += ($item->getAmount() * $item->getPricePerOne());
                }
            }
        } else {
            foreach ($historyBooking->getHistoryBookingItem() as $item) {
                $totalPrice += ($item->getAmount() * $item->getPricePerOne());
            }
        }

        return $totalPrice;
    }

    public function getBookingStatusByLocale($status, $locale = 'ru') {
        $statusRu = array(
          Booking::NEW => 'новый',
          Booking::IN_PROGRESS => 'выполняется',
          Booking::ACCEPTED => 'принят',
          Booking::COMPLETED => 'выполнен'
        );

        switch ($locale) {
            case 'ru':
                return $statusRu[$status];
                break;
            default:
                break;
        }
    }

    public function getBookingColorByStatus($status) {
        $statusRu = array(
            Booking::NEW => 'booking-status-new',
            Booking::IN_PROGRESS => 'booking-status-in-progress',
            Booking::ACCEPTED => 'booking-status-accepted',
            Booking::COMPLETED => 'booking-status-completed'
        );

        return $statusRu[$status];
    }

    public function getBookingIsDeletedByLocale($isDeleted, $locale = 'ru') {

        switch ($locale) {
            case 'ru':
                return $isDeleted? 'Да' : 'Нет';
                break;
            default:
                return $isDeleted? 'Да' : 'Нет';
                break;
        }
    }

}