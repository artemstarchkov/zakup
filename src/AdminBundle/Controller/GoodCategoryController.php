<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\GoodCategory;
use ApiBundle\Entity\GoodCategoryNestOne;
use ApiBundle\Entity\GoodCategoryNestTwo;
use ApiBundle\Form\GoodCategoryNestOneType;
use ApiBundle\Form\GoodCategoryNestTwoType;
use ApiBundle\Form\GoodCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoodCategoryController
 *
 * @Route("/admin/good_category", name="admin_good_category_")
 */
class GoodCategoryController extends Controller
{
    /**
     * @Route("/{companyId}", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:GoodCategory')->createQueryBuilder('gc')
            ->where('gc.company = :company')
            ->setParameter('company', $request->get('companyId'))
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:GoodCategory:index.html.twig', array(
            'pagination' => $pagination,
            'companyId' => $request->get('companyId')
        ));
    }

    /**
     * Add new GoodCategory
     *
     * @Route("/{companyId}/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $company = null;
        $em = $this->getDoctrine()->getManager();

        /** @var GoodCategory $goodCategory */
        $goodCategory = new GoodCategory();

        $form = $this->createForm(GoodCategoryType::class, $goodCategory);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));
            $goodCategory->setCompany($company);

            $em->persist($goodCategory);
            $em->flush();

            return $this->redirectToRoute('admin_good_category_index', array(
                'companyId' => $request->get('companyId')
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:GoodCategory:add.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'companyId' => $request->get('companyId')
        ));
    }

    /**
     * Edit GoodCategory
     *
     * @Route("/edit/{companyId}/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $goodCategory = null;
        $em =$this->getDoctrine()->getManager();
        $goodCategory = $em->getRepository('ApiBundle:GoodCategory')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $request->get('companyId')
        ));

        $form = $this->createForm(GoodCategoryType::class, $goodCategory);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($goodCategory);
            $em->flush();

            return $this->redirectToRoute('admin_good_category_index', array(
                'companyId' => $request->get('companyId')
            ));
        }

        return $this->render('AdminBundle:GoodCategory:edit.html.twig', array(
            'form' => $form->createView(),
            'goodCategory' => $goodCategory
        ));
    }

    /**
     * Delete GoodCategory
     *
     * @Route("/delete/{companyId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $goodCategory = null;
        $em = $this->getDoctrine()->getManager();

        $goodCategory = $em->getRepository('ApiBundle:GoodCategory')->find($request->get('id'));

        if (!$goodCategory) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($goodCategory);
        $em->flush();

        return $this->redirectToRoute('admin_good_category_index', array(
            'companyId' => $request->get('companyId')
        ));
    }

}
