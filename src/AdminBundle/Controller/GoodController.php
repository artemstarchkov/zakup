<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Good;
use ApiBundle\Form\GoodType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoodController
 *
 * @Route("/admin/good", name="admin_good_")
 */
class GoodController extends Controller
{
    /**
     * @Route("/{companyId}/list", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $companyId = $request->get('companyId');
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:Good')->createQueryBuilder('g')
            ->where('g.company = :company')
            ->setParameter('company', $companyId)
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:Good:index.html.twig', array(
            'pagination' => $pagination,
            'companyId' => $companyId
        ));
    }

    /**
     * New Good
     *
     * @Route("/{companyId}/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $companyId = $request->get('companyId');
        $company = null;
        $em = $this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        /** @var Good $good */
        $good = new Good();

        $company = $em->getRepository('ApiBundle:Company')->find($companyId);

        $form = $this->createForm(GoodType::class, $good, array(
            'company' => $company
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            // Сохраняем файл
            if($good->file){
                $em->persist($good);
                $em->flush();

                $good->saveFile();
                $result = false;
                $size = '128x';

                $result = $fileService->compressImageFile($good->getOriginPhotoWebLink(), $good->getPhotoWebLink(), $size);
            }

            $good->setCompany($company);

            $em->persist($good);
            $em->flush();

            return $this->redirectToRoute('admin_good_index', array(
                'companyId' => $companyId
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:Good:new.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'company' => $company
        ));
    }

    /**
     * Edit Good
     *
     * @Route("/{companyId}/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $good = null;
        $company = null;
        $companyId = $request->get('companyId');
        $em =$this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        $good = $em->getRepository('ApiBundle:Good')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $companyId
        ));

        $company = $em->getRepository('ApiBundle:Company')->find($companyId);

        $form = $this->createForm(GoodType::class, $good, array(
            'company' => $company
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Сохраняем файл
            if($good->file){
                $good->saveFile();

                $result = false;
                $size = '128x';

                $result = $fileService->compressImageFile($good->getOriginPhotoWebLink(), $good->getPhotoWebLink(), $size);
            }

            $em->persist($good);
            $em->flush();

            return $this->redirectToRoute('admin_good_index', array(
                'companyId' => $companyId
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:Good:edit.html.twig', array(
            'form' => $form->createView(),
            'good' => $good,
            'company' => $company,
            'csrf_token' => $csrfToken,
        ));
    }

    /**
     * Delete Good
     *
     * @Route("/delete/{companyId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $good = null;
        $companyId = $request->get('companyId');
        $em = $this->getDoctrine()->getManager();

        $good = $em->getRepository('ApiBundle:Good')->findOneBy(array(
            'id' => $request->get('id'),
            'company' => $companyId
        ));

        if (!$good) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $good->removeUpload();

        $em->remove($good);
        $em->flush();

        return $this->redirectToRoute('admin_good_index', array(
            'companyId' => $companyId
        ));
    }

}
