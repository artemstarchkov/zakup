<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShopController
 *
 * @Route("/admin/shop", name="admin_shop_")
 */
class ShopController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:Shop')->createQueryBuilder('s')
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:Shop:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/show/{id}", name="show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request)
    {
        $shop = null;
        $em = $this->getDoctrine()->getManager();

        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('id'));

        return $this->render('AdminBundle:Shop:show.html.twig', array(
            'shop' => $shop
        ));
    }
}
