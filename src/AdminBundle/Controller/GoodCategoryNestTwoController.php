<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\GoodCategoryNestOne;
use ApiBundle\Entity\GoodCategoryNestTwo;
use ApiBundle\Form\GoodCategoryNestTwoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GoodCategoryNestTwoController
 *
 * @Route("/admin/good_category_nest_two", name="admin_good_category_nest_two_")
 */
class GoodCategoryNestTwoController extends Controller
{
    /**
     * Add new GoodCategoryNestTwo
     *
     * @Route("/{companyId}/add", name="add")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var GoodCategoryNestTwo $goodCategoryNestTwo */
        $goodCategoryNestTwo = new GoodCategoryNestTwo();

        $form = $this->createForm(GoodCategoryNestTwoType::class, $goodCategoryNestTwo, array(
            'companyId' => $request->get('companyId')
        ));

        if ($form->isSubmitted() && $request->getMethod() == Request::METHOD_POST) {
            /** @var GoodCategoryNestOne $goodCategoryNestOne */
            $goodCategoryNestOne = null;
            $goodCategoryNestOneId = null;

            if ($request->get('apibundle_goodcategorynesttwo')['goodCategoryNestOne']) {
                $goodCategoryNestOneId = $request->get('apibundle_goodcategorynesttwo')['goodCategoryNestOne'];
                $goodCategoryNestOne = $em->getRepository('ApiBundle:GoodCategoryNestOne')->find($goodCategoryNestOneId);
                $goodCategoryNestTwo->setGoodCategoryNestOne($goodCategoryNestOne);
            }
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {

            $em->persist($goodCategoryNestTwo);
            $em->flush();

            return $this->redirectToRoute('admin_good_category_index', array(
                'companyId' => $request->get('companyId')
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:GoodCategoryNestTwo:add.html.twig', array(
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'companyId' => $request->get('companyId')
        ));
    }

    /**
     * Edit GoodCategory
     *
     * @Route("/edit/{companyId}/{goodCategoryId}/{goodCategoryNestOneId}/{id}", name="edit")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function editAction(Request $request)
    {
        $goodCategoryNestTwo = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryNestOneId' => $request->get('goodCategoryNestOneId'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $request->get('companyId')
        );

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findOneByCAndGCAndGCNOAndId($options);

        $form = $this->createForm(GoodCategoryNestTwoType::class, $goodCategoryNestTwo, array(
            'companyId' => $request->get('companyId')
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($goodCategoryNestTwo);
            $em->flush();

            return $this->redirectToRoute('admin_good_category_index', array(
                'companyId' => $request->get('companyId')
            ));
        }

        return $this->render('AdminBundle:GoodCategoryNestTwo:edit.html.twig', array(
            'form' => $form->createView(),
            'goodCategoryNestTwo' => $goodCategoryNestTwo
        ));
    }

    /**
     * Delete GoodCategory
     *
     * @Route("/delete/{companyId}/{goodCategoryId}/{goodCategoryNestOneId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function deleteAction(Request $request)
    {
        $goodCategoryNestTwo = null;
        $options = array();
        $em =$this->getDoctrine()->getManager();

        $options = array(
            'id' => $request->get('id'),
            'goodCategoryNestOneId' => $request->get('goodCategoryNestOneId'),
            'goodCategoryId' => $request->get('goodCategoryId'),
            'companyId' => $request->get('companyId')
        );

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findOneByCAndGCAndGCNOAndId($options);

        if (!$goodCategoryNestTwo) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($goodCategoryNestTwo);
        $em->flush();

        return $this->redirectToRoute('admin_good_category_index', array(
            'companyId' => $request->get('companyId')
        ));
    }

    /**
     * @Route("/get/list/by/good_category_nest_one", name="get_list_by_good_category_nest_one")
     * @Method({"GET", "POST"})
     */
    public function getListGoodCategoryNestTwoAjaxAction(Request $request) {
        $goodCategoryNestOneId = null;
        $goodCategoryNestTwo = array();
        $goodCategoryNestOneId = $request->get('goodCategoryNestOneId');
        $em = $this->getDoctrine()->getManager();

        $goodCategoryNestTwo = $em->getRepository('ApiBundle:GoodCategoryNestTwo')->findByGoodCategoryNestOne($goodCategoryNestOneId);

        return new JsonResponse($goodCategoryNestTwo);
    }

}
