<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Booking;
use ApiBundle\Entity\BookingItem;
use ApiBundle\Entity\Consignment;
use ApiBundle\Entity\HistoryBooking;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BookingController
 *
 * @Route("/admin/booking", name="admin_booking_")
 */
class BookingController extends Controller
{
    /**
     * @Route("/{companyId}/list", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $companyId = null;
        $companyId = $request->get('companyId');
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');
        $bookingStatuses = [Booking::NEW, Booking::ACCEPTED, Booking::IN_PROGRESS, Booking::COMPLETED];
        $selectedBookingStatus = null;

        if (Request::METHOD_POST === $request->getMethod()) {
            $request->getSession()->set('selectedBookingStatusFilter', $request->get('bookingStatus'));
        }

        $selectedBookingStatus = $request->getSession()->get('selectedBookingStatusFilter')? $request->getSession()->get('selectedBookingStatusFilter') : null;

        $query = $em->getRepository('ApiBundle:Booking')->createQueryBuilder('b')
            ->where('b.sellerCompany = :companyId')
            ->setParameter('companyId', $companyId);

        if ($selectedBookingStatus) {
            $query->andWhere('b.status = :status')
                  ->setParameter('status', $selectedBookingStatus);
        }

        $query->orderBy('b.createdDate', 'DESC')
              ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:Booking:index.html.twig', array(
            'pagination' => $pagination,
            'bookingService' => $bookingService,
            'bookingStatuses' => $bookingStatuses,
            'companyId' => $companyId,
            'selectedBookingStatus' => $selectedBookingStatus
        ));
    }

    /**
     * @Route("/show/{companyId}/{bookingId}", name="show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request)
    {
        $staffsCompany = array();
        $companyId = null;
        $bookingId = null;
        $booking = null;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');

        $companyId = $request->get('companyId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
           'sellerCompany' => $companyId
        ));

        $staffsCompany = $em->getRepository('ApiBundle:StaffCompany')->findBy(array(
            'company' => $companyId
        ));

        return $this->render('AdminBundle:Booking:show.html.twig', array(
            'booking' => $booking,
            'companyId' => $companyId,
            'bookingService' => $bookingService,
            'staffsCompany' => $staffsCompany
        ));
    }

    /**
     * @Route("/set/deliveryman", name="set_deliveryman")
     * @Method({"GET", "POST"})
     */
    public function setDeliveryManAjaxAction(Request $request)
    {
        $staffId = null;
        $companyId = null;
        $bookingId = null;
        $staffInfo = '';

        /** @var BookingItem[]|[]|null $bookingItems */
        $bookingItems = [];

        /** @var Booking $booking */
        $booking = null;

        /** @var HistoryBooking $historyBooking */
        $historyBooking = null;

        $staff = null;
        $em = $this->getDoctrine()->getManager();

        $staffId = $request->get('staffId');
        $companyId = $request->get('companyId');
        $bookingId = $request->get('bookingId');

        $booking = $em->getRepository('ApiBundle:Booking')->findOneBy(array(
            'id' => $bookingId,
            'sellerCompany' => $companyId
        ));

        if (!$booking) {
            return new JsonResponse(array('isSetDeliveryMan' => false, 'message' => 'Заказ не найден'));
        }

        $staff = $em->getRepository('ApiBundle:StaffCompany')->find($staffId);

        if ($staff) {
            $booking->setStatus(Booking::ACCEPTED);

            $staffInfo = "ФИО: {$staff->getSecondName()} {$staff->getFirstName()} {$staff->getLastName()}<br />
                          Телефон: {$staff->getPhone()}<br />
                          Адрес: {$staff->getAddress()}<br />
                          Компания: {$staff->getCompany()->getName()}<br />";
        } else {
            $booking->setStatus(Booking::NEW);
        }

        // Get bookingItems(s) with consignment
        $bookingItems = $em->getRepository('ApiBundle:BookingItem')->findByBookingAndConsignment($booking, true, false);

        // Set to consignment delivery company's staff
        if($bookingItems) {
            /** @var BookingItem $bookingItem */
            foreach ($bookingItems as $bookingItem) {
                $bookingItem->getConsignment()->setDeliveryStaffCompany($staff);

                $em->persist($bookingItem);
            }
        }

        $booking->setDeliveryStaffCompany($staff);
        $booking->setUpdatedDate(new \DateTime('now'));

        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneByBooking($booking);
        $historyBooking->setStatus($booking->getStatus());
        $historyBooking->setUpdatedDate(new \DateTime('now'));
        $historyBooking->setDeliveryManInfo($staffInfo);

        $em->persist($booking);
        $em->persist($historyBooking);
        $em->flush();

        return new JsonResponse(array('isSetDeliveryMan' => true));
    }

    /**
     * @Route("/history/{companyId}", name="history")
     * @Method({"GET", "POST"})
     */
    public function historyAction(Request $request)
    {
        $company = null;
        $paginator = $this->get('knp_paginator');
        $limit = 50;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');

        $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));

        $query = $em->getRepository('ApiBundle:HistoryBooking')->createQueryBuilder('hb')
            ->where('hb.sellerCompany = :companyId')
            ->setParameter('companyId', $company->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:Booking:history.html.twig', array(
            'pagination' => $pagination,
            'bookingService' => $bookingService,
            'companyId' => $company->getId()
        ));
    }

    /**
     * @Route("/show/history/{companyId}/{historyBookingId}", name="show_history")
     * @Method({"GET", "POST"})
     */
    public function showHistoryAction(Request $request)
    {
        $company = null;
        $historyBooking = null;
        $em = $this->getDoctrine()->getManager();
        $bookingService = $this->get('app.booking_service');

        $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));
        $historyBooking = $em->getRepository('ApiBundle:HistoryBooking')->findOneBy(array(
            'id' => $request->get('historyBookingId'),
            'sellerCompany' => $company->getId()
        ));

        return $this->render('AdminBundle:Booking:showHistory.html.twig', array(
            'historyBooking' => $historyBooking,
            'bookingService' => $bookingService,
            'companyId' => $company->getId()
        ));
    }

}
