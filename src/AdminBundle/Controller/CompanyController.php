<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\User;
use ApiBundle\Form\CompanyType;
use ApiBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CompanyController
 *
 * @Route("/admin/company", name="admin_company_")
 */
class CompanyController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:Company')->createQueryBuilder('c')
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:Company:index.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * Edit company
     *
     * @Route("/staff/list/{companyId}", name="staff_list")
     * @Method({"GET", "POST"})
     */
    public function staffListAction(Request $request)
    {
        $staffList = array();
        $company = null;
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));
        $staffList = $em->getRepository('ApiBundle:StaffCompany')->findBy(array(
           'company' => $request->get('companyId')
        ));

        return $this->render('AdminBundle:Company:staffList.html.twig', array(
            'staffList' => $staffList,
            'company' => $company
        ));
    }

    /**
     * User login
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = null;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'id' => $request->get('id'),
            'specialRole' => 'ROLE_COMPANY'
        ));

        if (!$user) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $user->getCompany()->removeUpload();

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_user_index');
    }

}
