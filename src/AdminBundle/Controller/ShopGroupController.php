<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\DateAccessCompany;
use ApiBundle\Entity\ShopGroup;
use ApiBundle\Form\ShopGroupType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ShopGroupController
 *
 * @Route("/admin/shop_group", name="admin_shop_group_")
 */
class ShopGroupController extends Controller
{
    /**
     * @Route("/{companyId}", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $company = null;
        $shopGroups = array();
        $companyId = $request->get('companyId');
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('ApiBundle:Company')->find($companyId);

        $paginator = $this->get('knp_paginator');
        $limit = 20;

        $query = $em->getRepository('ApiBundle:ShopGroup')->createQueryBuilder('sg')
            ->where('sg.company = :company')
            ->setParameter('company', $company->getId())
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);

        return $this->render('AdminBundle:ShopGroup:index.html.twig', array(
            'pagination' => $pagination,
            'company' => $company
        ));
    }

    /**
     * Create new ShopGroup
     *
     * @Route("/new/{companyId}", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $company = null;

        /** @var ShopGroup $shopGroup */
        $shopGroup = new ShopGroup();

        $form = $this->createForm(ShopGroupType::class, $shopGroup);

        $form->handleRequest($request);

        $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));

        if ($form->isSubmitted() and $form->isValid()) {
            $shopGroup->setCompany($company);

            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('admin_shop_group_index', array(
                'companyId' => $company->getId()
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:ShopGroup:new.html.twig', array(
            'form' => $form->createView(),
            'company' => $company,
            'csrf_token' => $csrfToken
        ));
    }

    /**
     * Edit ShopGroup
     *
     * @Route("/edit/{companyId}/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $shopGroup = null;
        $em =$this->getDoctrine()->getManager();
        $companyId = $request->get('companyId');
        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($request->get('id'));

        $form = $this->createForm(ShopGroupType::class, $shopGroup);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('admin_shop_group_index', array(
                'companyId' => $companyId
            ));
        }

        return $this->render('AdminBundle:ShopGroup:edit.html.twig', array(
            'form' => $form->createView(),
            'companyId' => $companyId
        ));
    }

    /**
     * Delete ShopGroup
     *
     * @Route("/delete/{companyId}/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $shopGroup = null;
        $companyId = $request->get('companyId');
        $em = $this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($request->get('id'));

        if (!$shopGroup) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $em->remove($shopGroup);
        $em->flush();

        return $this->redirectToRoute('admin_shop_group_index', array(
            'companyId' => $companyId
        ));
    }

    /**
     * @Route("/shop/list/{companyId}/{shopGroupId}", name="shop_list")
     * @Method({"GET", "POST"})
     */
    public function shopListAction(Request $request)
    {
        $shopGroupId = $request->get('shopGroupId');
        $companyId = $request->get('companyId');
        $em = $this->getDoctrine()->getManager();
        $shopList = array();
        $shopIds = array();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($shopGroupId);

        $shopGroupList = $em->getRepository('ApiBundle:ShopGroup')->findBy(array(
            'company' => $companyId
        ));

        foreach ($shopGroupList as $shopGroupItem) {
            foreach ($shopGroupItem->getShop() as $shopItem) {
                $shopIds[] = $shopItem->getId();
            }
        }

        $shopIds = array_unique($shopIds);

        $query = $em->getRepository('ApiBundle:Shop')->createQueryBuilder('s');

        if (count($shopIds) > 0) {
            $query->where($query->expr()->notIn('s.id', $shopIds));
        }

        $shopList = $query->getQuery()->getResult();

        return $this->render('AdminBundle:ShopGroup:shopList.html.twig', array(
            'shopGroup' => $shopGroup,
            'companyId' => $companyId,
            'shopList' => $shopList
        ));
    }

    /**
     * Add Shop to ShopGroup
     *
     * @Route("/add/shop/{companyId}/{shopGroupId}", name="add_shop")
     * @Method({"GET", "POST"})
     */
    public function addShopAction(Request $request)
    {
        $shopGroup = null;
        $dateAccessCompany = null;
        $shopIds = array();
        $shopId = $request->get('shop');
        $companyId = $request->get('companyId');
        $em =$this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($request->get('shopGroupId'));

        if ($request->getMethod() == 'POST') {
            $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
                'company' => $companyId,
                'shopGroup' => $shopGroup->getId()
            ));

            $shop = $em->getRepository('ApiBundle:Shop')->find($shopId);
            $shopGroup->addShop($shop);

            if ($dateAccessCompany) {
                foreach ($shopGroup->getShop() as $shop) {
                    $shopIds[] = $shop->getId();
                }

                $dateAccessCompany->setShop(serialize($shopIds));
                $em->persist($dateAccessCompany);
            }

            $em->persist($shopGroup);
            $em->flush();

            return $this->redirectToRoute('admin_shop_group_shop_list', array(
                'companyId' => $companyId,
                'shopGroupId' => $shopGroup->getId()
            ));
        }

        return $this->redirectToRoute('admin_shop_group_shop_list', array(
            'companyId' => $companyId,
            'shopGroupId' => $shopGroup->getId()
        ));
    }

    /**
     * Delete Shop from ShopGroup
     *
     * @Route("/delete/shop/from/group/{companyId}/{shopGroupId}/{shopId}", name="delete_shop")
     * @Method({"GET", "POST"})
     */
    public function deleteShopAction(Request $request)
    {
        $shopGroup = null;
        $shop = null;
        $dateAccessCompany = null;
        $shopIds = array();
        $companyId = $request->get('companyId');
        $em = $this->getDoctrine()->getManager();

        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($request->get('shopGroupId'));
        $shop = $em->getRepository('ApiBundle:Shop')->find($request->get('shopId'));

        if (!$shopGroup || !$shop) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
            'company' => $companyId,
            'shopGroup' => $shopGroup->getId()
        ));

        $shopGroup->removeShop($shop);

        if ($dateAccessCompany) {
            foreach ($shopGroup->getShop() as $shop) {
                $shopIds[] = $shop->getId();
            }

            $dateAccessCompany->setShop(serialize($shopIds));
            $em->persist($dateAccessCompany);
        }

        $em->persist($shopGroup);
        $em->flush();

        return $this->redirectToRoute('admin_shop_group_shop_list', array(
            'companyId' => $companyId,
            'shopGroupId' => $shopGroup->getId()
        ));
    }

    /**
     * Set date company access to ShopGroup and it's shops
     *
     * @Route("/set_date/company_access/{companyId}/{shopGroupId}", name="set_date_company_access")
     * @Method({"GET", "POST"})
     */
    public function setDateCompanyAccessAction(Request $request)
    {
        $company = null;
        $shopGroup = null;
        $weekDay = array();
        $shops = array();
        $dateAccessCompany = null;
        $weekDayList = array(
            'Понедельник' => 1,
            'Вторник' => 2,
            'Среда' => 3,
            'Четверг' => 4,
            'Пятница' => 5,
            'Суббота' => 6,
            'Воскресенье' => 7
        );
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));
        $shopGroup = $em->getRepository('ApiBundle:ShopGroup')->find($request->get('shopGroupId'));

        if (!$shopGroup || !$company) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $dateAccessCompany = $em->getRepository('ApiBundle:DateAccessCompany')->findOneBy(array(
            'company' => $company->getId(),
            'shopGroup' => $shopGroup->getId()
        ));

        if ($request->getMethod() == 'POST') {
            $weekDay = $request->get('weekDay');

            if (!$dateAccessCompany) {
                $dateAccessCompany = new DateAccessCompany();
            }

            foreach ($shopGroup->getShop() as $shop) {
                $shops[] = $shop->getId();
            }

            $dateAccessCompany->setCompany($company);
            $dateAccessCompany->setShopGroup($shopGroup);
            $dateAccessCompany->setWeekDay(serialize($weekDay));
            $dateAccessCompany->setShop(serialize($shops));

            $em->persist($dateAccessCompany);
            $em->flush();

            return $this->redirectToRoute('admin_shop_group_index', array(
                'companyId' => $company->getId()
            ));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:ShopGroup:dateAccessCompany.html.twig', array(
            'shopGroup' => $shopGroup,
            'company' => $company,
            'dateAccessCompany' => $dateAccessCompany,
            'weekDayList' => $weekDayList,
            'csrf_token' => $csrfToken
        ));
    }

}
