<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\Company;
use ApiBundle\Entity\Shop;
use ApiBundle\Entity\StaffCompany;
use ApiBundle\Entity\User;
use ApiBundle\Form\CompanyType;
use ApiBundle\Form\ShopType;
use ApiBundle\Form\StaffCompanyType;
use ApiBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class UserController
 *
 * @Route("/admin/user", name="admin_user_")
 */
class UserController extends Controller
{
    /**
     * User login
     *
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $limit = 20;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository('ApiBundle:User')->createQueryBuilder('u')
            ->where('u.specialRole IS NOT NULL')
            ->andWhere('u.specialRole != :specialRole')
            ->setParameter('specialRole', User::ROLE_STAFF_COMPANY)
            ->getQuery();

        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), $limit);
        $userService = $this->get('app.user_service');

        return $this->render('AdminBundle:User:index.html.twig', array(
            'pagination' => $pagination,
            'userService' => $userService
        ));
    }

    /**
     * @Route("/new", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $roleList = array();
        $role = '';
        $manager = $this->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        /** @var User $user */
        $user = $manager->createUser();

        /** @var Company $company */
        $company = new Company();

        /** @var Shop $shop */
        $shop = new Shop();

        $form = $this->createForm(UserType::class,$user);
        $formCompany = $this->createForm(CompanyType::class,$company);
        $formShop = $this->createForm(ShopType::class,$shop);

        $form->handleRequest($request);
        $formCompany->handleRequest($request);
        $formShop->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
            $manager->updateUser($user, false);

            if ($user->getSpecialRole() == User::ROLE_COMPANY) {
                // Сохраняем файл
                $em->persist($company);
                $em->flush();

                if($company->file){
                    $company->saveFile();

                    $result = false;
                    $size = '256x';
                    $thumbnailSize = '128x';

                    $result = $fileService->compressImageFile($company->getPhotoWebLink(), $company->getPhotoThumbnailWebLink(), $thumbnailSize);

                    if ($result) {
                        $result = $fileService->compressImageFile($company->getPhotoWebLink(), $company->getPhotoWebLink(), $size);
                    }
                }

                $company->setUser($user);
                $em->persist($company);
            }

            if ($user->getSpecialRole() == User::ROLE_SHOP) {
                $em->persist($shop);
                $em->flush();

                // Сохраняем файл
                if($shop->file){
                    $shop->saveFile();

                    $result = false;
                    $size = '256x';
                    $thumbnailSize = '128x';

                    $result = $fileService->compressImageFile($shop->getPhotoWebLink(), $shop->getPhotoThumbnailWebLink(), $thumbnailSize);

                    if ($result) {
                        $result = $fileService->compressImageFile($shop->getPhotoWebLink(), $shop->getPhotoWebLink(), $size);
                    }
                }

                $shop->setUser($user);
                $em->persist($shop);
            }

            $em->flush();

            return $this->redirectToRoute('admin_user_index');
        }

        $roleList = $this->getParameter('role_list');

        return $this->render('AdminBundle:User:new.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'roleList' => $roleList
        ));
    }

    /**
     * User login
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(User $user, Request $request)
    {
        $addForm = null;
        $addEntity = null;
        $em =$this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');

        $form = $this->createForm(UserType::class, $user);

        if ($user->getSpecialRole() == User::ROLE_COMPANY) {
            $addForm = $this->createForm(CompanyType::class, $user->getCompany());
            $addEntity = $user->getCompany();
            $formTitle = 'Данные о компании';
            $view = $this->renderView('AdminBundle:User:companyForm.html.twig', array(
                'form' => $addForm->createView(),
                'formTitle' => $formTitle
            ));
        }

        if ($user->getSpecialRole() == User::ROLE_SHOP) {
            $addForm = $this->createForm(ShopType::class, $user->getShop());
            $addEntity = $user->getShop();
            $formTitle = 'Данные о магазине';
            $view = $this->renderView('AdminBundle:User:shopForm.html.twig', array(
                'form' => $addForm->createView(),
                'formTitle' => $formTitle
            ));
        }

        $form->handleRequest($request);
        $addForm->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $manager = $this->get('fos_user.user_manager');
            $manager->updateUser($user, false);

            if ($user->getSpecialRole() == User::ROLE_SHOP || $user->getSpecialRole() == User::ROLE_COMPANY) {
                if ($addEntity->file) {
                    $addEntity->saveFile();

                    $result = false;
                    $size = '256x';
                    $thumbnailSize = '128x';

                    $result = $fileService->compressImageFile($addEntity->getPhotoWebLink(), $addEntity->getPhotoThumbnailWebLink(), $thumbnailSize);

                    if ($result) {
                        $result = $fileService->compressImageFile($addEntity->getPhotoWebLink(), $addEntity->getPhotoWebLink(), $size);
                    }
                }
            }

            $em->persist($addEntity);
            $em->flush();

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('AdminBundle:User:edit.html.twig', array(
            'form' => $form->createView(),
            'user' => $user,
            'addFormView' => $view,
            'addEntity' => $addEntity
        ));
    }

    /**
     * User login
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = null;

        $user = $em->getRepository('ApiBundle:User')->find($request->get('id'));

        if (!$user) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        if (in_array($user->getSpecialRole(), array_keys($this->getParameter('role_list')))) {
            if ($user->getSpecialRole() === User::ROLE_STAFF_COMPANY) {
                $user->getStaffCompany()->removeUpload();
            }
            if ($user->getSpecialRole() === User::ROLE_SHOP) {
                $user->getShop()->removeUpload();
            }
            if($user->getSpecialRole() === User::ROLE_COMPANY) {
                $user->getCompany()->removeUpload();
            }
        }

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * User login
     *
     * @Route("/get/add_form/by_user_role", name="additional_form_by_role")
     * @Method({"GET", "POST"})
     */
    public function additionalFormByUserRoleAction(Request $request)
    {
        $role = null;
        $additionalForm = null;
        $entity = null;
        $view = null;
        $form = null;
        $userId = null;
        $formTitle = '';
        $role = $request->get('role');
        $userId = $request->get('userId');
        $em = $this->getDoctrine()->getManager();

        if ($role && $userId) {
            if ($role == User::ROLE_COMPANY) {
                $formTitle = 'Данные о компании';
                $entity = $em->getRepository('ApiBundle:Company')->findOneBy(array(
                    'user' => $userId
                ));

                if (!$entity) {
                    $entity = new Company();
                    $form = $this->createForm(CompanyType::class, $entity);
                    $view = $this->renderView('AdminBundle:User:companyForm.html.twig', array(
                        'form' => $form->createView(),
                        'formTitle' => $formTitle
                    ));
                } else {
                    $form = $this->createForm(CompanyType::class, $entity);
                    $view = $this->renderView('AdminBundle:User:companyForm.html.twig', array(
                        'form' => $form->createView(),
                        'formTitle' => $formTitle,
                        'company' => $entity
                    ));
                }
            }

            if ($role == User::ROLE_SHOP) {
                $formTitle = 'Данные о магазине';
                $entity = $em->getRepository('ApiBundle:Shop')->findOneBy(array(
                    'user' => $userId
                ));

                if (!$entity) {
                    $entity = new Shop();
                    $form = $this->createForm(ShopType::class, $entity);
                    $view = $this->renderView('AdminBundle:User:shopForm.html.twig', array(
                        'form' => $form->createView(),
                        'formTitle' => $formTitle
                    ));
                } else {
                    $form = $this->createForm(ShopType::class, $entity);
                    $view = $this->renderView('AdminBundle:User:shopForm.html.twig', array(
                        'form' => $form->createView(),
                        'formTitle' => $formTitle,
                        'shop' => $entity
                    ));
                }
            }
        } elseif ($role && !$userId) {
            if ($role == User::ROLE_COMPANY) {
                $entity = new Company();
                $formTitle = 'Данные о компании';
                $form = $this->createForm(CompanyType::class,$entity);
                $view = $this->renderView('AdminBundle:User:companyForm.html.twig', array(
                    'form' => $form->createView(),
                    'formTitle' => $formTitle
                ));
            }
            if ($role == User::ROLE_SHOP) {
                $entity = new Shop();
                $formTitle = 'Данные о магазине';
                $form = $this->createForm(ShopType::class,$entity);
                $view = $this->renderView('AdminBundle:User:shopForm.html.twig', array(
                    'form' => $form->createView(),
                    'formTitle' => $formTitle
                ));
            }
        } else {
            if ($role == User::ROLE_COMPANY) {
                $entity = new Company();
                $formTitle = 'Данные о компании';
                $form = $this->createForm(CompanyType::class,$entity);
                $view = $this->renderView('AdminBundle:User:companyForm.html.twig', array(
                    'form' => $form->createView(),
                    'formTitle' => $formTitle
                ));
            }
            if ($role == User::ROLE_SHOP) {
                $entity = new Shop();
                $formTitle = 'Данные о магазине';
                $form = $this->createForm(ShopType::class,$entity);
                $view = $this->renderView('AdminBundle:User:shopForm.html.twig', array(
                    'form' => $form->createView(),
                    'formTitle' => $formTitle
                ));
            }
        }

        return new JsonResponse(array('view' => $view));
    }

    /**
     * Check if user exist by username
     *
     * @Route("/check/exist_username", name="check_exist_username")
     * @Method({"GET", "POST"})
     */
    public function checkExistLoginAction(Request $request) {
        $user = null;
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'username' => $request->get('username')
        ));

        if ($user) {
            return new JsonResponse(['isExist' => true, 'message' => 'Пользователь с данным логином уже создан в системе. Используйте другой логин.']);
        } else {
            return new JsonResponse(['isExist' => false, 'message' => '']);
        }
    }

}
