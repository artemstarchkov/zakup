<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 *
 * @Route("/admin", name="admin_")
 */
class AdminController extends Controller
{
    /**
     *
     * @Route("/", name="index")
     * @Method({"GET", "POST"})
     */
    public function indexAction()
    {
        return $this->render('AdminBundle:Admin:index.html.twig', array(
        ));
    }

}
