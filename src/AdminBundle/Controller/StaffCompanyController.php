<?php

namespace AdminBundle\Controller;

use ApiBundle\Entity\StaffCompany;
use ApiBundle\Entity\User;
use ApiBundle\Form\StaffCompanyType;
use ApiBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class StaffCompanyController
 *
 * @Route("/admin/staff_company", name="admin_staff_company_")
 */
class StaffCompanyController extends Controller
{
    /**
     * New StaffCompany
     *
     * @Route("/new/{companyId}", name="new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $company = null;
        $manager = $this->get('fos_user.user_manager');
        $fileService = $this->get('app.file_service');

        /** @var User $user */
        $user = $manager->createUser();

        /** @var StaffCompany $staffCompany */
        $staffCompany = new StaffCompany();

        $userForm = $this->createForm(UserType::class,$user);
        $form = $this->createForm(StaffCompanyType::class,$staffCompany);

        $userForm->handleRequest($request);
        $form->handleRequest($request);

        if ($userForm->isSubmitted() and $userForm->isValid()) {
            $company = $em->getRepository('ApiBundle:Company')->find($request->get('companyId'));

            $user->setSpecialRole('ROLE_STAFF_COMPANY');
            $user->setEnabled(true);
            $manager->updateUser($user);

            // Сохраняем файл
            if($staffCompany->file){
                $em->persist($staffCompany);
                $em->flush();

                $staffCompany->saveFile();

                $result = false;
                $size = '256x';
                $thumbnailSize = '128x';

                $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoThumbnailWebLink(), $thumbnailSize);

                if ($result) {
                    $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoWebLink(), $size);
                }
            }

            $staffCompany->setCompany($company);
            $staffCompany->setUser($user);

            $em->persist($staffCompany);
            $em->flush();

            return $this->redirectToRoute('admin_company_staff_list', array('companyId' => $company->getId()));
        }

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->render('AdminBundle:StaffCompany:new.html.twig', array(
            'form' => $form->createView(),
            'userForm' => $userForm->createView(),
            'staffCompany' => $staffCompany,
            'companyId' => $request->get('companyId'),
            'csrf_token' => $csrfToken
        ));
    }

    /**
     *
     * @Route("/edit/{id}", name="edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request)
    {
        $staffCompany = null;
        $em =$this->getDoctrine()->getManager();
        $fileService = $this->get('app.file_service');
        $staffCompany = $em->getRepository('ApiBundle:StaffCompany')->find($request->get('id'));

        $staffCompanyForm = $this->createForm(StaffCompanyType::class, $staffCompany);
        $userForm = $this->createForm(UserType::class, $staffCompany->getUser());

        $staffCompanyForm->handleRequest($request);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {

            // Set again "ROLE_STAFF_COMPANY" role, because we haven't it role in "specialRole" select,
            // and "specialRole" field for user set in "NULL" when edit "staffCompany"
            $staffCompany->getUser()->setSpecialRole('ROLE_STAFF_COMPANY');

            // Сохраняем файл
            if($staffCompany->file){
                $staffCompany->saveFile();

                $result = false;
                $size = '256x';
                $thumbnailSize = '128x';

                $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoThumbnailWebLink(), $thumbnailSize);

                if ($result) {
                    $result = $fileService->compressImageFile($staffCompany->getPhotoWebLink(), $staffCompany->getPhotoWebLink(), $size);
                }
            }

            $em->persist($staffCompany);
            $em->flush();

            $manager = $this->get('fos_user.user_manager');
            $manager->updateUser($staffCompany->getUser());

            return $this->redirectToRoute('admin_company_staff_list', array('companyId' => $staffCompany->getCompany()->getId()));
        }

        return $this->render('AdminBundle:StaffCompany:edit.html.twig', array(
            'staffCompanyForm' => $staffCompanyForm->createView(),
            'staffCompany' => $staffCompany,
            'userForm' => $userForm->createView()
        ));
    }

    /**
     *
     * @Route("/delete/{id}", name="delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = null;
        $companyId = null;

        $user = $em->getRepository('ApiBundle:User')->findOneBy(array(
            'id' => $request->get('id'),
            'specialRole' => 'ROLE_STAFF_COMPANY'
        ));

        if (!$user) {
            throw  $this->createNotFoundException('Запись не найдена');
        }

        $companyId = $user->getStaffCompany()->getCompany()->getId();
        $user->getStaffCompany()->removeUpload();

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_company_staff_list', array('companyId' => $companyId));
    }

}
